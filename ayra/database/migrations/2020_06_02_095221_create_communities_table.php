<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vid');
            $table->string('title', 120)->nullable();
            $table->string('state', 120)->nullable();
            $table->string('images', 120)->nullable();
            $table->text('info_data', 120)->nullable();
            $table->string('lat', 120)->nullable();
            $table->string('long', 120)->nullable();
            $table->string('meta_title', 120)->nullable();
            $table->string('meta_key', 120)->nullable();
            $table->string('meta_discption', 120)->nullable();
            $table->integer('order_id')->nullable();
            $table->enum('is_deleted', ['1', '0'])->default('0');
            $table->enum('is_published', ['1', '0'])->default('0');       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communities');
    }
}
