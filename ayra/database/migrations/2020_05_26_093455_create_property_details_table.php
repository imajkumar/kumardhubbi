<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('property_id');
            $table->string('reciprocity_property')->nullable();              
            $table->string('property_title')->nullable();         
            $table->string('mlsId')->nullable();         
            $table->string('block')->nullable();         
            $table->string('parcel')->nullable();         
            $table->string('listdate')->nullable();         
            $table->string('expiredate')->nullable();         
            $table->string('modifydate')->nullable();         
            $table->string('solddate')->nullable();         
            $table->string('price_currency')->nullable();         
            $table->string('price')->nullable();         
            $table->string('listprice_currency')->nullable();         
            $table->string('listprice')->nullable();         
            $table->text('description')->nullable();         
            $table->string('num_bedrooms')->nullable();         
            $table->string('num_full_bathrooms')->nullable();         
            $table->string('square_feet')->nullable();         
            $table->string('lot_size')->nullable();         
            $table->string('views')->nullable();         
            $table->string('year_built')->nullable();         
            $table->string('width')->nullable();         
            $table->string('depth')->nullable();         
            $table->string('brochure')->nullable();         
                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_details');
    }
}
