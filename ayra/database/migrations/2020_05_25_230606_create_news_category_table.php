<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vid');
            $table->string('news_title', 120)->nullable();
            $table->string('news_short_discription', 120)->nullable();
            $table->text('news_discription')->nullable();
            $table->string('news_meta_title', 120)->nullable();
            $table->string('news_meta_keywords', 120)->nullable();
            $table->string('news_meta_discription', 120)->nullable();
            $table->enum('is_published', ['1', '0'])->default('0');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_category');
    }
}
