<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_blocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vid');
            $table->integer('module_id');
            $table->string('page_block_name', 120)->nullable();
            $table->string('parent_block', 120)->nullable();
            $table->string('parent_title', 120)->nullable();
            $table->string('parent_sub_title', 120)->nullable();
            $table->string('parent_sub_sub_title', 120)->nullable();
            $table->text('block_title_content')->nullable();
            $table->string('banner_img', 120)->nullable();
            $table->string('map_img', 120)->nullable();
            $table->string('video_img', 120)->nullable();
            $table->string('video_file', 120)->nullable();
            $table->longText('static_content')->nullable();
            $table->longText('sub_static_content')->nullable();            
            $table->text('external_link')->nullable();          
            $table->enum('is_published', ['1', '0'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_blocks');
    }
}
