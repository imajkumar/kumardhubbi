<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vid');
            $table->string('name', 120)->nullable();
            $table->string('agent_no', 120)->nullable();
            $table->string('designation', 120)->nullable();
            $table->string('assign_user', 120)->nullable();
            $table->string('photo', 120)->nullable();
            $table->text('info_data')->nullable();
            $table->string('email', 120)->nullable();
            $table->string('phone', 120)->nullable();
            $table->string('address', 120)->nullable();
            $table->string('facebook', 120)->nullable();
            $table->string('linkedin', 120)->nullable();
            $table->string('instagram', 120)->nullable();
            $table->string('youtube', 120)->nullable();
            $table->string('twitter', 120)->nullable();
            $table->string('meta_title', 120)->nullable();
            $table->string('meta_key', 120)->nullable();
            $table->string('meta_discption', 120)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
