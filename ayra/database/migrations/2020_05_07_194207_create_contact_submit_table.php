<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactSubmitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_submit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vid');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('mesage')->nullable();
            $table->string('ip_address')->nullable();          
            $table->string('replay_message')->nullable();
            $table->enum('is_deleted', ['0', '1']);
            $table->enum('is_read', ['0', '1']);
            $table->enum('status', ['0', '1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_submit');
    }
}
