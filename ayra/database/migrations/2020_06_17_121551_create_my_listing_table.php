<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMyListingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_listing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vid');
            $table->string('p_agent', 120)->nullable();
            $table->string('s_agnet', 120)->nullable();
            $table->string('property_type', 120)->nullable();
            $table->string('listing_type', 120)->nullable();
            $table->string('property_name', 120)->nullable();
            $table->string('currency', 120)->nullable();
            $table->string('monthly_rent', 120)->nullable();
            
            $table->string('available_from', 120)->nullable();
            $table->text('data_info')->nullable();
            $table->string('community', 120)->nullable();
            $table->string('address', 120)->nullable();
            $table->string('lat', 120)->nullable();
            $table->string('lng', 120)->nullable();
            $table->string('brochure', 120)->nullable();
            $table->string('photo', 120)->nullable();
            $table->string('video', 120)->nullable();
            $table->string('status', 120)->nullable();
            $table->dateTime('exp_date')->nullable();
            $table->enum('is_published', ['1', '0'])->default('0'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_listing');
    }
}
