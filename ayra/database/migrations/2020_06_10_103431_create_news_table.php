<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vid');
            $table->string('title', 120)->nullable();
            $table->string('category', 120)->nullable();
            $table->date('start_date')->nullable();
            $table->text('tags')->nullable();
            $table->string('photo')->nullable();
            $table->text('info')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_key')->nullable();
            $table->string('meta_discption')->nullable();
            $table->enum('is_published', ['1', '0'])->default('0');    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
