<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websetting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vid');
            $table->string('web_logo')->nullable();
            $table->string('favicon')->nullable();            
            $table->string('meta_keywords')->nullable();
            $table->string('meta_discription')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('youtube_link')->nullable();
            $table->string('linked_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('contact_cell')->nullable();
            $table->string('contact_office')->nullable();
            $table->string('contact_fax')->nullable();
            $table->string('contact_address')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('admin_email')->nullable();
            $table->string('admin_phone')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websetting');
    }
}
