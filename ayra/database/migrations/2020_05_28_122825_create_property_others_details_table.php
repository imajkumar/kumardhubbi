<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyOthersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_others_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('property_id');
            $table->string('sector')->nullable();              
            $table->string('area')->nullable(); 
            $table->string('house')->nullable(); 
            $table->string('unit_num')->nullable(); 
            $table->string('annual_income')->nullable(); 
            $table->string('monthly_maint_cost')->nullable(); 
            $table->string('monthly_insurance')->nullable(); 
            $table->string('monthly_sewage')->nullable(); 
            $table->string('insurance_in_maint')->nullable(); 
            $table->string('sewage_in_maint')->nullable(); 
            $table->string('stories_building')->nullable(); 
            $table->string('stories_condo')->nullable(); 
            $table->string('number_of_units')->nullable(); 
            $table->string('annual_operating_costs')->nullable(); 
            $table->string('sea_frontage')->nullable(); 
            $table->string('covenant')->nullable(); 
            $table->string('dishwasher')->nullable(); 
            $table->string('microwave')->nullable(); 
            $table->string('disposal')->nullable(); 
            $table->string('refrigerator')->nullable(); 
            $table->string('pool')->nullable(); 
            $table->string('tennis')->nullable(); 
            $table->string('fence')->nullable(); 
            $table->string('cistern')->nullable(); 
            $table->string('well')->nullable(); 
            $table->string('sewer')->nullable(); 
            $table->string('septic')->nullable(); 
            $table->string('city_water')->nullable(); 
            $table->string('dining_area')->nullable(); 
            $table->string('breakfast')->nullable(); 
            $table->string('kitchen')->nullable(); 
            $table->string('living_room')->nullable(); 
            $table->string('utility')->nullable(); 
            $table->string('family_room')->nullable(); 
            $table->string('den')->nullable(); 
            $table->string('assets')->nullable(); 
            $table->string('lease_details')->nullable(); 
            $table->string('permnt_fixtures')->nullable(); 
            $table->string('foundation')->nullable(); 
            $table->string('furnished')->nullable(); 
            $table->string('class')->nullable(); 
            $table->string('construction')->nullable(); 
            $table->string('land_certificate')->nullable(); 
            $table->string('trees')->nullable(); 
            $table->string('road_surface')->nullable(); 
            $table->string('soil')->nullable(); 
            $table->string('washer_dryer')->nullable(); 
            $table->string('how_shown')->nullable(); 
            $table->string('oven_or_range')->nullable(); 
            $table->string('ac')->nullable(); 
            $table->string('tv')->nullable(); 
            $table->string('porch')->nullable(); 
            $table->string('garage')->nullable(); 
            $table->string('car_port')->nullable(); 
            $table->string('zoning')->nullable(); 
            $table->string('topography')->nullable(); 
            $table->string('occupant')->nullable(); 
            $table->string('title')->nullable(); 
            $table->string('possession')->nullable(); 
            $table->string('frontage_road')->nullable(); 
            $table->string('type_use')->nullable(); 
            $table->string('floor_level')->nullable(); 
            $table->string('patio')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()

    {
        Schema::dropIfExists('property_others_details');
    }
}
