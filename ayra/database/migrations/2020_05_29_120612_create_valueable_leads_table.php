<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValueableLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valueable_leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vid');
            $table->string('name', 120)->nullable();
            $table->string('email', 120)->nullable();
            $table->string('telephone', 120)->nullable();
            $table->string('address', 120)->nullable();
            $table->string('block', 120)->nullable();
            $table->string('parcel', 120)->nullable();
            $table->string('view', 120)->nullable();
            $table->string('bedroom', 120)->nullable();
            $table->string('bathroom', 120)->nullable();
            $table->enum('is_read', ['1', '0'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valueable_leads');
    }
}
