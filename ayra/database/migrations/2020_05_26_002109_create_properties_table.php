<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('proerty primary id');
            $table->string('vid')->nullable();;
            $table->string('prop_id')->comment('proerty id');
            $table->string('module_name')->nullable();
            $table->string('property_type')->nullable();
            $table->string('listing_type')->nullable();
            $table->string('listing_status')->nullable();           
            $table->string('location_district')->nullable();
            $table->string('location_address_1')->nullable();
            $table->string('location_address_2')->nullable();
            $table->string('location_longitude')->nullable();
            $table->string('location_latitude')->nullable();            
            $table->enum('is_deleted', ['1', '0'])->default('0');          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
