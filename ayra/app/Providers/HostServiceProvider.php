<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Harimayco\Menu\Models\Menus;
use Harimayco\Menu\Models\MenuItems;
use Harimayco\Menu\Facades\Menu;
class HostServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function bootA(){
        
    }
    public function boot()
    {
        
        $vendors = DB::table('vendors')->where('web_url',HOST_DOMAIN)->first();       
        $vid=optional($vendors)->vid==null? '9711309624':$vendors->vid;
        $web_general = DB::table('websetting')->where('vid',$vid)->first();
        $contact = DB::table('contacts')->where('vid',$vid)->first();       
       // $public_menu_header = Menu::getByName('Header'); //return array
       
        $public_menu_header = Menu::getByVID_WITH_NAME($vid,'Header'); //return array
        //echo "<pre>";print_r($vid);exit('neeraj');
        $public_menu_footer = Menu::getByVID_WITH_NAME($vid,'Footer'); //return array
        
        // $public_menu_footer = Menu::getByName('Footer'); //return array
        // echo "<pre>";
        // $menu_arr=Menus::where('vid',$vid)->where('name','Header')->first();       
        // $menus_arrlist = DB::table('admin_menu_items')->where('menu',$menu_arr->id)->get();       


        // $public_menu_header = json_decode(json_encode($menus_arrlist), true);
        // // echo "<pre>";
        // // print_r($public_menu_header);
        // // die;
       // $public_menu_footer = Menu::getByName('Footer'); //return array

        $data=array(
            'web_settings'=>$vendors,
            'web_general'=>$web_general,
            'web_contact'=>$contact,
            'public_menu_header'=>$public_menu_header,
            'public_menu_footer'=>$public_menu_footer,
        );
        config()->set('web_settings', $data);


    }
}
