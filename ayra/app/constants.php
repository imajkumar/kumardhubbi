<?php
$host = FALSE; 
$base = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';


if (isset($_SERVER['HTTP_HOST'])) {
    $base .= '://'.$_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
    
    $host = $_SERVER['HTTP_HOST'];

  }

define('BASE_PATH', $base);
define('PREFIX', $base);
define('HOST_DOMAIN', $host); //kumar.local
define('UPLOADS', PREFIX.'uploads/');
define('IMAGE_PATH_SETTINGS', UPLOADS.'img/logo/');
define('URL_HOME', 'home');


//sasadmin

define('SAS_ADMIN_CATEGORYS', 'news-category');
