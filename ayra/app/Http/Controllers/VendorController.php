<?php

namespace App\Http\Controllers;

use App\Helpers\AyraHelp;
use Illuminate\Http\Request;
use Theme;
use App\Models\Vendor;
use DB;
class VendorController extends Controller
{

    public function getVendorsList()
    {
        $theme = Theme::uses('admin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('vendor.index', $data)->render();
    }

    /**
     * Add Vendor UI :addVendor
     */
    public function addVendor()
    {
        $theme = Theme::uses('admin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('vendor.add_vendor', $data)->render();
    }
    /**
     * SAVE Vendors:saveVendor
     */
    public function saveVendor(Request $request)
    {

        $data = array(
            'vid' => AyraHelp::getVID(),
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'web_url' => $request->web_url,
            'web_name' => $request->web_name,
            'notes' => $request->notes,
        );

        $resp = Vendor::saveModelData($data);
        return response()->json($resp);
    }
    /**
     * update vendor
     */
    public function updateVendor(Request $request)
    {

        $data = array(
            'vid' => $request->vid,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'web_url' => $request->web_url,
            'web_name' => $request->web_name,
            'notes' => $request->notes,
        );

        $resp = Vendor::UpdateModelData($data);
        return response()->json($resp);
    }

    /**
     * update website admin settings
     */
    public function saveAdminWebsiteSettings(Request $request){
        $resp = Vendor::UpdateWebsiteData($request);
        return response()->json($resp);
    }

    

     /**
      * getAjaxVendorsList 
      */
      public function getAjaxVendorsList(Request $request){      
        $vendors_data = DB::table('vendors')->get();
        $data_arr_1=array();
        foreach ($vendors_data as $key => $rowData) {

            $data_arr_1[]=array(    
                'RecordID' =>$rowData->id,          
                'vid' =>$rowData->vid,          
                'name' =>$rowData->name,
                'email' =>$rowData->email,         
                'phone' =>$rowData->phone,                            
                'web_url' =>$rowData->web_url,                              
                'web_name' =>$rowData->web_name,                                           
                'created_at' =>$rowData->created_at,
                ); 

        }
        $JSON_Data =json_encode($data_arr_1);
        $columnsDefault = [
        'RecordID'     => true, 
        'vid'     => true,  
        'name'     => true,    
        'email'  => true,  
        'phone'  => true,  
        'web_url'  => true,  
        'web_name'  => true, 
        'created_at'  => true, 
        'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data,$columnsDefault); 
        

      }
      /**
       * edit vendor data 
       */
      public function editVdendor($vid){  
        $theme = Theme::uses('admin')->layout('layout');
        $vendor_data=AyraHelp::getVendorbyVID($vid);
        $website_data=AyraHelp::getWebSettings($vid);
        $data = ["vendor_data" => $vendor_data,"web_setting"=>$website_data];
        return $theme->scope('vendor.edit_vendor', $data)->render();

      }

      /**
       * SuperUser Web settings
       */
      

      public function myWebsettings(){  
        $theme = Theme::uses('sasadmin')->layout('layout');
        $domain_arr=AyraHelp::getVendorbyDomain(request()->getHost());
        $vid=$domain_arr->vid;
        $vendor_data=AyraHelp::getVendorbyVID($vid);
        $website_data=AyraHelp::getWebSettings($vid);
        $data = ["vendor_data" => $vendor_data,"web_setting"=>$website_data];
        return $theme->scope('vendor.edit_my_vendor', $data)->render();

      }
      
      //end of function
}
