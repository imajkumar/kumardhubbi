<?php

namespace App\Http\Controllers;

use App\Helpers\AyraHelp;
use App\Helpers\AyraContant;
use Illuminate\Http\Request;
use Theme;
use DB;
use Mail;
use App\Mail\TestEmail;
use App\Models\Vendor;
use Harimayco\Menu\Models\Menus;
use Harimayco\Menu\Models\MenuItems;
use Harimayco\Menu\Facades\Menu;
use PDF;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     * $mysqlDatabaseName='aa';

       * $res = DB::connection('mysql')->statement("CREATE DATABASE IF NOT EXISTS `{$mysqlDatabaseName}`");

     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
       // saveAPIData_toDB();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $data['name']=array(5,6,7);
        // $pdf = PDF::loadView('pdf.property', $data);
        // return $pdf->download('invoice.pdf');

        getWebMobileHit();
       //sendEmail('registration', array('user_name'=>'imajkumar', 'username'=>'ajaja', 'to_email' => 'ajayit2020@gmail.com', 'password'=>'sss'));
       //die;

        // $data = ['message' => 'This is a test!'];

        // Mail::to('ajayit2020@gmail.com')->send(new TestEmail($data));

        // $flight = new Vendor;

        // $flight->name = 'ajay';
        // $flight->email = 'ajayit@kjk.com';
        // $flight->phone = 'ajayit@kjk.com';
        // $flight->web_url = 'ajayit@kjk.com';
        // $flight->notes = 'ajayit@kjk.com';

        // $flight->save();
        //echo request()->getHost();
        //die;

//   $domain_arr=AyraHelp::getVendorbyDomain(request()->getHost());
//   print_r($domain_arr);
//   die;


        $userRoles=[];
        if(\Auth::check()){   // Check is user logged in
        $user = auth()->user();
        $userRoles = $user->getRoleNames();


        $user_role = $userRoles[0];
        }else{
        $user_role='GUEST';
        }
       
        switch($user_role){
            case 'Admin':

            return $this->AdminDashboard();
            break;
            case 'SuperUser':
            return $this->SuperUserDashboard();
            break;
            case 'User':
            return $this->UserDashboard();
            break;
            default:
            return $this->FrontEnd();
            break;
        }


    } //index stop
    public function AdminDashboard(){

        $theme = Theme::uses('admin')->layout('layout');

        $data=["avatar_img"=>''];
        $theme->setTitle('Your rrtitle');
        return $theme->scope('index', $data)->render();
    } //AdminDashboard stop

    public function UserDashboard(){
        $theme = Theme::uses('user')->layout('layout');
        $data=["name"=>"Ajay"];
        return $theme->scope('user.index', $data)->render();
    } //UserDashboard stop

    public function FrontEnd(){
        $theme = Theme::uses('front')->layout('layout');

        $domain_arr=AyraHelp::getVendorbyDomain(request()->getHost());
        //$data=['public_menu'=>$public_menu];
        $data['info'] = 'Hello World';

        $theme->setTitle(optional($domain_arr)->web_name);
        //$theme->appendTitle('555');

        //return $theme->of('home', $data)->render();

        return $theme->scope('home.index', $data)->render();

    } //FrontEnd stop

    public function myWebsite(){
        $theme = Theme::uses('front')->layout('layout');
     
        $domain_arr=AyraHelp::getVendorbyDomain(request()->getHost());
        //$data=['public_menu'=>$public_menu];
        $data['info'] = 'Hello World';

        $theme->setTitle(optional($domain_arr)->web_name);
        //$theme->appendTitle('555');

        //return $theme->of('home', $data)->render();

        return $theme->scope('home.index', $data)->render();

    } //FrontEnd stop

    /**
     * Super Admin
     */

    public function SuperUserDashboard(){
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data=["name"=>"Ajay"];

        $domain_arr=AyraHelp::getVendorbyDomain(request()->getHost());


        $theme->setTitle(optional($domain_arr)->web_name);
        $theme->appendTitle('');


        return $theme->scope('index', $data)->render();
    } //FrontEnd stop







}
