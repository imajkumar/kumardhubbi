<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Str;
use App\Helpers\AyraHelp;
use Laravel\Ui\Presets\React;
use DB;
use Mail;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function validatePasswordRequestA(Request $request)
    {
        $otp=$request->otp;

       if(!empty($otp)){
        $user = DB::table('password_resets')->where('token',$otp)->where('email', '=', $request->email)->latest()->first(); //Check if the user exists

        $user_arr_data = User::where('email',$request->email)->first();
        $id=$user_arr_data->id;
        $user = User::findOrFail($id);
        

        $input['password'] = $request->new_password;
        $user->fill($input)->save();

        echo 2;
        
       

       }else{
        $user = DB::table('users')->where('email', '=', $request->email)->first(); //Check if the user exists

        //  die;
        $otp = rand(10000, 99999);



        //Create Password Reset Token
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $otp,
            'created_at' => Carbon::now()
        ]); //Get the token just created above
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();



        //send email
        $subLine = 'Password Recovery ';
        $txtEmail = $request->email;
        $data = array(
            'OTP' => $otp


        );
        Mail::send('emails.passwordRecovery', $data, function ($message) use ($txtEmail, $subLine) {

            $message->to($txtEmail, 'DM')->subject($subLine);

            $message->from('ajayit2020@gmail.com', 'DM Property');
        });
     
     echo 1;
        //send email


       }
        

    }
    private function sendResetEmail($email, $token)
    { //Retrieve the user from the database

        $user = DB::table('users')->where('email', $email)->select('name', 'email')->first(); //Generate, the password reset link. The token generated is embedded in the link$link = config('base_url') . 'password/reset/' . $token . '?email=' . urlencode($user->email);

        try {
            //Here send the link with CURL with an external email API         return true;

            sendEmail('registration', array('first_name' => 'ajay', 'last_name' => 'ajay', 'to_email' => 'ajayit2020@gmail.com', 'adasdf'));
        } catch (\Exception $e) {
            return false;
        }
    }
    //https://medium.com/@victorighalo/custom-password-reset-in-laravel-21e57816989f


    public function customLogin(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $mydomain_arr = AyraHelp::getVendorbyDomain(request()->getHost());
        $domai_url = User::where('email', $request->email)->where('vid_userid', $mydomain_arr->vid)->first();
        if ($domai_url == null) {
            $data = array(
                'login_token' => '',
                'status' => 0,
                'msg' => 'not hosted'
            );
        } else {
            $model = User::where('email', $request->email)->first();
            if (Hash::check($request->password, $model->password, [])) {
                
                Auth::loginUsingId($model->id, true);
                $data = array(
                    'login_token' => '',
                    'status' => 1
                );
            } else {
                $data = array(
                    'login_token' => '',
                    'status' => 0,
                    'msg' => 'failed login'
                );
            }
        }


        return response()->json($data);
    }
}
