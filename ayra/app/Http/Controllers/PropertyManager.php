<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\AyraHelp;
use Theme;
use DB;

class PropertyManager extends Controller
{



    public function updateMyListing(Request $request)
    {
        $fileName_brochure_img = "";
        $fileName_photo = "";
        $fileName_video = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;

        if ($request->hasFile('brochure_img')) {

            $file = $request->file('brochure_img');

            $fileName_brochure_img = 'brochure_img' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName_brochure_img);
            $affected = DB::table('my_listing')
              ->where('id', $request->txtID)
              ->update(['brochure' => $fileName_brochure_img]);          

        }
        if ($request->hasFile('prop_img')) {

            $file = $request->file('prop_img');

            $fileName_photo = 'prop_img' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName_photo);

            $affected = DB::table('my_listing')
              ->where('id', $request->txtID)
              ->update(['photo' => $fileName_photo]);

        }

        if ($request->hasFile('pro_video')) {

            $file = $request->file('pro_video');

            $fileName_video = 'pro_video' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName_video);

            $affected = DB::table('my_listing')
            ->where('id', $request->txtID)
            ->update(['video' => $fileName_video]);

        }

        $vidData=DB::table('my_listing')
            ->updateOrInsert(
                ['id' => $request->txtID],
                [
                    'vid' => $vid,
                    'p_agent' => $request->p_agent,
                    's_agnet' => $request->s_agent,
                    'property_type' => $request->p_type,
                    'listing_type' => $request->listing_type,
                    'property_name' => $request->p_name,
                    'currency' => $request->currency,
                    'monthly_rent' => $request->monthly_rent,
                    'available_from' => $request->available_from,
                    'data_info' => $request->summary_ckeditor,
                    'community' => $request->community_id,
                    'address' => $request->address,
                    'lat' => $request->lat,
                    'lng' => $request->lng,
                   
                    'status' => $request->prop_status,
                    'exp_date' => $request->prop_exp,
                    'updated_at' => date('Y-m-d H:i:s'),
    
                ]
            );


        
        $resp = array(
            'status' => 1,
            'message' => 'Added succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }

    public function saveMyListing(Request $request)
    {


        $fileName_brochure_img = "";
        $fileName_photo = "";
        $fileName_video = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;

        if ($request->hasFile('brochure_img')) {

            $file = $request->file('brochure_img');

            $fileName_brochure_img = 'brochure_img' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName_brochure_img);
        }
        if ($request->hasFile('prop_img')) {

            $file = $request->file('prop_img');

            $fileName_photo = 'prop_img' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName_photo);
        }

        if ($request->hasFile('pro_video')) {

            $file = $request->file('pro_video');

            $fileName_video = 'pro_video' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName_video);
        }

        $vidData = DB::table('my_listing')->insert(
            [
                'vid' => $vid,
                'p_agent' => $request->p_agent,
                's_agnet' => $request->s_agent,
                'property_type' => $request->p_type,
                'listing_type' => $request->listing_type,
                'property_name' => $request->p_name,
                'currency' => $request->currency,
                'monthly_rent' => $request->monthly_rent,
                'available_from' => $request->available_from,
                'data_info' => $request->summary_ckeditor,
                'community' => $request->community_id,
                'address' => $request->address,
                'lat' => $request->lat,
                'lng' => $request->lng,
                'brochure' => $fileName_brochure_img,
                'photo' => $fileName_photo,
                'video' => $fileName_video,
                'status' => $request->prop_status,
                'exp_date' => $request->prop_exp,
                'created_at' => date('Y-m-d H:i:s'),

            ]
        );
        $resp = array(
            'status' => 1,
            'message' => 'Added succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }

    public function AddNewProperty()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('property.add_mylisting', $data)->render();
    }
    public function editMyList($id)
    {
        $vendors_data = DB::table('my_listing')->where('id', $id)->first();

        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["data" => $vendors_data];

        return $theme->scope('property.edit_mylisting', $data)->render();
    }



    public function myListing()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('property.mylisting_List', $data)->render();
    }
    public function valueableLeads()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('property.view_valueablelead_list', $data)->render();
    }

    public function RentalLeads(){
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('property.view_rentallead_list', $data)->render();
    }
    public function contactLeads()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('property.view_contactlead_list', $data)->render();
    }
    public function propertyLeadView(Request $request)
    {



        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('property.view_property_list', $data)->render();
    }
    public function getContactMessage(Request $request)
    {
        $vendors_data = DB::table('contactus')->where('id', $request->prop_id)->first();

        $vidData = DB::table('contactus')
            ->where('id', $request->prop_id)
            ->update(['is_read' => 1]);

        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vendors_data

        );
        return response()->json($resp);
    }

    public function getAjaxContactValueList(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('valueable_leads')->where('is_deleted',0)->get();
        } else {
            $vendors_data = DB::table('valueable_leads')->where('is_deleted',0)->where('vid',$vid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {
            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'name' => $rowData->name,
                'email' => $rowData->email,
                'telephone' =>  $rowData->telephone,
                'address' => $rowData->address,
                'block' => $rowData->block,
                'parcel' => $rowData->parcel,
                'view' => $rowData->view,
                'bedroom' => $rowData->bedroom,
                'bathroom' => $rowData->bathroom,
                'status' => $rowData->is_read == 0 ? 2:1,


            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'name'     => true,
            'email'     => true,
            'telephone'  => true,
            'address'  => true,
            'block'  => true,
            'parcel'  => true,
            'view'  => true,
            'bedroom'  => true,
            'bathroom'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }
    //getAjaxRentalList
    public function getAjaxRentalList(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('rental_lead')->where('is_deleted',0)->get();
        } else {
            $vendors_data = DB::table('rental_lead')->where('is_deleted',0)->where('vid',$vid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {
            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'first_name' => $rowData->first_name,
                'last_name' => $rowData->last_name,
                'email' =>  $rowData->email,
                'phone' => $rowData->phone,
                'message' => $rowData->message,
                'created_at' => $rowData->created_at,
                'status' => $rowData->is_read,


            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'first_name'     => true,
            'last_name'     => true,
            'email'  => true,
            'phone'  => true,
            'message'  => true,
            'created_at'  => true,
            'status'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }

    //getAjaxRentalList

    public function getAjaxContactList(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('contactus')->where('is_deleted',0)->get();
        } else {
            $vendors_data = DB::table('contactus')->where('is_deleted',0)->where('vid',$vid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {
            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'first_name' => $rowData->first_name,
                'last_name' => $rowData->last_name,
                'email' =>  $rowData->email,
                'phone' => $rowData->phone,
                'message' => $rowData->message,
                'created_at' => $rowData->created_at,
                'status' => $rowData->is_read,


            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'first_name'     => true,
            'last_name'     => true,
            'email'  => true,
            'phone'  => true,
            'message'  => true,
            'created_at'  => true,
            'status'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }
    public function getAjaxPropertyList(Request $request)
    {

        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('properties')->get();
        } else {
            //$vendors_data = DB::table('properties')->get();
            // $vendors_data = DB::table('properties')
            //     ->join('property_details', 'properties.prop_id', '=', 'property_details.property_id')
            //     ->select('properties.*', 'property_details.*')
            //     ->get();
                $vendors_data = DB::table('properties')
            ->join('property_details', 'properties.prop_id', '=', 'property_details.property_id')
            ->join('property_others_details', 'properties.prop_id', '=', 'property_others_details.property_id')
            ->join('property_pictures', 'properties.prop_id', '=', 'property_pictures.property_id')
            ->select('properties.*', 'property_details.*', 'property_others_details.*','property_pictures.*')
            
            ->get();

        }


        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {
            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'name' => $rowData->property_title,
                'listing_name' => $rowData->property_title,
                'source' => 'NEW',
                'property_details' => '',
                'property_email' => '',
                'property_message' => '',
                'property_created_on' => $rowData->listdate,
                'picture_url' => $rowData->picture_url,
                'is_featured' => $rowData->is_featured == 0 ? 2 : 1,


            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'name'     => true,
            'listing_name'     => true,
            'source'  => true,
            'property_details'  => true,
            'property_email'  => true,
            'property_message'  => true,
            'property_created_on'  => true,
            'picture_url'  => true,
            'is_featured'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }
}
