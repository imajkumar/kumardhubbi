<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Socialite;
use App\User;
use Carbon\Carbon;


class SocialAuthController extends Controller
{
    public function redirect($provider){

       
        
    return Socialite::driver($provider)->redirect();
    }

    public function callback($provider){
        

    $getInfo = Socialite::driver($provider)->user();
    
    $user = $this->createUser($getInfo,$provider);
    

     
    
    auth()->login($user);

    return redirect()->to('/');

    }

    function createUser($getInfo,$provider){

        $user = User::where('provider_id', $getInfo->id)->first();
       
        if (!$user) {
            $user = User::create([
               'name'     => $getInfo->name,
               'email'    => $getInfo->email,
               'provider' => $provider,
               'provider_id' => $getInfo->id
           ]);
           $user->assignRole('User');

         }else{

         $ip=\Request::ip();
        $ip_json=json_encode((array)geoip($ip));
            $user->update([
                'last_login' =>date('Y-m-d H:i:s'),
                'login_from_json' => $ip_json,
                'last_login_ip' => $ip,
            ]);

         }
         return $user;
    }

    
    


}
