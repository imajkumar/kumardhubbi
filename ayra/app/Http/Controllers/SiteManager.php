<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Theme;
use DB;
use App\Models\Pages;
use Auth;
use App\ContactUS;
use App\Team;
use App\Community;
use App\Helpers\AyraHelp;
use Illuminate\Support\Str;
use Mail;
use PDF;

class SiteManager extends Controller
{



    public function propertyPrintDetail($propID)
    {

        $proData = DB::table('properties')
            ->join('property_details', 'properties.prop_id', '=', 'property_details.property_id')
            ->join('property_others_details', 'properties.prop_id', '=', 'property_others_details.property_id')
            ->join('property_pictures', 'properties.prop_id', '=', 'property_pictures.property_id')
            ->select('properties.*', 'property_details.*', 'property_others_details.*', 'property_pictures.*')
            ->where('properties.prop_id', $propID)
            ->first();
        $data['proData'] = $proData;

        $data['BASE_PATH'] = BASE_PATH;


        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('pdf.propertyDetail', $data);
        return $pdf->download('propertyDetail.pdf');
    }
    public function propertyPrint($Myroute)
    {

        switch ($Myroute) {
            case 'rentals':
                $data_arr = AyraHelp::getSellingData();
                break;
            case 'buying':
                $data_arr = AyraHelp::getSellingData();
                break;
        }
        $data['data_arr'] = $data_arr;

        $data['BASE_PATH'] = BASE_PATH;


        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('pdf.property', $data);
        return $pdf->download('property.pdf');
    }
    public function sendToFrined(Request $request)
    {
        //send email
        $subLine = 'Friend Refferal; ';
        $txtEmail = $request->email;
        $txtEmailMy = $request->myemail;
        $propID = $request->propID;
        $proData = DB::table('properties')
            ->join('property_details', 'properties.prop_id', '=', 'property_details.property_id')
            ->join('property_others_details', 'properties.prop_id', '=', 'property_others_details.property_id')
            ->join('property_pictures', 'properties.prop_id', '=', 'property_pictures.property_id')
            ->select('properties.*', 'property_details.*', 'property_others_details.*', 'property_pictures.*')
            ->where('properties.prop_id', $propID)
            ->first();


        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'myname' => $request->myname,
            'myemail' => $request->myemail,
            'msg' => $request->msg,
            'BASE_PATH' => BASE_PATH,
            'proData' => $proData


        );
        if ($request->copyFlag == 0) {
            Mail::send('emails.sendtoFriend', $data, function ($message) use ($txtEmail, $subLine) {

                $message->to($txtEmail, 'DM')->subject($subLine);

                $message->from('ajayit2020@gmail.com', 'DM Property');
            });
        } else {
            Mail::send('emails.sendtoFriend', $data, function ($message) use ($txtEmail, $txtEmailMy, $subLine) {

                $message->to($txtEmail, 'DM')->subject($subLine)
                    ->cc([$txtEmailMy]);

                $message->from('ajayit2020@gmail.com', 'DM Property');
            });
        }


        echo 1;
        //send email

    }

    public function paulaMccartney()
    {
        $vendors_data = DB::table('teams')->where('id', 1)->first();

        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => $vendors_data];
        return $theme->scope('pages.our_team_paulaMccartney', $data)->render();
    }
    public function teamPage($slug)
    {
        $vendors_data = DB::table('teams')->where('slug', $slug)->first();
        if($vendors_data->is_deleted==1 || $vendors_data->is_published==0){
            abort(404);
        }

        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => $vendors_data];
        return $theme->scope('pages.our_team_paulaMccartney', $data)->render();
    }

    

    public function MelisaMcTaggart()
    {
        $vendors_data = DB::table('teams')->where('id', 2)->first();

        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => $vendors_data];
        return $theme->scope('pages.our_team_melisaMcTaggart', $data)->render();
    }


    public function blogDetails($id)
    {
        $vendors_data = DB::table('news')->where('id', $id)->first();

        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => $vendors_data];

        return $theme->scope('pages.blog_details', $data)->render();
    }

    public function saveTestimonial(Request $request)
    {
        // print_r($request->all());
        $fileName = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($request->hasFile('photo')) {

            $file = $request->file('photo');

            $fileName = 'photo_testimonial' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
        }

        $vidData = DB::table('testimonials')->insert(
            [
                'vid' => $vid,
                'title' => $request->title,
                'created_by' => $request->created_by,
                'photo' => $fileName,
                'info_data' => $request->summary_ckeditor,
                'is_published' => $request->rdo_page_status,

            ]
        );
        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }




    public function updateTeams(Request $request)
    {
        // print_r($request->all());
        $fileName = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($request->hasFile('photo')) {

            $file = $request->file('photo');

            $fileName = 'photo_team' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
        }

       
        $flight = Team::find($request->txtID);
        $flight->vid = $vid;
        $flight->name = $request->name;
        $flight->slug = $request->slug;
        $flight->agent_no = $request->agent_no;
        $flight->assign_user = $request->assign_user;
        $flight->designation = $request->designation;
        $flight->photo = $fileName;
        $flight->info_data = $request->summary_ckeditor;
        $flight->email = $request->email;
        $flight->phone = $request->phone;
        $flight->address = $request->address;
        $flight->facebook = $request->facebook;
        $flight->linkedin = $request->linkedin;
        $flight->instagram = $request->instagram;
        $flight->youtube = $request->youtube;
        $flight->twitter = $request->twitter;
        $flight->meta_title = $request->cm_meta_title;
        $flight->meta_key = $request->cm_meta_keywords;
        $flight->meta_discption = $request->cm_meta_discription;
        $flight->is_published = $request->rdo_page_status;
        $flight->save();

        // $vidData = DB::table('teams')
        //     ->updateOrInsert(
        //         ['id' => $request->txtID],
        //         [

        //             'nam
        //             e' => $request->name,
        //             'agent_no' => $request->agent_no,
        //             'assign_user' => $request->assign_user,
        //             'designation' => $request->designation,
        //             'photo' => $fileName,
        //             'info_data' => $request->summary_ckeditor,
        //             'email' => $request->email,
        //             'phone' => $request->phone,
        //             'address' => $request->address,
        //             'facebook' => $request->facebook,
        //             'linkedin' => $request->linkedin,
        //             'instagram' => $request->instagram,
        //             'youtube' => $request->youtube,
        //             'twitter' => $request->twitter,
        //             'meta_title' => $request->cm_meta_title,
        //             'meta_key' => $request->cm_meta_keywords,
        //             'meta_discption' => $request->cm_meta_discription,
        //             'is_published' => $request->rdo_page_status,
        //         ]
        //     );


        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $flight

        );
        return response()->json($resp);
    }



    public function saveTeams(Request $request)
    {
        // print_r($request->all());
        $fileName = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($request->hasFile('photo')) {

            $file = $request->file('photo');

            $fileName = 'photo_team' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
        }

       

        $flight = new Team;

        $flight->vid = $vid;
        $flight->name = $request->name;
        // $flight->title = $request->name;
        $flight->agent_no = $request->agent_no;
        $flight->assign_user = $request->assign_user;
        $flight->designation = $request->designation;
        $flight->photo = $fileName;
        $flight->info_data = $request->summary_ckeditor;
        $flight->email = $request->email;
        $flight->phone = $request->phone;
        $flight->address = $request->address;
        $flight->facebook = $request->facebook;
        $flight->linkedin = $request->linkedin;
        $flight->instagram = $request->instagram;
        $flight->youtube = $request->youtube;
        $flight->twitter = $request->twitter;
        $flight->meta_title = $request->cm_meta_title;
        $flight->meta_key = $request->cm_meta_keywords;
        $flight->meta_discption = $request->cm_meta_discription;
        $flight->is_published = $request->rdo_page_status;
        $flight->save();


        
        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $flight

        );
        return response()->json($resp);
    }


    public function saveNews(Request $request)
    {
        // print_r($request->all());
        $fileName = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($request->hasFile('photo')) {

            $file = $request->file('photo');

            $fileName = 'photo_news' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
        }

        $vidData = DB::table('news')->insert(
            [
                'vid' => $vid,
                'title' => $request->news_title,
                'category' => $request->news_cat,
                'start_date' => $request->start_date,
                // 'tags' => $request->cm_state,
                'photo' => $fileName,
                'info' => $request->summary_ckeditor,
                'meta_title' => $request->cm_meta_title,
                'meta_key' => $request->cm_meta_keywords,
                'meta_discption' => $request->cm_meta_discription,
            ]
        );
        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }


    public function saveUpdateCommunity(Request $request)
    {
        // print_r($request->all());
        $fileName = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($request->hasFile('photo')) {

            $file = $request->file('photo');

            $fileName = 'photo' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
            $affected = DB::table('communities')
                ->where('id', $request->txtCID)
                ->update(['images' => $fileName]);
        }

        $vidData = DB::table('communities')
            ->where('id', $request->txtCID)
            ->update(
                [
                    'vid' => $vid,
                    'title' => $request->cm_title,
                    'slug' => $request->cm_slug,
                    'state' => $request->cm_state,
                    'info_data' => $request->summary_ckeditor,
                    'lat' => $request->lat,
                    'long' => $request->lng,
                    'meta_title' => $request->cm_meta_title,
                    'meta_key' => $request->cm_meta_keywords,
                    'meta_discption' => $request->cm_meta_discription,
                    'is_published' => $request->rdo_page_status,
                ]
            );


        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }



    public function saveCommunity(Request $request)
    {
        // print_r($request->all());
        $fileName = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($request->hasFile('photo')) {

            $file = $request->file('photo');

            $fileName = 'photo' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
        }

        // $vidData = DB::table('communities')->insert(
        //     [
        //         'vid' => $vid,
        //         'title' => $request->cm_title,
        //         'state' => $request->cm_state,
        //         'images' => $fileName,
        //         'info_data' => $request->summary_ckeditor,
        //         'lat' => $request->lat,
        //         'long' => $request->lng,
        //         'meta_title' => $request->cm_meta_title,
        //         'meta_key' => $request->cm_meta_keywords,
        //         'meta_discption' => $request->cm_meta_discription,
        //         'is_published' => $request->rdo_page_status,
        //     ]
        // );

        $flight = new Community;

        $flight->vid = $vid;
        $flight->title = $request->cm_title;
        $flight->state = $request->cm_state;
        $flight->images = $fileName;
        $flight->info_data = $request->summary_ckeditor;
        $flight->lat = $request->lat;
        //$flight->slug = null;
        $flight->long = $request->lng;
        $flight->meta_title = $request->cm_meta_title;
        $flight->meta_key = $request->cm_meta_keywords;
        $flight->meta_discption = $request->cm_meta_discription;
        $flight->is_published = $request->rdo_page_status;

        $flight->save();


        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => ''

        );
        return response()->json($resp);
    }



    public function updateDesignation(Request $request)
    {
        $vidData = DB::table('designations')
            ->updateOrInsert(
                ['id' => $request->txtID],
                [

                    'name' => $request->name,
                    'order_no' => $request->disp_order

                ]

            );

        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }



    public function updateNewsCategory(Request $request)
    {
        $vidData = DB::table('news_category')
            ->updateOrInsert(
                ['id' => $request->RecordID],
                [

                    'news_title' => $request->news_title,
                    'news_short_discription' => $request->news_short_discription,
                    'news_discription' => $request->summary_ckeditor,
                    'news_meta_title' => $request->news_meta_title,
                    'news_meta_keywords' => $request->news_meta_keywords,
                    'news_meta_discription' => $request->news_meta_discription,
                ]

            );

        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }
    public function editNewsCategory($id)
    {
        $vendors_data = DB::table('news_category')->where('id', $id)->first();


        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => $vendors_data];

        return $theme->scope('category.edit_news_category', $data)->render();
    }
    public function editCommunity($id)
    {
        $vendors_data = DB::table('communities')->where('id', $id)->first();


        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["data" => $vendors_data];

        return $theme->scope('community.edit_community', $data)->render();
    }






    public function editTeams($id)
    {
        $vendors_data = DB::table('teams')->where('id', $id)->first();


        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["data_arr" => $vendors_data];

        return $theme->scope('team.edit_team', $data)->render();
    }
    public function privicyPolicy()
    {



        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => ''];

        return $theme->scope('pages.privicy_policy', $data)->render();
    }





    public function editDesignation($id)
    {
        $vendors_data = DB::table('designations')->where('id', $id)->first();


        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["data_arr" => $vendors_data];

        return $theme->scope('designation.edit_designation', $data)->render();
    }



    public function siteNews()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('news.news_list', $data)->render();
    }

    public function siteTeam()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('team.team_list', $data)->render();
    }
    public function SiteCommunityView()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('pages.community', $data)->render();
    }
    public function siteCommunityDetails($id)
    {
        $vendors_data = DB::table('communities')->where('id', $id)->first();

        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => $vendors_data];

        return $theme->scope('pages.community_details', $data)->render();
    }
    public function siteCommunityDetailsView($slug)
    {
        $vendors_data = DB::table('communities')->where('slug', $slug)->first();

        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => $vendors_data];

        return $theme->scope('pages.community_details', $data)->render();
    }





    public function siteAddTeam()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('team.add_team', $data)->render();
    }






    public function testimonial()
    {



        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('testimonial.testimonial_list', $data)->render();
    }
    public function addTestimonial()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => ''];

        return $theme->scope('testimonial.add_testimonial', $data)->render();
    }
    public function ediTestimonial($id)
    {
        $vendors_data = DB::table('testimonials')->where('id', $id)->first();

        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["data_arr" => $vendors_data];

        return $theme->scope('testimonial.edit_testimonial', $data)->render();
    }



    //getAjaxMypropertyData
    public function getAjaxMypropertyData(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('my_listing')->where('is_deleted', 0)->get();
        } else {
            $vendors_data = DB::table('property_lead')->where('is_deleted', 0)->where('vid', Auth::user()->vid_userid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {


            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'first_name' => $rowData->first_name,
                'last_name' => $rowData->last_name,
                'email' => $rowData->email,
                'phone' => $rowData->phone,
                'message' => $rowData->message,
                'created_at' => $rowData->created_at,


            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'first_name'     => true,
            'last_name'     => true,
            'email'  => true,
            'phone'  => true,
            'message'  => true,
            'created_at'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }


    //getAjaxMypropertyData

    // getAjaxMyListingData
    public function getAjaxMyListingData(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('my_listing')->where('is_deleted', 0)->get();
        } else {
            $vendors_data = DB::table('my_listing')->where('is_deleted', 0)->where('vid', Auth::user()->vid_userid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {

            $photo = asset('uploads/img/logo') . "/" . $rowData->photo;
            $expd = date('Y-m-d', strtotime($rowData->exp_date));
            $catArr = DB::table('propertycats')->where('id', $rowData->property_type)->first();


            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'photo' => $photo,
                'property_name' => $rowData->property_name,
                'mlsid' => $rowData->id,
                'exp_date' => $expd,
                'property_type' => optional($catArr)->property_type,
                'status' => $rowData->status,
                'publish' => $rowData->is_published==0 ? 2 :1,

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'photo'     => true,
            'property_name'     => true,
            'mlsid'  => true,
            'exp_date'  => true,
            'property_type'  => true,
            'status'  => true,
            'publish'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }

    // getAjaxMyListingData


    public function getAjaxTeamList(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('teams')->where('is_deleted', 0)->get();
        } else {
            $vendors_data = DB::table('teams')->where('is_deleted', 0)->where('vid', Auth::user()->vid_userid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {


            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'name' => $rowData->name,
                'agent_no' => $rowData->agent_no,
                'designation' => $rowData->designation,
                'email' => $rowData->email,
                'is_published' => $rowData->is_published == 0 ? 2:1,

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'name'     => true,
            'agent_no'     => true,
            'designation'  => true,
            'email'  => true,
            'is_published'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }





    public function getAjaxDesignationList(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('designations')->get();
        } else {
            $vendors_data = DB::table('designations')->where('vid', Auth::user()->vid_userid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {


            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'name' => $rowData->name,
                'order_no' => $rowData->order_no,
                'status' => $rowData->is_published == 0 ? 2:1,

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'name'     => true,
            'order_no'     => true,
            'status'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }




    public function getAjaxTestimonialList(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('testimonials')->where('is_deleted', 0)->get();
        } else {
            $vendors_data = DB::table('testimonials')->where('is_deleted', 0)->where('vid', Auth::user()->vid_userid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {
            $img_path = asset('/uploads/img/logo') . "/" . optional($rowData)->photo;

            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'title' => $rowData->title,
                'created_by' => $rowData->created_by,
                'info_data' => $rowData->info_data,
                'status' => $rowData->is_published == 0 ? 2:1,

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'title'     => true,
            'created_by'     => true,
            'info_data'  => true,
            'status'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }


    public function getAjaxNewsList(Request $request)
    {

        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('news')->where('is_deleted', 0)->get();
        } else {
            $vendors_data = DB::table('news')->where('is_deleted', 0)->where('vid', Auth::user()->vid_userid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {
            $img_path = asset('/uploads/img/logo') . "/" . optional($rowData)->photo;

            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'title' => $rowData->title,
                'category' => $rowData->category,
                'start_date' => $rowData->start_date,
                'photo' => $img_path,
                'info' => Str::limit($rowData->info, $limit = 50, $end = '...'),
                'status' => $rowData->is_published == 0 ? 2:1,

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'title'     => true,
            'category'     => true,
            'start_date'  => true,
            'photo'  => true,
            'info'  => true,
            'status'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }
    public function myProfile(Request $request)
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('admin.myprofile', $data)->render();
    }
    public function getValueLeadDetails(Request $request)
    {
        $leadValueArr = DB::table('valueable_leads')->where('id', $request->rowid)->first();
        $html = '<div class="kt-section">
            <div class="kt-section__content">
                <table class="table table-bordered">
                   
                    <tbody>';

        $html .= '<tr>
                            <th scope="row">Block</th>
                            <td>' . $leadValueArr->block . '</td>
                            
                        </tr>';
        $html .= '<tr>
                            <th scope="row">Parcel</th>
                            <td>' . $leadValueArr->parcel . '</td>
                            
                        </tr>';
        $html .= '<tr>
                        <th scope="row">View</th>
                        <td>' . $leadValueArr->view . '</td>
                        
                    </tr>';
        $html .= '<tr>
                    <th scope="row">Bed Room</th>
                    <td>' . $leadValueArr->bedroom . '</td>
                    
                </tr>';
        $html .= '<tr>
                <th scope="row">Bath Room</th>
                <td>' . $leadValueArr->bathroom . '</td>
                
            </tr>';


        $html .= '    
                    </tbody>
                </table>
            </div>
        </div>
        ';
        echo $html;
    }

    public function getAjaxVendorNewsCategory(Request $request)
    {

        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('news_category')->where('is_deleted', 0)->get();
        } else {
            $vendors_data = DB::table('news_category')->where('is_deleted', 0)->where('vid', Auth::user()->vid_userid)->get();
        }

        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {

            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'title' => $rowData->news_title,
                'news_short_discription' => $rowData->news_short_discription,
                'news_discription' => $rowData->news_discription,
                'is_published' => $rowData->is_published == 0 ? 2:1,

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'title'     => true,
            'news_short_discription'     => true,
            'news_discription'  => true,
            'is_published'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }
    public function saveNewsCategory(Request $request)
    {
        // print_r($request->all());
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        $vidData = DB::table('news_category')->insert(
            [
                'vid' => $vid,
                'news_title' => $request->news_title,
                'news_short_discription' => $request->news_short_discription,
                'news_discription' => $request->summary_ckeditor,
                'news_meta_title' => $request->news_meta_title,
                'news_meta_keywords' => $request->news_meta_keywords,
                'news_meta_discription' => $request->news_meta_discription,
            ]
        );
        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }
    public function addNewsCategory()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('category.add_news_category', $data)->render();
    }
    public function addCommunity()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('community.add_community', $data)->render();
    }



    public function siteDesignation()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('designation.designation_list', $data)->render();
    }
    public function siteAddDesignation()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('designation.add_designation', $data)->render();
    }
    public function saveDesignation(Request $request)
    {


        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;


        $vidData = DB::table('designations')->insert(
            [
                'vid' => $vid,
                'name' => $request->name,
                'order_no' => $request->disp_order

            ]
        );
        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }






    public function addNews()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('news.add_news', $data)->render();
    }


    public function editNews($id)
    {
        $vendors_data = DB::table('news')->where('id', $id)->first();

        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["data_arr" => $vendors_data];
        return $theme->scope('news.edit_news', $data)->render();
    }



    public function updateTestimonial(Request $request)
    {

        $fileName = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($request->hasFile('photo')) {

            $file = $request->file('photo');

            $fileName = 'photo_news' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
        }


        $vidData = DB::table('testimonials')
            ->updateOrInsert(
                ['id' => $request->txtID],
                [

                    'title' => $request->title,
                    'created_by' => $request->created_by,
                    'photo' => $fileName,
                    'info_data' => $request->summary_ckeditor,
                    'is_published' => $request->rdo_page_status

                ]
            );

        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }




    public function updateNews(Request $request)
    {

        $fileName = "";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        if ($request->hasFile('photo')) {

            $file = $request->file('photo');

            $fileName = 'photo_news' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
        }


        $vidData = DB::table('news')
            ->updateOrInsert(
                ['id' => $request->txtID],
                [

                    'title' => $request->news_title,
                    'category' => $request->news_cat,
                    'start_date' => $request->start_date,
                    // 'tags' => $request->cm_state,
                    'photo' => $fileName,
                    'info' => $request->summary_ckeditor,
                    'meta_title' => $request->cm_meta_title,
                    'meta_key' => $request->cm_meta_keywords,
                    'meta_discption' => $request->cm_meta_discription,
                ]
            );

        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }






    public function newsCategory()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('category.category_list', $data)->render();
    }

    public function siteCommunity(Request $request)
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('community.community_list', $data)->render();
    }

    public function saveContactValuableData(Request $request)
    {
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        $admin_email = $web_arr->email;


        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'telephone' => 'required',
            'address' => 'required',
            'block' => 'required',
            'parcel' => 'required',
            'square' => 'required',
            'view_data' => 'required',
            'bedrooms' => 'required',
            'bathrooms' => 'required',
            'message' => 'required'
        ]);
        //ContactUS::create($request->all());
        DB::table('valueable_leads')->insert(
            [
                'vid' => $vid,
                'name' => $request->name,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'address' => $request->address,
                'block' => $request->block,
                'parcel' => $request->parcel,
                'view' => $request->view,
                'bedroom' => $request->bedroom,
                'bathroom' => $request->bathroom,
                'created_at' => date('Y-m-d H:i:s'),


            ]
        );

        return back()->with('success', 'Thanks ');
    }
    public function sendContactData(Request $request){
        $txtEmail="ajayit2020@gmail.com";
        $subLine="Support ticket | DM";
        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;

        $data = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,            
            'message' => $request->message,
            'BASE_PATH' => BASE_PATH
            


        );
        
        
           
        Mail::send('emails.sendtoSupport', $data, function ($message) use ($txtEmail, $subLine) {

            $message->to($txtEmail, 'DM')->subject($subLine);

            $message->from('ajayit2020@gmail.com', 'DM Property');
        });
        echo 1;
         
    }
    public function saveContactData(Request $request)
    {

        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;
        $admin_email = $web_arr->email;


        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'message' => 'required'
        ]);
        //ContactUS::create($request->all());


        $flight = new ContactUS;

        $flight->first_name = $request->first_name;
        $flight->last_name = $request->last_name;
        $flight->email = $request->email;
        $flight->phone = $request->phone;
        $flight->message = $request->message;
        $flight->vid = $vid;
        $flight->save();
        //sendEmail('registration', array('first_name'=>$request->first_name, 'last_name'=>$request->last_name, 'to_email' => $request->email,$request->message));
        echo 1;

        //return back()->with('success', 'Thanks for contacting us!');
    }
    //saveAdminBanner
    public function saveAdminBanner(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vid = Auth::user()->vid_userid;
        } else {
            $vid = Auth::user()->vid_userid;
        }

       

        //-----------------------------------
        if ($request->hasFile('banner_img')) {

            $file = $request->file('banner_img');

            $fileName = 'web_banner_imgAJ' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
            $vidData = DB::table('banners_list')
                ->updateOrInsert(
                    ['vid' => $vid, 'id' => $request->bannerID],
                    [
                        'banner_name' => $request->banner_title,
                        'photo' => $fileName,
                        'status' => $request->rdo_page_status,
                    ]

                );
        }
        //-----------------------------------
        $vidData = DB::table('banners_list')
        ->updateOrInsert(
            ['vid' => $vid, 'id' => $request->bannerID],
            [
              
                'status' => $request->rdo_page_status
            ]

        );
       

        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }


    //saveAdminBanner
    public function saveAdminBlocks(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vid = Auth::user()->vid_userid;
        } else {
            $vid = Auth::user()->vid_userid;
        }

        $vidData = DB::table('static_blocks')
            ->updateOrInsert(
                ['vid' => $vid, 'module_id' => $request->block_parent],
                [
                    'parent_title' => $request->block_title,
                    'parent_sub_title' => $request->block_subtitle,
                    'parent_sub_sub_title' => $request->block_sub_tag,
                    'block_title_content' => $request->block_title_content,
                    'static_content' => $request->summary_ckeditor,
                    'sub_static_content' => $request->summary_ckeditor_1,
                    'external_link' => $request->block_link,

                ]

            );

        //-----------------------------------
        if ($request->hasFile('banner_img')) {

            $file = $request->file('banner_img');

            $fileName = 'web_banner_img' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
            $vidData = DB::table('static_blocks')
                ->updateOrInsert(
                    ['vid' => $vid, 'module_id' => $request->block_parent],
                    [
                        'banner_img' => $fileName,
                    ]

                );
        }
        //-----------------------------------

        //-----------------------------------
        if ($request->hasFile('banner_map_img')) {

            $file = $request->file('banner_map_img');

            $fileName = 'banner_map_img' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
            $vidData = DB::table('static_blocks')
                ->updateOrInsert(
                    ['vid' => $vid, 'module_id' => $request->block_parent],
                    [
                        'map_img' => $fileName,
                    ]

                );
        }
        //-----------------------------------


        //-----------------------------------
        if ($request->hasFile('banner_video_img')) {

            $file = $request->file('banner_video_img');

            $fileName = 'web_banner_video_img' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
            $vidData = DB::table('static_blocks')
                ->updateOrInsert(
                    ['vid' => $vid, 'module_id' => $request->block_parent],
                    [
                        'video_img' => $fileName,
                    ]

                );
        }
        //-----------------------------------

        //-----------------------------------
        if ($request->hasFile('video_file')) {

            $file = $request->file('video_file');

            $fileName = 'web_video_file' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
            $vidData = DB::table('static_blocks')
                ->updateOrInsert(
                    ['vid' => $vid, 'module_id' => $request->block_parent],
                    [
                        'video_file' => $fileName,
                    ]

                );
        }
        //-----------------------------------


        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }

    public function getAjaxCommunityList(Request $request)
    {
        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            $vendors_data = DB::table('communities')->where('is_deleted', '0')->get();
        } else {
            $vendors_data = DB::table('communities')->where('is_deleted', '0')->where('vid', Auth::user()->vid_userid)->get();
        }
        $data_arr_1 = array();

        foreach ($vendors_data as $key => $rowData) {
            $img_path = asset('/uploads/img/logo') . "/" . optional($rowData)->images;
            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'name' => $rowData->title,
                'photo' => $img_path,
                'info_data' => Str::limit($rowData->info_data, $limit = 50, $end = '...'),
                'order' => $rowData->order_id,
                'is_published' => ($rowData->is_published == 0) ? 2:1

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'name'     => true,
            'photo'     => true,
            'info_data'  => true,
            'order'  => true,
            'is_published'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }
    //getAjaxVendorBannerList
    public function getAjaxVendorBannerList(Request $request)
    {

        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            //$vendors_data = DB::table('static_blocks')->get();
            $vendors_data = DB::table('banners_list')->where('vid', Auth::user()->vid_userid)->get();
        } else {
            $vendors_data = DB::table('banners_list')->where('vid', Auth::user()->vid_userid)->get();
        }



        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {
           
            $photo = asset('uploads/img/logo') . "/" . $rowData->photo;

            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'banner_name' => $rowData->banner_name,
                'photo' => $photo,
                'created_at' => $rowData->created_at,
                'status' => $rowData->status == 0 ? 2 : 1,
                

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'banner_name'     => true,
            'photo'     => true,
            'created_at'  => true,
            'status'  => true,          
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }


    //getAjaxVendorBannerList

    public function getAjaxVendorStaticBlockList(Request $request)
    {

        $user = auth()->user();
        $userRoles = $user->getRoleNames();
        $user_role = $userRoles[0];
        if ($user_role == 'Admin') {
            //$vendors_data = DB::table('static_blocks')->get();
            $vendors_data = DB::table('static_blocks')->where('vid', Auth::user()->vid_userid)->get();
        } else {
            $vendors_data = DB::table('static_blocks')->where('vid', Auth::user()->vid_userid)->get();
        }



        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {
            $module_data = DB::table('modules')->where('id', $rowData->module_id)->first();
            $vendor_data = DB::table('vendors')->where('vid', $rowData->vid)->first();

            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'title' => $module_data->module_name,
                'module_id' => $rowData->module_id,
                'module_name' => $module_data->module_name,
                'is_published' => ($rowData->is_published == 0)? 2:1,
                'created_by' => $vendor_data->name,

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'title'     => true,
            'module_id'     => true,
            'module_name'  => true,
            'is_published'  => true,
            'created_by'  => true,
            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }



    public function getAjaxPagesList(Request $request)
    {
        $vendors_data = DB::table('static_pages')->get();
        $data_arr_1 = array();
        foreach ($vendors_data as $key => $rowData) {
            $module_data = DB::table('modules')->where('id', $rowData->module_id)->first();
            $data_arr_1[] = array(
                'RecordID' => $rowData->id,
                'title' => $rowData->title,
                'module_id' => $rowData->module_id,
                'module_name' => $module_data->module_name,
                'is_published' => $rowData->is_published,

            );
        }
        $JSON_Data = json_encode($data_arr_1);
        $columnsDefault = [
            'RecordID'     => true,
            'title'     => true,
            'module_id'     => true,
            'module_name'  => true,
            'is_published'  => true,

            'Actions'      => true,
        ];
        $this->DataGridResponse($JSON_Data, $columnsDefault);
    }


    public function upload(Request $request)
    {
        if ($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $file = $request->file('upload');
            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename . '_' . time() . '.' . $extension;

            //Upload File
            // $request->file('upload')->storeAs('public', $filenametostore);

            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $filenametostore);


            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('uploads/img/logo/' . $filenametostore);
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            // Render HTML output 
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }
    }
    /**
     * Add Menu UI :adminMenu
     */
    public function adminMenu()
    {

        // $theme = Theme::uses('admin')->layout('layout');
        // $data = ["avatar_img" => ''];
        // return $theme->scope('menu.add_menu', $data)->render();

        $theme = Theme::uses('admin')->layout('layout');
        $data = ["avatar_img" => ''];
    }

    public function show($slug = 'home')
    {
        $theme = Theme::uses('front')->layout('layout');
        $page = Pages::whereSlug($slug)->first();
        if ($page == null) {
            abort(404);
        }

        $data = ["page" => $page];
        $pagesView = 'pages.' . $page->route;
        return $theme->scope($pagesView, $data)->render();
    }
    public function siteContact()
    {
        $theme = Theme::uses('admin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('contact.add_contact', $data)->render();
    }
    public function sellingView()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.selling', $data)->render();
    }
    public function editBannerList($id)
    {
        $vendors_data = DB::table('banners_list')->where('id', $id)->first();


        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["data" => $vendors_data];

        return $theme->scope('pages.edit_bannerList', $data)->render();
    }

    




    public function adminPages()
    {
        $theme = Theme::uses('admin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.add_home_page', $data)->render();
    }
    public function vendorPagesList()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.add_home_page', $data)->render();
    }
    public function adminStaticBlock()
    {
        $theme = Theme::uses('admin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.static_block', $data)->render();
    }
    public function bannersList()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.bannners_list', $data)->render();
    }

    

    public function editvendorStaticBlock($id)
    {
        $vendors_data = DB::table('static_blocks')->where('id', $id)->first();


        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["sb_arr_data" => $vendors_data];
        return $theme->scope('pages.add_static_block', $data)->render();
    }



    public function vendorStaticBlock()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.static_block', $data)->render();
    }

    public function addAdminStaticBlock()
    {
        $theme = Theme::uses('admin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.add_static_block', $data)->render();
    }



    public function adminPagesList()
    {
        $theme = Theme::uses('admin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.page_list', $data)->render();
    }



    public function siteAbout()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.about', $data)->render();
    }
    public function siteOurTeam()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        //return $theme->scope('pages.our_team', $data)->render();
        abort('404');
    }
    public function viewTeamMemberDetails($id)
    {
        $vendors_data = DB::table('teams')->where('id', $id)->first();

        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => $vendors_data];
        return $theme->scope('pages.our_team', $data)->render();
    }


    public function siteBuying()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.buying', $data)->render();
    }
    public function siteRentals()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.rentals', $data)->render();
    }

    public function siteMoreMlsListing()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.more_mls_listing', $data)->render();
    }
    public function siteProperties()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.properties', $data)->render();
    }
    public function sitePropertiesDetails($id)
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.propertiesDetails', $data)->render();
    }
    public function myproDetails($id)
    {
        $vendors_data = DB::table('my_listing')->where('id', $id)->first();

        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => $vendors_data];
        return $theme->scope('pages.my_propertiesDetails', $data)->render();
    }
    public function FeaturemyproDetails($id)
    {
        //$vendors_data = DB::table('my_listing')->where('id', $id)->first();
        $vendors_data = DB::table('properties')
            ->join('property_details', 'properties.prop_id', '=', 'property_details.property_id')
            ->join('property_others_details', 'properties.prop_id', '=', 'property_others_details.property_id')
            ->join('property_pictures', 'properties.prop_id', '=', 'property_pictures.property_id')
            ->select('properties.*', 'property_details.*', 'property_others_details.*', 'property_pictures.*')
            ->where('properties.id', $id)
            ->first();



        $theme = Theme::uses('front')->layout('layout');
        $data = ["data_arr" => $vendors_data];
        return $theme->scope('pages.my_propertiesDetailsFeature', $data)->render();
    }
    public function getSetPublishMyListing(Request $request)
    {
        $pageID = $request->pageID;
        $publish = $request->publish;
        $row = $request->row;
        switch ($pageID) {
            case 1:
                $affected = DB::table('my_listing')
                    ->where('id', $row)
                    ->update(['is_published' => $publish]);

                break;

            case 2:
                $affected = DB::table('testimonials')
                    ->where('id', $row)
                    ->update(['is_published' => $publish]);

                break;
            case 3:
                $affected = DB::table('communities')
                    ->where('id', $row)
                    ->update(['is_published' => $publish]);

                break;
            case 4:
                $affected = DB::table('designations')
                    ->where('id', $row)
                    ->update(['is_published' => $publish]);

                break;
            case 5:
                $affected = DB::table('news_category')
                    ->where('id', $row)
                    ->update(['is_published' => $publish]);

                break;
            case 6:
                $affected = DB::table('news')
                    ->where('id', $row)
                    ->update(['is_published' => $publish]);

                break;
            case 10:
                $affected = DB::table('property_details')
                    ->where('id', $row)
                    ->update(['is_featured' => $publish]);

                break;
                case 11:
                    $affected = DB::table('teams')
                        ->where('id', $row)
                        ->update(['is_published' => $publish]);
    
                    break;
        }
    }

    //setAllcheckboxAction
    public function setAllcheckboxAction(Request $request)
    {
        foreach ($request->rows_selected as $key => $row) {
           

            if ($request->action_id == 0) {
                $affected = DB::table('contactus')
            ->where('id', $row)
            ->update(['is_deleted' => 1]);
            }
        }
    }
    //setAllcheckboxAction
    public function setAllcheckboxAction1(Request $request)
    {
        foreach ($request->rows_selected as $key => $row) {
           

            if ($request->action_id == 0) {
                $affected = DB::table('rental_lead')
            ->where('id', $row)
            ->update(['is_deleted' => 1]);
            }
        }
    }
    public function savePropertyLeadData(Request $request)
    {

        $web_arr = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
        $vid = $web_arr->vid;

        switch ($request->frmType) {
            case 3:
              
                $vidData = DB::table('rental_lead')->insert(
                    [
                        'vid' => $vid,
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'email' => $request->email,
                        'phone' => $request->phone,
                        'message' => $request->message,
                        'created_at' => date('Y-m-d H:i:s'),
        
        
                    ]
                );

                break;
            
            default:
            $vidData = DB::table('property_lead')->insert(
                [
                    'vid' => $vid,
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'message' => $request->message,
                    'created_at' => date('Y-m-d H:i:s'),
    
    
                ]
            );

                break;
        }

        
        echo 1;
    }
    public function siteBlogNews()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.blog_news', $data)->render();
    }

    public function propCatList($id)
    {


        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.property_cat_listdata', $data)->render();
    }
    public function deleteAction(Request $request)
    {
        switch ($request->option) {
            case 1:
                $affected = DB::table('news_category')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                break;
            case 2:
                $affected = DB::table('testimonials')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                break;
            case 3:
                $affected = DB::table('communities')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                break;
            case 4:
                $affected = DB::table('teams')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                break;
            case 5:
                $affected = DB::table('my_listing')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                break;
            case 6:
                $affected = DB::table('contactus')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                break;
            case 7:
                $affected = DB::table('property_lead')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                break;
            case 8:
                $affected = DB::table('valueable_leads')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                break;
            case 9:
                $affected = DB::table('news')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                break;
                case 50:
                    $affected = DB::table('rental_lead')
                    ->where('id', $request->rowid)
                    ->update(['is_deleted' => 1]);
                    break;
        }
    }
    public function searchAdvance()
    {
        $theme = Theme::uses('front')->layout('layout');

        $propertyName = request('propertyName');
        $propID = request('propID');

        //$users = User::where('name', 'LIKE', '%' . $query . '%')->paginate(10);;
        $data = DB::table('properties')
            ->join('property_details', 'properties.prop_id', '=', 'property_details.property_id')
            ->join('property_others_details', 'properties.prop_id', '=', 'property_others_details.property_id')
            ->join('property_pictures', 'properties.prop_id', '=', 'property_pictures.property_id')
            ->select('properties.*', 'property_details.*', 'property_others_details.*', 'property_pictures.*')
            ->where('property_details.property_title', 'LIKE', '%' . $propertyName . '%')
            ->paginate(2);


        // $data = DB::table('property_details')
        // ->where('property_title', 'LIKE', '%' . $query . '%')->paginate(2);

        $data = ["users" => $data];
        //return view('searchresult',compact('users'));
        return $theme->scope('pages.searchresult', $data)->render();
    }
    public function search()
    {
        $theme = Theme::uses('front')->layout('layout');

        $query = request('search_text');
        //$users = User::where('name', 'LIKE', '%' . $query . '%')->paginate(10);;
        $data = DB::table('properties')
            ->join('property_details', 'properties.prop_id', '=', 'property_details.property_id')
            ->join('property_others_details', 'properties.prop_id', '=', 'property_others_details.property_id')
            ->join('property_pictures', 'properties.prop_id', '=', 'property_pictures.property_id')
            ->select('properties.*', 'property_details.*', 'property_others_details.*', 'property_pictures.*')
            ->where('property_details.property_title', 'LIKE', '%' . $query . '%')
            ->orwhere('properties.property_type', 'LIKE', '%' . $query . '%')
            ->orwhere('properties.location_district', 'LIKE', '%' . $query . '%')
            ->paginate(2);


        // $data = DB::table('property_details')
        // ->where('property_title', 'LIKE', '%' . $query . '%')->paginate(2);

        $data = ["users" => $data];
        //return view('searchresult',compact('users'));
        return $theme->scope('pages.searchresult', $data)->render();
    }

    public function prop_cat_details($id)
    {


        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.property_cat_listdata_details', $data)->render();
    }


















    public function siteContactView()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.contact', $data)->render();
    }
    public function siteNewsBlog()
    {
        $theme = Theme::uses('front')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('pages.news_blogs', $data)->render();
    }


    public function siteContactUser()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('contact.add_contact', $data)->render();
    }
    public function property_lead()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('property.propertyLead', $data)->render();
    }
    public function editPropertyLead()
    {
        $theme = Theme::uses('sasadmin')->layout('layout');
        $data = ["avatar_img" => ''];
        return $theme->scope('property.edit_propertyLead', $data)->render();
    }




    public function saveStaticPages(Request $request)
    {
        $vid = Auth::user()->vid_userid;
        $vidData = DB::table('static_pages')
            ->updateOrInsert(
                ['vid' => $vid, 'module_id' => $request->pages_module],
                [
                    'title' => $request->page_title,
                    'slug' => $request->page_title,
                    'module_id' => $request->pages_module,
                    'contents' => $request->summary_ckeditor,
                    'meta_title' => $request->page_meta_title,
                    'meta_keywords' => $request->page_meta_keywords,
                    'meta_discription' => $request->page_meta_discription,
                    'is_published' => $request->rdo_page_status,
                ]

            );

        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );
        return response()->json($resp);
    }
    public function saveContact(Request $request)
    {

        //  print_r($request->all());
        $vid = $request->vid;
        $vidData = DB::table('contacts')
            ->updateOrInsert(
                ['vid' => $vid],
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'facebook' => $request->facebook,
                    'twitter' => $request->twitter,
                    'instagram' => $request->instagram,
                    'linkedin' => $request->linkedin,
                    'youtube' => $request->youtube,
                    'office_phone' => $request->office_phone,
                    'fax_phone' => $request->fax_phone,
                    'office_phone' => $request->office_phone,
                    'site_address' => $request->site_address,
                ]

            );

        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );


        return response()->json($resp);
    }
}
