<?php

//namespace App;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Helpers\AyraHelp;
use Auth;
use Illuminate\Http\Request;
use Mail;
use App\Mail\TestEmail;
use DB;

class Vendor extends Model
{
    protected $fillable = [
        'vid', 'name', 'email', 'phone', 'web_url', 'notes', 'web_name'

    ];


    protected static function saveModelData($data = [])
    {
        $password = AyraHelp::getVID();
        $user_data = User::where('email', $data['email'])->first();
        $password = 'vendor@123';
        $vendata = Vendor::where('web_url', $data['web_url'])->where('email', $data['email'])->first();
        if ($vendata == null) {
            if ($user_data == null) {
                $vidData = Vendor::create($data); //vendor creation and get id and save wtih users 
                $users = User::create([
                    'name' => $data['name'],
                    'email' =>  $data['email'],
                    'password' => $password,
                    'vid_userid' => $vidData->vid,
                ]);
                $users->assignRole('SuperUser');
                //add header
                DB::table('admin_menus')->insert(
                    [
                        'vid' => $vidData->vid, 
                        'name' => 'Header'
                    ]
                );
                DB::table('admin_menus')->insert(
                    [
                        'vid' => $vidData->vid, 
                        'name' => 'Footer'
                    ]
                );
                
                //add footer
                $dataMAIL = ['password' => $password, 'email' => $data['email']];

               Mail::to($data['email'])->send(new TestEmail($dataMAIL)); //send email to vendor with email and password

                $resp = array(
                    'status' => 1,
                    'message' => 'Already exists vendor',
                    'data' => $vidData

                );
            }
        } else {
            $resp = array(
                'status' => 0,
                'message' => 'Already exists vendor',

            );
        }
        return $resp;
    }
    //update vendor data 
    protected static function UpdateModelData($data = [])
    {


        $vidData = Vendor::where('vid', $data['vid'])
            ->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'web_url' => $data['web_url'],
                'web_name' => $data['web_name'],
                'notes' => $data['notes'],
            ]);


        $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );

        return $resp;
    }

    //update vendor data 

    // update admin website

    protected static function UpdateWebsiteData($request)
    {
        $vid = $request->vid;

        $vidData= DB::table('websetting')
                ->updateOrInsert(
                    ['vid' => $vid],
                    [
                        'meta_keywords' => $request->meta_keywords,
                        'meta_discription' => $request->meta_discription,
                    ]

                );

        //-----------------------------------
        if ($request->hasFile('logo')) {
           
            $file = $request->file('logo');

            $fileName = 'web_logo' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $name_arr = explode(".", $file->getClientOriginalName());
            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
           $vidData= DB::table('websetting')
                ->updateOrInsert(
                    ['vid' => $vid],
                    [
                        'web_logo' => $fileName,
                    ]

                );
        }
        if ($request->hasFile('logoLogin')) {
           
            $file = $request->file('logoLogin');

            $fileName = 'web_logoLogin' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $name_arr = explode(".", $file->getClientOriginalName());
            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
           $vidData= DB::table('websetting')
                ->updateOrInsert(
                    ['vid' => $vid],
                    [
                        'web_logoLogin' => $fileName,
                    ]

                );
        }

        

        if ($request->hasFile('favicon')) {
         
            $file = $request->file('favicon');

            $fileName = 'web_favicon' . $vid . "_" . rand() . "_" . date('dmyhis') . "." . $file->getClientOriginalExtension();

            $name_arr = explode(".", $file->getClientOriginalName());
            $destinationPath = 'uploads/img/logo';
            $file->move($destinationPath, $fileName);
           
        }
         //-----------------------------------

         $resp = array(
            'status' => 1,
            'message' => 'updated succfully',
            'data' => $vidData

        );

        return $resp;

    }

    // update admin websiteg

}
