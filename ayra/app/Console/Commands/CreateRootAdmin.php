<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Models\Vendor;
use DB;

class CreateRootAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:root';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used to create root user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $vendar = Vendor::create([
            'vid' => 9711309624,
            'email' => 'admin@admin.com',
            'name' => 'Diamond Property',
            'phone' => '99999999999',
            'web_url' => '13.232.175.191',
            'web_name' => 'Diamond Property',
            'notes' => 'Auto Created',
        ]);

        $newuser = [
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => '123456',
            'verified' => '1',
            'vid_userid' => $vendar->vid,
        ];
        $users = User::create($newuser);
        $users->assignRole('Admin');

        //add pages
        DB::table('pages')->insert(
            [
                'slug' => 'about',
                'title' => 'About',
                'route' => 'about',
            ]
        );
        DB::table('pages')->insert(
            [
                'slug' => 'contact',
                'title' => 'Contact Us',
                'route' => 'contact',
            ]
        );
        

        //add pages



    }
}
