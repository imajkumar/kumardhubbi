<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class AyraHelp
{


    public static function getPropertyIncludeCommunity(){
       
        $users = DB::table('my_listing')
            ->join('communities', 'my_listing.community', '=', 'communities.id')
          
            ->select('my_listing.*', 'communities.*')
            ->get();
             return $users;


    }

    public static function getSellingData(){
        // $vendors = DB::table('property_details')->get();
        // return $vendors;
        $users = DB::table('properties')
            ->join('property_details', 'properties.prop_id', '=', 'property_details.property_id')
            ->join('property_others_details', 'properties.prop_id', '=', 'property_others_details.property_id')
            ->join('property_pictures', 'properties.prop_id', '=', 'property_pictures.property_id')
            ->select('properties.*', 'property_details.*', 'property_others_details.*','property_pictures.*')
            ->get();
             return $users;


    }

    public static function getBlockByHOST_HOME($vid,$mid){
        $vendors = DB::table('static_blocks')->where('vid',$vid)->where('module_id',$mid)->first();
        return $vendors;
    }
    public static function getTestimonialkByHOST_HOME($vid){
        $vendors = DB::table('testimonials')->where('vid',$vid)->where('is_published',1)->where('is_deleted',0)->limit(10)->get();

        return $vendors;
    }

    public static function getmyTeamByHOST_HOME($vid){
        $vendors = DB::table('teams')->where('vid',$vid)->where('is_deleted',0)->where('is_published',1)->limit(10)->get();
        return $vendors;
    }
    public static function getmyTeamByHOST_HOME_NOLIMIT($vid){
        $vendors = DB::table('teams')->where('vid',$vid)->where('is_deleted',0)->where('is_published',1)->get();
        return $vendors;
    }
    public static function getCommunitiesByHOSTHOME($vid){
        $vendors = DB::table('communities')->where('is_deleted','0')->where('is_published','1')->where('vid',$vid)->get();
                      
        return $vendors;
    }
    public static function getCommunitiesByHOSTHOME_ID($vid,$id){
        $vendors = DB::table('communities')->where('vid',$vid)->where('id',$id)->first();
        return $vendors;
    }
    public static function getTeamAgentByID($id){
        $vendors = DB::table('teams')->where('id',$id)->first();
        return $vendors;
    }

    public static function MyFeatureHOSTHOME($vid){
        $vendors = DB::table('my_listing')->where('vid',$vid)->get();
        return $vendors;

    }
    public static function MyFeaturePropHOSTHOME($vid){
       // $vendors_data = DB::table('my_listing')->where('id', $id)->first();
        $vendors_data = DB::table('properties')
        ->join('property_details', 'properties.prop_id', '=', 'property_details.property_id')
        ->join('property_others_details', 'properties.prop_id', '=', 'property_others_details.property_id')
        ->join('property_pictures', 'properties.prop_id', '=', 'property_pictures.property_id')
        ->select('properties.*', 'property_details.*', 'property_others_details.*','property_pictures.*')
       ->where('properties.vid',$vid)        
        ->get();
        return $vendors_data;

    }
    

    public static function getContactLead($vid){
        $vendors = DB::table('contactus')->where('vid',$vid)->where('is_deleted',0)->limit(5)->get();
        return $vendors;
    }
    public static function valueable_leads($vid){
        $vendors = DB::table('valueable_leads')->where('vid',$vid)->where('is_deleted',0)->limit(5)->get();
        return $vendors;
    }
    public static function propValue_leads($vid){
        $vendors = DB::table('property_lead')->where('vid',$vid)->where('is_deleted',0)->limit(5)->get();
        return $vendors;
    }
    public static function newsCatbyVid($vid){
        $vendors = DB::table('news_category')->where('vid',$vid)->get();
        return $vendors;
    }

    public static function getProprtyCat(){
        $vendors = DB::table('propertycats')->get();
        return $vendors;
    }
    public static function getProprtyBYCat($catName){
        $vendors = DB::table('properties')->where('property_type',$catName)->get();
        return $vendors;
    }
    public static function getProprtyBYCatDetailBYPropID($prop_id){
        $vendors = DB::table('property_details')->where('property_id',$prop_id)->first();
        return $vendors;
    }
    public static function getProprtyBYCatPictureBYPropID($prop_id){
        $vendors = DB::table('property_pictures')->where('property_id',$prop_id)->first();
        return $vendors;
    }
    public static function getProprtyBYPropID($prop_id){
        $vendors = DB::table('properties')->where('prop_id',$prop_id)->first();
        return $vendors;
    }
    public static function getProprtyAgentBYPropID($prop_id){
        $vendors = DB::table('property_agents')->where('property_id',$prop_id)->first();
        return $vendors;
    }
    
    

    




    public static function getmyNewsByHOST_HOME($vid){
        $vendors = DB::table('news')->where('vid',$vid)->orderBy('start_date', 'DESC')->where('is_published',1)->where('is_deleted',0)->limit(10)->get();
        return $vendors;
    }
    public static function getmyOtherNewsByHOST_HOME($vid){
        $vendors = DB::table('news')->where('vid',$vid)->limit(10)->where('is_published',1)->where('is_deleted',0)->latest()->get();
        return $vendors;
    }




    public static function getVID()
    {
        $length = 10;
        $number = '';
        do {
            for ($i = $length; $i--; $i > 0) {
                $number .= mt_rand(0, 9);
            }
        } while (!empty(DB::table('vendors')->where('vid', $number)->first(['vid'])));
        return $number;
    }
    public static function getVendorbyDomain($web_url){
        $vendors = DB::table('vendors')->where('web_url',$web_url)->first();
        return $vendors;
    }
    public static function getContactData($vid){
        $vendors = DB::table('contacts')->where('vid',$vid)->first();
        return $vendors;
    }
    public static function getModules(){
       
            $vendors = DB::table('modules')->get();
       
           
       
        return $vendors;
    }
    public static function getPages(){
       
        $vendors = DB::table('pages')->get();
   
       
   
    return $vendors;
}
    

    public static function getVendorbyVID($vid){
        $vendors = DB::table('vendors')->where('vid',$vid)->first();
        return $vendors;
    }
    public static function getTeambyVID($vid){
        $vendors = DB::table('teams')->where('vid',$vid)->get();
        return $vendors;
    }


    public static function getWebSettings($vid){
        $webSettings = DB::table('websetting')->where('vid',$vid)->first();
        return $webSettings;
    }
}
