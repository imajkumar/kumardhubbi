"use strict";
var KTMAYRA_CUSTOM = {
    init: function() {
        //  For Add Edit vendor vendor-add-edit-action-submit
        var e, r, i = $("#kt_form_1");

        e = i.validate({
            ignore: ":hidden",
            rules: {
                name: {
                    required: !0,                   
                },
                email: {
                    required: !0,
                    email: !0,
                    minlength: 5
                },
                domain_url: {
                    required: !0
                },         
                
                phone: {
                    required: !0,
                    //phoneUS: !0
                },
            },
            messages: {
                name: "Enter your full name",
                email: "Enter valid email",
                domain_url: "Enter valid domain ",
                phone: "Enter valid phone ",
                
            },
            errorPlacement: function(e, r) {
                var i = r.closest(".input-group");
                i.length ? i.after(e.addClass("invalid-feedback")) : r.after(e.addClass("invalid-feedback"))
            },
            invalidHandler: function(e, r) {
                $("#kt_form_1_msg").removeClass("kt--hide").show(), KTUtil.scrollTop()
            },
            submitHandler: function(e) {}
        }),(i = e.find('[data-wizard-action="ajsubmit"]')).on("click", function (r) {
            r.preventDefault(), e.form() && (mApp.progress(n), i.ajaxSubmit({
              success: function () {
                mApp.unprogress(n), swal({
                  title: "",
                  text: "The Sample has been successfully added!",
                  type: "success",
                  confirmButtonClass: "btn btn-secondary m-btn m-btn--wide",
                  onClose: function (e) {
                    window.location.href = BASE_URL + '/ownbrand'
                  }
                })
      
              }
            }))
          })
                 
        //  For Add Edit vendor       
    }

};
jQuery(document).ready(function() {
    KTMAYRA_CUSTOM.init()
});







//-------------------
