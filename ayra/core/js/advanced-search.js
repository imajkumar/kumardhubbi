"use strict";
var KTDAYRASEARCH = function () {
    $.fn.dataTable.Api.register("column().title()", function () {
        return $(this.header()).text().trim()
    });
    return {
        //custom start 
        initA: function () {
            var t;
            t = $("#kt_table_1_vendorList").DataTable({
                responsive: !0,
                dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxVendorsList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "vid",
                            "name",
                            "email",
                            "phone",
                            "web_url",
                            "web_name",
                            "created_at",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "vid"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "email"
                    },
                    {
                        data: "phone"
                    },
                    {

                        data: "web_url"
                    },
                    {
                        data: "web_name"
                    },
                    {
                        data: "created_at"
                    },

                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],
                initComplete: function () {
                    this.api().columns().every(function () {
                        switch (this.title()) {
                            case "Country":
                                this.data().unique().sort().each(function (t, a) {
                                    $('.kt-input[data-col-index="2"]').append('<option value="' + t + '">' + t + "</option>")
                                });
                                break;
                            case "Status":
                                var t = {
                                    1: {
                                        title: "Pending",
                                        class: "kt-badge--brand"
                                    },
                                    2: {
                                        title: "Delivered",
                                        class: " kt-badge--danger"
                                    },
                                    3: {
                                        title: "Canceled",
                                        class: " kt-badge--primary"
                                    },
                                    4: {
                                        title: "Success",
                                        class: " kt-badge--success"
                                    },
                                    5: {
                                        title: "Info",
                                        class: " kt-badge--info"
                                    },
                                    6: {
                                        title: "Danger",
                                        class: " kt-badge--danger"
                                    },
                                    7: {
                                        title: "Warning",
                                        class: " kt-badge--warning"
                                    }
                                };
                                this.data().unique().sort().each(function (a, e) {
                                    $('.kt-input[data-col-index="6"]').append('<option value="' + a + '">' + t[a].title + "</option>")
                                });
                                break;
                            case "Type":
                                t = {
                                    1: {
                                        title: "Online",
                                        state: "danger"
                                    },
                                    2: {
                                        title: "Retail",
                                        state: "primary"
                                    },
                                    3: {
                                        title: "Direct",
                                        state: "success"
                                    }
                                }, this.data().unique().sort().each(function (a, e) {
                                    $('.kt-input[data-col-index="7"]').append('<option value="' + a + '">' + t[a].title + "</option>")
                                })
                        }
                    })
                },
                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-vendor/" + e.vid;
                        return `<span class="dropdown">
                        <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                        <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 
                       
                         </div>
                    </span>
                    `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                    }
                }, {
                    targets: 6,
                    render: function (t, a, e, n) {
                        var i = {
                            1: {
                                title: "Pending",
                                class: "kt-badge--brand"
                            },
                            2: {
                                title: "Delivered",
                                class: " kt-badge--danger"
                            },
                            3: {
                                title: "Canceled",
                                class: " kt-badge--primary"
                            },
                            4: {
                                title: "Success",
                                class: " kt-badge--success"
                            },
                            5: {
                                title: "Info",
                                class: " kt-badge--info"
                            },
                            6: {
                                title: "Danger",
                                class: " kt-badge--danger"
                            },
                            7: {
                                title: "Warning",
                                class: " kt-badge--warning"
                            }
                        };
                        return void 0 === i[t] ? t : '<span class="kt-badge ' + i[t].class + ' kt-badge--inline kt-badge--pill">' + i[t].title + "</span>"
                    }
                }, {
                    targets: 7,
                    render: function (t, a, e, n) {
                        var i = {
                            1: {
                                title: "Online",
                                state: "danger"
                            },
                            2: {
                                title: "Retail",
                                state: "primary"
                            },
                            3: {
                                title: "Direct",
                                state: "success"
                            }
                        };
                        return void 0 === i[t] ? t : '<span class="kt-badge kt-badge--' + i[t].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + i[t].state + '">' + i[t].title + "</span>"
                    }
                }]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        //custom start , new inti
        //page list
        initB: function () {
            var t;
            t = $("#kt_table_1_page_List").DataTable({
                responsive: !0,
                dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxPagesList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "title",
                            "module_name",
                            "is_published",

                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "title"
                    },
                    {
                        data: "module_name"
                    },
                    {
                        data: "is_published"
                    },


                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-vendor/" + e.vid;
                        return `<span class="dropdown">
                        <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                        <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a> 
                       
                         </div>
                    </span>
                    `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                    }
                }, {
                    targets: 3,
                    render: function (t, a, e, n) {
                        var i = {
                            0: {
                                title: "PUBLISH",
                                class: "kt-badge--brand"
                            },
                            1: {
                                title: "UNPUBLISH",
                                class: " kt-badge--danger"
                            },

                        };
                        return void 0 === i[t] ? t : '<span class="kt-badge ' + i[t].class + ' kt-badge--inline kt-badge--pill">' + i[t].title + "</span>"
                    }
                }
                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        //page list
        //  kt_table_1_vendor_static_block_List
        initC: function () {
            var t;
            t = $("#kt_table_1_vendor_static_block_List").DataTable({
                // responsive: !0,
                // dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxVendorStaticBlockList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "title",
                            "module_name",
                            "is_published",
                            "created_by",

                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "title"
                    },
                    {
                        data: "module_name"
                    },
                    {
                        data: "is_published"
                    },
                    {
                        data: "created_by"
                    },

                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-static-block/" + e.RecordID;
                        return `<span class="dropdown">
                        <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                        <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 
                       
                         </div>
                    </span>
                    `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                    }
                }, {
                    targets: 3,
                    render: function (t, a, e, n) {
                        var i = {
                            2: {
                                title: "PUBLISH",
                                class: "kt-badge--brand"
                            },
                            1: {
                                title: "UNPUBLISH",
                                class: " kt-badge--danger"
                            },

                        };
                        return void 0 === i[t] ? t : '<span class="kt-badge ' + i[t].class + ' kt-badge--inline kt-badge--pill">' + i[t].title + "</span>"
                    }
                }
                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        initCAJ: function () {
            var t;
            t = $("#kt_table_1_vendor_banner_List").DataTable({
                //responsive: !0,
                //dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxVendorBannerList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "banner_name",
                            "photo",
                            "created_at",
                            "status",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "banner_name"
                    },
                    {
                        data: "photo"
                    },
                    {
                        data: "created_at"
                    },
                    {
                        data: "status"
                    },

                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-banner/" + e.RecordID;
                        return `<span class="dropdown">
                        <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                        <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 
                       
                         </div>
                    </span>
                    `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                    }
                },
                {
                    targets: 2,
                    render: function (t, a, e, n) {
                        return `<img src="${e.photo}" width="100" height="100">`
                     }
                     },
                      {
                    targets: 4,
                    render: function (t, a, e, n) {
                        var i = {
                            2: {
                                title: "PUBLISH",
                                class: "kt-badge--brand"
                            },
                            1: {
                                title: "UNPUBLISH",
                                class: " kt-badge--danger"
                            },

                        };
                        return void 0 === i[t] ? t : '<span class="kt-badge ' + i[t].class + ' kt-badge--inline kt-badge--pill">' + i[t].title + "</span>"
                    }
                }
                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },

        //  kt_table_1_vendor_static_block_List
        initD: function () {
            var t;
            t = $("#kt_table_1_category_List").DataTable({
                //responsive: !0,
                //dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxVendorNewsCategory',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "title",
                            "news_short_discription",
                            "news_discription",
                            "is_published",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "title"
                    },
                    {
                        data: "news_short_discription"
                    },
                    {
                        data: "news_discription"
                    },

                    {
                        data: "is_published"
                    },



                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-news-category/" + e.RecordID;
                        return `<span class="dropdown">
                        <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                        <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 
                       
                         </div>
                    </span>
                    <a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},1)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>

                    `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                    }
                }, {
                    targets: 4,
                    render: function (t, a, e, n) {
                        var ischeck = "";
                        if (e.is_published == 2) {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},5)"   name="">
                            <span></span>
                        </label>
                    </span>`;

                        } else {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input checked type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},5)"   name="">
                            <span></span>
                        </label>
                    </span>`;
                        }

                    }
                },
                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        //kt_table_1_property_List
        initE: function () {
            var t;
            t = $("#kt_table_1_property_List").DataTable({
                //responsive: !0,
                ///dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxPropertyList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "name",
                            "listing_name",
                            "source",
                            "property_details",
                            "property_email",
                            "property_message",
                            "property_created_on",
                            "picture_url",
                            "is_featured",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "listing_name"
                    },
                    {
                        data: "source"
                    },


                    {
                        data: "is_featured"
                    },





                ],

                columnDefs: [

                    {
                        targets: 1,
                        title: "IMAGE",
                        orderable: !1,
                        render: function (t, a, e, n) {
                            var editURL = BASE_URL + "/edit-news-category/" + e.RecordID;
                            return `
                        <img src="${e.picture_url}" width="80">
                        `

                        }
                    },
                    {
                        targets: 4,
                        render: function (t, a, e, n) {
                            var ischeck = "";
                            if (e.is_featured == 2) {
                                return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},10)"   name="">
                            <span></span>
                        </label>
                    </span>`;

                            } else {
                                return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input checked type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},10)"   name="">
                            <span></span>
                        </label>
                    </span>`;
                            }

                        }
                    },


                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        //kt_table_1_property_List
        // kt_table_1_contact_lead
        initF: function () {
            var t;
            t = $("#kt_table_1_contact_lead").DataTable({
                responsive: !0,
                dom: 'Bfrltip',
                lengthMenu: [5, 10, 25, 50],
                pageLength: 5,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxContactList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "first_name",
                            "last_name",
                            "email",
                            "phone",
                            "message",
                            "created_at",
                            "status",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "RecordID"
                    },
                    {
                        data: "first_name"
                    },
                    {
                        data: "last_name"
                    },
                    {
                        data: "email"
                    },

                    {
                        data: "phone"
                    },
                    {
                        data: "message"
                    },
                    {
                        data: "created_at"
                    },
                    




                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': false
                        }
                    },
                   
                    {
                        targets: -1,
                        title: "Actions",
                        orderable: !1,
                        render: function (t, a, e, n) {
                            var editURL = BASE_URL + "/edit-news-category/" + e.RecordID;
                            return `<a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},6)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>`
                            //     return `<span class="dropdown">
                            //     <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                            //     <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                            //      </div>
                            // </span>
                            // `;
                            // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                            // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                            // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                        }
                    },

                    {
                        targets: 6,
                        render: function (t, a, e, n) {
                            return `<a href="javascript:void(0)" data-propmsg="ddd" onclick="showProMessage(${e.RecordID})" > <span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded">View</span></a>`;
                        }
                    }
                    

                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }),
                //---------------------------------------
                $('#btngetAllUserSelected').click(function () {
                    var table = $('#kt_table_1_contact_lead').DataTable();
                    var rows_selected = table.column(0).checkboxes.selected();
                    var arr = [];
                    var i = 0;
                    $.each(rows_selected, function (index, rowId) {

                        arr[i++] = rowId;
                    });
                    //ajax
                    var action_id = $("#action_id option:selected").val();
                    var formData = {
                        
                        'action_id': action_id,
                        'rows_selected': arr,
                        'action': 1,
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    };
                    $.ajax({
                        url: BASE_URL + '/setAllcheckboxAction',
                        type: 'POST',
                        data: formData,
                        success: function (res) {
                            swal.fire("Good job!", "Deleted Successfully", "success");
                            location.reload();

                        }
                    });

                    //ajax


                })
            //-----------------------------------------------

        },
        initFRental: function () {
            var t;
            t = $("#kt_table_1_Rental_lead").DataTable({
                responsive: !0,
                dom: 'Bfrltip',
                lengthMenu: [5, 10, 25, 50],
                pageLength: 5,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxRentalList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "first_name",
                            "last_name",
                            "email",
                            "phone",
                            "message",
                            "created_at",
                            "status",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "RecordID"
                    },
                    {
                        data: "first_name"
                    },
                    {
                        data: "last_name"
                    },
                    {
                        data: "email"
                    },

                    {
                        data: "phone"
                    },
                    {
                        data: "message"
                    },
                    {
                        data: "created_at"
                    },
                   



                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': false
                        }
                    },
                    {
                        targets: -1,
                        title: "Actions",
                        orderable: !1,
                        render: function (t, a, e, n) {
                            var editURL = BASE_URL + "/edit-news-category/" + e.RecordID;
                            return `<a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},50)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>`
                            //     return `<span class="dropdown">
                            //     <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                            //     <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                            //      </div>
                            // </span>
                            // `;
                            // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                            // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                            // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                        }
                    },

                    {
                        targets: 6,
                        render: function (t, a, e, n) {
                            return `<a href="javascript:void(0)" data-propmsg="ddd" onclick="showProMessage(${e.RecordID})" > <span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded">View</span></a>`;
                        }
                    }
                   

                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }),
                //---------------------------------------
                $('#btngetAllUserSelectedS').click(function () {
                    var table = $('#kt_table_1_Rental_lead').DataTable();
                    var rows_selected = table.column(0).checkboxes.selected();
                    var arr = [];
                    var i = 0;
                    $.each(rows_selected, function (index, rowId) {

                        arr[i++] = rowId;
                    });
                    //ajax
                    var action_id = $("#action_id option:selected").val();
                    var formData = {
                        
                        'action_id': action_id,
                        'rows_selected': arr,
                        'action': 1,
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    };
                    $.ajax({
                        url: BASE_URL + '/setAllcheckboxAction1',
                        type: 'POST',
                        data: formData,
                        success: function (res) {
                            swal.fire("Good job!", "Deleted Successfully", "success");
                            location.reload();

                        }
                    });

                    //ajax


                })
            //-----------------------------------------------

        },

        //kt_table_1_contact_value_lead
        initG: function () {
            var t;
            t = $("#kt_table_1_contact_value_lead").DataTable({
                //responsive: !0,
                //dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 5,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxContactValueList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "name",
                            "email",
                            "telephone",
                            "address",
                            "address",
                            "status",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "email"
                    },
                    {
                        data: "telephone"
                    },

                    {
                        data: "address"
                    },
                    {
                        data: "address"
                    },

                    {
                        data: "status"
                    },




                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-news-category/" + e.RecordID;
                        return `<a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},8)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>`;
                        //     return `<span class="dropdown">
                        //     <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                        //     <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                        //      </div>
                        // </span>
                        // `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                    }
                },

                {
                    targets: 5,
                    render: function (t, a, e, n) {
                        return `<a href="javascript:void(0)" data-propmsg="ddd" onclick="showProMessageLV(${e.RecordID})" > <span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded">View</span></a>`;
                    }
                },
                {
                    targets: 6,
                    render: function (t, a, e, n) {
                        if (e.status == 2) {
                            return `<a href="javascript:void(0)"  onclick="shSowSProMessage(${e.RecordID})"> <span class="kt-badge kt-badge--primary kt-badge--inline kt-badge--pill kt-badge--rounded">New</span></a>`;
                        } else {
                            return `<a href="javascript:void(0)" onclick="showPSSroMessage(${e.RecordID})"> <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded">Read</span></a>`;
                        }

                    }
                }

                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },

        //kt_table_1_contact_value_lead
        // kt_table_1_community_List
        initH: function () {
            var t;
            t = $("#kt_table_1_community_List").DataTable({
                //responsive: !0,
                //dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxCommunityList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "photo",
                            "name",
                            "info_data",

                            "is_published",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },
                    {
                        data: "photo"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "info_data"
                    },


                    {
                        data: "is_published"
                    },

                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-community/" + e.RecordID;

                        return `<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                            <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                             </div>
                        </span>
                        <a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},3)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>

                        `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                    }
                },
                {
                    targets: 4,
                    render: function (t, a, e, n) {
                        var ischeck = "";
                        if (e.is_published == 2) {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},3)"   name="">
                            <span></span>
                        </label>
                    </span>`;

                        } else {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input checked type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},3)"   name="">
                            <span></span>
                        </label>
                    </span>`;
                        }

                    }
                },
                {
                    targets: 1,
                    render: function (t, a, e, n) {
                        return `<img src="${e.photo}" alt="" width="45px">`;

                    }
                },





                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        // kt_table_1_news_List
        initIK: function () {
            var t;

            t = $("#kt_table_1_news_List").DataTable({
                //responsive: !0,
                //dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxNewsList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "title",
                            "category",
                            "start_date",
                            "photo",
                            "info",
                            "status",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },

                    {
                        data: "title"
                    },
                    {
                        data: "photo"
                    },
                    {
                        data: "info"
                    },

                    {
                        data: "category"
                    },
                    {
                        data: "status"
                    },

                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-news/" + e.RecordID;
                        // return '';
                        return `<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                            <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                             </div>
                        </span>
                        <a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},9)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>

                        `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>

                    }
                },
                {
                    targets: 5,
                    render: function (t, a, e, n) {
                        var ischeck = "";
                        if (e.status == 2) {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},6)"   name="">
                            <span></span>
                        </label>
                    </span>`;

                        } else {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input checked type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},6)"   name="">
                            <span></span>
                        </label>
                    </span>`;
                        }

                    }
                },
                {
                    targets: 2,
                    render: function (t, a, e, n) {
                        return `<img src="${e.photo}" alt="" width="45px">`;

                    }
                },





                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {
                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        //----------------
        initIL: function () {

            var t;

            t = $("#kt_table_2_testimonial_List").DataTable({
                //responsive: !0,
                //dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxTestimonialList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "title",
                            "created_by",
                            "info_data",
                            "status",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },

                    {
                        data: "title"
                    },
                    {
                        data: "created_by"
                    },
                    {
                        data: "info_data"
                    },


                    {
                        data: "status"
                    },

                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-testimonial/" + e.RecordID;
                        // return '';
                        return `<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                            <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                             </div>
                        </span>
                        <a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},2)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>
                        `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>

                    }
                },
                {
                    targets: 4,
                    render: function (t, a, e, n) {
                        var ischeck = "";
                        if (e.status == 2) {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},2)"   name="">
                            <span></span>
                        </label>
                    </span>`;

                        } else {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input checked type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},2)"   name="">
                            <span></span>
                        </label>
                    </span>`;
                        }

                    }
                },







                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {






                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        // kt_table_1_testimonial_List kt_table_2_designation_List
        initIM: function () {

            var t;

            t = $("#kt_table_2_designation_List").DataTable({
                //responsive: !0,
                //dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxDesignationList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "name",
                            "order_no",
                            "status",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },

                    {
                        data: "name"
                    },
                    {
                        data: "order_no"
                    },
                    {
                        data: "status"
                    },

                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-designation/" + e.RecordID;
                        // return '';
                        return `<span class="dropdown">
                           <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                           <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                            </div>
                       </span>
                       `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>

                    }
                },
                {
                    targets: 3,
                    render: function (t, a, e, n) {
                        var ischeck = "";
                        if (e.status == 2) {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},4)"   name="">
                            <span></span>
                        </label>
                    </span>`;

                        } else {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input checked type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},4)"   name="">
                            <span></span>
                        </label>
                    </span>`;
                        }

                    }
                },







                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {






                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        //    kt_table_2_designation_List
        // kt_table_2_team_List
        initIN: function () {

            var t;

            t = $("#kt_table_2_team_List").DataTable({
                //responsive: !0,
                //dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxTeamList',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "name",
                            "agent_no",
                            "designation",
                            "is_published",
                            "email",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },

                    {
                        data: "name"
                    },
                    {
                        data: "agent_no"
                    },
                    {
                        data: "designation"
                    },
                    {
                        data: "email"
                    },
                    {
                        data: "is_published"
                    },


                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-team/" + e.RecordID;
                        // return '';
                        return `<span class="dropdown">
                        <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                        <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                         </div>
                    </span>
                    <a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},4)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>

                    `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>

                    }
                },
                {
                    targets: 5,
                    render: function (t, a, e, n) {
                        var ischeck = "";
                        if (e.is_published == 2) {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},11)"   name="">
                            <span></span>
                        </label>
                    </span>`;

                        } else {
                            return `<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input checked type="checkbox" id="${e.RecordID}" onclick="toggleWeather(${e.RecordID},11)"   name="">
                            <span></span>
                        </label>
                    </span>`;
                        }

                    }
                },







                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {






                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        //kt_table_1_MYProperty_List
        initIO: function () {

            var t;

            t = $("#kt_table_1_MYProperty_List").DataTable({
                responsive: !0,
               // dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxMyListingData',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "photo",
                            "property_name",
                            "mlsid",
                            "exp_date",
                            "property_type",
                            "status",
                            "publish",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },

                    {
                        data: "photo"
                    },
                    {
                        data: "property_name"
                    },
                    {
                        data: "mlsid"
                    },
                    {
                        data: "exp_date"
                    },
                    {
                        data: "property_type"
                    },
                    {
                        data: "status"
                    },
                    {
                        data: "publish"
                    },


                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-my-list/" + e.RecordID;
                        // return '';
                        return `<span class="dropdown">
                        <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                        <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                         </div>
                    </span>
                    <a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},5)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>

                    `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>

                    }
                },
                {
                    targets: 1,
                    render: function (t, a, e, n) {
                        return `<img src="${e.photo}" width="85">`;
                    }
                },
                
                {
                    targets: 6,
                    render: function (t, a, e, n) {
                        var i = {
                            1: {
                                title: "PUBLISH",
                                class: "kt-badge--brand"
                            },
                            6: {
                                title: "UNPUBLISH",
                                class: " kt-badge--danger"
                            },

                        };
                        return void 0 === i[t] ? t : '<span class="kt-badge ' + i[t].class + ' kt-badge--inline kt-badge--pill">' + i[t].title + "</span>"
                    }
                },
                {
                    targets: 7,
                    render: function (t, a, e, n) {
                        //return e.publish;
                        var i = {
                            1: {
                                title: "PUBLISH",
                                class: "kt-badge--brand"
                            },
                            2: {
                                title: "UNPUBLISH",
                                class: " kt-badge--danger"
                            },

                        };
                        return void 0 === i[t] ? t : '<span class="kt-badge ' + i[t].class + ' kt-badge--inline kt-badge--pill">' + i[t].title + "</span>"

                    }
                }







                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {






                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        initIPQ: function () {

            var t;

            t = $("#kt_table_1_MYProperty_List_1").DataTable({
                responsive: !0,
                // dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 5,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {

                    url: BASE_URL + '/getAjaxMypropertyData',
                    type: "POST",
                    data: {
                        columnsDef: [
                            "RecordID",
                            "first_name",
                            "last_name",
                            "email",
                            "phone",
                            "message",
                            "created_at",
                            "Actions"],
                        '_token': $('meta[name="csrf-token"]').attr('content')

                    }
                },
                columns: [
                    {
                        data: "RecordID"
                    },

                    {
                        data: "first_name"
                    },
                    {
                        data: "last_name"
                    },
                    {
                        data: "email"
                    },
                    {
                        data: "phone"
                    },
                    {
                        data: "message"
                    },
                    {
                        data: "created_at"
                    },



                    {
                        data: "Actions",
                        responsivePriority: -1
                    }],

                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (t, a, e, n) {
                        var editURL = BASE_URL + "/edit-property-lead/" + e.RecordID;
                        return `<a href="javascript:void(0)" onclick="deleteMe(${e.RecordID},7)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="la la-trash"></i> </a>`

                        return `<span class="dropdown">
                        <a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> <i class="la la-ellipsis-h"></i> </a>
                        <div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="${editURL}"><i class="la la-edit"></i> Edit Details</a> 

                         </div>
                    </span>
                    `;
                        // <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-edit"></i> </a>
                        // <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a> 
                        // <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>

                    }
                },








                ]
            }), $("#kt_search").on("click", function (a) {
                a.preventDefault();
                var e = {};
                $(".kt-input").each(function () {
                    var t = $(this).data("col-index");
                    e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                }), $.each(e, function (a, e) {
                    t.column(a).search(e || "", !1, !1)
                }), t.table().draw()
            }), $("#kt_reset").on("click", function (a) {






                a.preventDefault(), $(".kt-input").each(function () {
                    $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                }), t.table().draw()
            }), $("#kt_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        },
        //kt_table_1_MYProperty_List


    }
}();



function toggleWeather(row, pageID) {
    var data = "#" + row;
    var myId = $(this).attr('id');
    var publish = 0;

    if ($(data).prop("checked") == true) {
        publish = 1;
    }
    else {
        publish = 0;
    }

    var formData = {
        'publish': publish,
        'pageID': pageID,
        'row': row,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };
    $.ajax({
        url: BASE_URL + '/getSetPublishMyListing',
        type: 'POST',
        data: formData,
        success: function (res) {
            swal.fire({
                title: "",
                text: "Successfully submitted!",
                type: "success",
                confirmButtonClass: "btn btn-secondary",
                onClose: function (e) {
                    //window.location.assign(_redirect);  
                    location.reload(1);
                }
            })

        }
    });



}



//btnPasswordReset
$('#btnPasswordReset').click(function () {
    var curr_pass = $('#current').val();
    var new_pass = $('#password').val();
    var confirm_pass = $('#confirmed').val();
    if (curr_pass == "") {
        alert('Invalid Input');
        return false;
    }
    if (new_pass == "") {
        alert('Invalid Input');
        return false;
    }
    if (confirm_pass == "") {
        alert('Invalid Input');
        return false;
    }
    if (confirm_pass != new_pass) {
        alert('Invalid Input');
        return false;
    }
    var formData = {
        'current': curr_pass,
        'password': new_pass,
        'confirmed': confirm_pass,
        '_token': $('meta[name="csrf-token"]').attr('content'),
        'user_id': $('meta[name="UUID"]').attr('content'),

    };
    $.ajax({
        url: BASE_URL + '/UserResetPassword',
        type: 'POST',
        data: formData,
        success: function (res) {
            if (res.status == 1) {

                alert('Password successfully changed');
            }
            if (res.status == 2) {

                alert('Your current password does not matches with the password you provided');

            }
            if (res.status == 3) {

                alert('New Password cannot be same as your current password. Please choose a different password..');

            }

        }

    });




});
//btnPasswordReset



function showProDetail(prop_id) {

    $('#kt_modal_4_viewProp').modal('toggle');

}
function showProMessageLV(rowid) {
    var formData = {
        'rowid': rowid,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };
    $.ajax({
        url: BASE_URL + '/getValueLeadDetails',
        type: 'POST',
        data: formData,
        success: function (res) {
            $('.viewLead').html(res);
            $('#kt_modal_1_2_VLEAD').modal('toggle');
        }
    });
}

function showProMessage(prop_id) {

    var formData = {
        'prop_id': prop_id,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };
    $.ajax({
        url: BASE_URL + '/getContactMessage',
        type: 'POST',
        data: formData,
        success: function (res) {


            if (res.status == 1) {
                //location.reload();
                console.log(res.data);
                $('#showmemsg').html(res.data.message);

                $('#kt_modal_4_viewPropContact_message').modal('toggle');

            }

        }
    });



}

function deleteMe(rowid, option) {

    swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!'
    }).then(function (result) {
        if (result.value) {

            var formData = {
                'rowid': rowid,
                'option': option,
                '_token': $('meta[name="csrf-token"]').attr('content')
            };
            $.ajax({
                url: BASE_URL + '/deleteAction',
                type: 'POST',
                data: formData,
                success: function (res) {

                    swal.fire({
                        title: "",
                        text: "Deleted",
                        type: "success",
                        confirmButtonClass: "btn btn-secondary",
                        onClose: function (e) {
                            //window.location.assign(_redirect);  
                            location.reload(1);
                        }
                    })




                }
            });

        }
    });

}

jQuery(document).ready(function () {
    KTDAYRASEARCH.initA();
    KTDAYRASEARCH.initB();
    KTDAYRASEARCH.initC();
    KTDAYRASEARCH.initCAJ();
    KTDAYRASEARCH.initD();
    KTDAYRASEARCH.initE();
    KTDAYRASEARCH.initF();
    KTDAYRASEARCH.initFRental();
    KTDAYRASEARCH.initG();
    KTDAYRASEARCH.initH();
    KTDAYRASEARCH.initIK();
    KTDAYRASEARCH.initIL();
    KTDAYRASEARCH.initIM();
    KTDAYRASEARCH.initIN();
    KTDAYRASEARCH.initIO();
    KTDAYRASEARCH.initIPQ();


});




var KTBootstrapMaxlength = function () {

    // Private functions
    var demos = function () {
        // minimum setup
        $('#kt_maxlength_1').maxlength({
            alwaysShow: true,
            warningClass: "kt-badge kt-badge--warning kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--success kt-badge--rounded kt-badge--inline"
        });

        // threshold value
        $('#kt_maxlength_2').maxlength({
            threshold: 5,
            alwaysShow: true,
            warningClass: "kt-badge kt-badge--danger kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--success kt-badge--rounded kt-badge--inline"
        });

        // always show
        $('#kt_maxlength_3').maxlength({
            alwaysShow: true,
            threshold: 5,
            warningClass: "kt-badge kt-badge--primary kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
        });

        // custom text
        $('#kt_maxlength_4').maxlength({
            alwaysShow: true,
            threshold: 3,
            warningClass: "kt-badge kt-badge--danger kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--success kt-badge--rounded kt-badge--inline",
            separator: ' of ',
            preText: 'You have ',
            postText: ' chars remaining.',
            validate: true
        });

        // textarea example
        $('#kt_maxlength_5').maxlength({
            alwaysShow: true,
            threshold: 5,
            warningClass: "kt-badge kt-badge--primary kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
        });

        // position examples
        $('#kt_maxlength_6_1').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'top-left',
            warningClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
        });

        $('#kt_maxlength_6_2').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'top-right',
            warningClass: "kt-badge kt-badge--success kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
        });

        $('#kt_maxlength_6_3').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'bottom-left',
            warningClass: "kt-badge kt-badge--warning kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
        });

        $('#kt_maxlength_6_4').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'bottom-right',
            warningClass: "kt-badge kt-badge--danger kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
        });

        // Modal Examples

        // minimum setup
        $('#kt_maxlength_1_modal').maxlength({
            warningClass: "kt-badge kt-badge--warning kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--success kt-badge--rounded kt-badge--inline",
            appendToParent: true
        });

        // threshold value
        $('#kt_maxlength_2_modal').maxlength({
            threshold: 5,
            warningClass: "kt-badge kt-badge--danger kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--success kt-badge--rounded kt-badge--inline",
            appendToParent: true
        });

        // always show
        // textarea example
        $('#kt_maxlength_5_modal').maxlength({
            threshold: 5,
            warningClass: "kt-badge kt-badge--primary kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline",
            appendToParent: true
        });

        // custom text
        $('#kt_maxlength_4_modal').maxlength({
            threshold: 3,
            warningClass: "kt-badge kt-badge--danger kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--success kt-badge--rounded kt-badge--inline",
            appendToParent: true,
            separator: ' of ',
            preText: 'You have ',
            postText: ' chars remaining.',
            validate: true
        });
    }

    return {
        // public functions
        init: function () {
            demos();
        }
    };
}();

jQuery(document).ready(function () {
    KTBootstrapMaxlength.init();
});



// Class definition
var KTSelect2 = function () {
    // Private functions
    var demos = function () {
        // basic
        $('#kt_select2_1, #kt_select2_1_validate').select2({
            placeholder: "Select a state"
        });

        // nested
        $('#kt_select2_2, #kt_select2_2_validate').select2({
            placeholder: "Select a state"
        });

        // multi select
        $('#kt_select2_3, #kt_select2_3_validate').select2({
            placeholder: "Select a state",
        });

        // basic
        $('#kt_select2_4').select2({
            placeholder: "Select a state",
            allowClear: true
        });

        // loading data from array
        var data = [{
            id: 0,
            text: 'Enhancement'
        }, {
            id: 1,
            text: 'Bug'
        }, {
            id: 2,
            text: 'Duplicate'
        }, {
            id: 3,
            text: 'Invalid'
        }, {
            id: 4,
            text: 'Wontfix'
        }];

        $('#kt_select2_5').select2({
            placeholder: "Select a value",
            data: data
        });

        // loading remote data

        function formatRepo(repo) {
            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";
            if (repo.description) {
                markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
            }
            markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";
            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.full_name || repo.text;
        }

        $("#kt_select2_6").select2({
            placeholder: "Search for git repositories",
            allowClear: true,
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        // custom styles

        // tagging support
        $('#kt_select2_12_1, #kt_select2_12_2, #kt_select2_12_3, #kt_select2_12_4').select2({
            placeholder: "Select an option",
        });

        // disabled mode
        $('#kt_select2_7').select2({
            placeholder: "Select an option"
        });

        // disabled results
        $('#kt_select2_8').select2({
            placeholder: "Select an option"
        });

        // limiting the number of selections
        $('#kt_select2_9').select2({
            placeholder: "Select an option",
            maximumSelectionLength: 2
        });

        // hiding the search box
        $('#kt_select2_10').select2({
            placeholder: "Select an option",
            minimumResultsForSearch: Infinity
        });

        // tagging support
        $('#kt_select2_11').select2({
            placeholder: "Add a tag",
            tags: true
        });

        // disabled results
        $('.kt-select2-general').select2({
            placeholder: "Select an option"
        });
    }

    var modalDemos = function () {
        $('#kt_select2_modal').on('shown.bs.modal', function () {
            // basic
            $('#kt_select2_1_modal').select2({
                placeholder: "Select a state"
            });

            // nested
            $('#kt_select2_2_modal').select2({
                placeholder: "Select a state"
            });

            // multi select
            $('#kt_select2_3_modal').select2({
                placeholder: "Select a state",
            });

            // basic
            $('#kt_select2_4_modal').select2({
                placeholder: "Select a state",
                allowClear: true
            });
        });
    }

    // Public functions
    return {
        init: function () {
            demos();
            modalDemos();
        }
    };
}();

// Initialization
jQuery(document).ready(function () {
    KTSelect2.init();
});


