"use strict";
var AYRA_FORM_SUBMIT = function() {
  
    return {
        init: function() {
            
            var e, r, t;
           

            var i;
            
            function CKupdate(){
                for ( instance in CKEDITOR.instances )
                    CKEDITOR.instances[instance].updateElement();
            }

            e = $("#kt_form_1");
             r = e.validate({
                ignore: ":hidden",
                 ignore: [],
              debug: false,
                rules: {
                    name: {
                        required: !0,                   
                    },
                    email: {
                        required: !0,
                        email: !0,
                        minlength: 5
                    },
                    domain_url: {
                        required: !0
                    },         
                    
                    phone: {
                        required: !0,
                        //phoneUS: !0
                    },
                   
                    page_title: {
                        required: !0,
                        //phoneUS: !0
                    },
                    pages_module: {
                        required: !0,
                        //phoneUS: !0
                    },
                   
                    page_meta_title: {
                        required: !0,
                        //phoneUS: !0
                    },
                    page_meta_keywords: {
                        required: !0,
                        //phoneUS: !0
                    },
                    page_meta_discription: {
                        required: !0,
                        //phoneUS: !0
                    },
                   
                    rdo_page_status: {
                        required: true
                    }
                    

                    
                    
                },
                messages: {
                    name: "Enter your full name",
                    email: "Enter valid email",
                    domain_url: "Enter valid domain ",
                    phone: "Enter valid phone ",
                    rdo_page_status: "Please choose ",
                    
                    
                
                },
                // errorPlacement: function(e, r) {
                //    // var i = r.closest(".input-group");
                //     //i.length ? i.after(e.addClass("invalid-feedback")) : r.after(e.addClass("invalid-feedback"))
                // },
                invalidHandler: function(e, r) {                   
                    $("#kt_form_1_msg").removeClass("kt--hide").show(), KTUtil.scrollTop()

                },
                submitHandler: function(e) {}
            }), (i = e.find('[data-ktwizard-type="action-submit"]')).on("click", function(t) {
                var _redirect = BASE_URL+"/"+e.data("redirect");

              
                t.preventDefault(), r.form() && (KTApp.progress(i), e.ajaxSubmit({
                   
                    success: function(resp) {
                        if(resp.status==1){
                            KTApp.unprogress(i), swal.fire({
                                title: "",
                                text: "Successfully submitted!",
                                type: "success",
                                confirmButtonClass: "btn btn-secondary",
                                onClose: function(e) {                              
                                    window.location.assign(_redirect);  
                                  }
                            })
                        }else{
                            KTApp.unprogress(i), swal.fire({
                                title: "",
                                text: resp.message,
                                type: "error",
                                confirmButtonClass: "btn btn-secondary",
                                onClose: function(e) {                              
                                    window.location.assign(_redirect);  
                                  }
                            })

                        }

                        
                    },
                    'dataType':'json'
                }))
                
            })
        },
        //kt_form_1_web_settigs_admin
        init_admin_web_setting: function() {
            
          
            var e, r, t;

            var i;
            e = $("#kt_form_1_web_settigs_admin");
             r = e.validate({
                ignore: ":hidden",
                rules: {
                    meta_keywords: {
                        required: !0,                   
                    },
                    meta_discription: {
                        required: !0,                   
                    },
                    // email: {
                    //     required: !0,
                    //     email: !0,
                    //     minlength: 5
                    // },
                    // domain_url: {
                    //     required: !0
                    // },         
                    
                    // phone: {
                    //     required: !0,
                    //     //phoneUS: !0
                    // },
                },
                messages: {
                    name: "Enter your full name",
                    email: "Enter valid email",
                    domain_url: "Enter valid domain ",
                    phone: "Enter valid phone ",
                    
                
                },
                // errorPlacement: function(e, r) {
                //    // var i = r.closest(".input-group");
                //     //i.length ? i.after(e.addClass("invalid-feedback")) : r.after(e.addClass("invalid-feedback"))
                // },
                invalidHandler: function(e, r) {                   
                    $("#kt_form_1_msg").removeClass("kt--hide").show(), KTUtil.scrollTop()

                },
                submitHandler: function(e) {}
            }), (i = e.find('[data-ktwizard-type="action-submit"]')).on("click", function(t) {
                var _redirect = BASE_URL+"/"+e.data("redirect");

                t.preventDefault(), r.form() && (KTApp.progress(i), e.ajaxSubmit({
                    success: function(resp) {
                        if(resp.status==1){
                            KTApp.unprogress(i), swal.fire({
                                title: "",
                                text: "Successfully submitted!",
                                type: "success",
                                confirmButtonClass: "btn btn-secondary",
                                onClose: function(e) {                              
                                    window.location.assign(_redirect);  
                                  }
                            })
                        }else{
                            KTApp.unprogress(i), swal.fire({
                                title: "",
                                text: resp.message,
                                type: "error",
                                confirmButtonClass: "btn btn-secondary",
                                onClose: function(e) {                              
                                    window.location.assign(_redirect);  
                                  }
                            })

                        }

                        
                    },
                    'dataType':'json'
                }))
            })
        },
        //kt_form_1_web_settigs_admin
    }
}();
jQuery(document).ready(function() {
    AYRA_FORM_SUBMIT.init();
   AYRA_FORM_SUBMIT.init_admin_web_setting();
    
});