<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    
    
});
Route::get('/', 'HomeController@myWebsite');

Route::get('/auth/redirect/{provider}', 'SocialAuthController@redirect');
Route::get('/callback/{provider}', 'SocialAuthController@callback');
Auth::routes();
Route::get('/dashboard', 'HomeController@index')->name('myDashboard');;
Route::post('/customLogin', 'Auth\LoginController@customLogin')->name('customLogin');
Route::post('reset_password_without_token', 'Auth\LoginController@validatePasswordRequestA')->name('validatePasswordRequestA');
//Route::post('reset_password_without_token', 'Auth\LoginController@validatePasswordRequest')->name('validatePasswordRequest');

Route::get('/search','SiteManager@search');
Route::get('/search-advance','SiteManager@searchAdvance')->name('searchAdvance');
Route::post('/deleteAction','SiteManager@deleteAction')->name('deleteAction');


//Route::get('/{service}', 'SiteManager@siteRoute')->name('siteRoute');
Route::get('/about', 'SiteManager@siteAbout')->name('siteAbout');
Route::get('/our-team', 'SiteManager@siteOurTeam')->name('siteOurTeam');
Route::get('/paula-mccartney', 'SiteManager@paulaMccartney')->name('paulaMccartney');
Route::get('/melisa-mcTaggart', 'SiteManager@MelisaMcTaggart')->name('MelisaMcTaggart');


Route::get('/community', 'SiteManager@SiteCommunityView')->name('SiteCommunityView');

Route::get('/buying', 'SiteManager@siteBuying')->name('siteBuying');
Route::get('/rentals', 'SiteManager@siteRentals')->name('siteRentals');
Route::get('/more-mls-listing', 'SiteManager@siteMoreMlsListing')->name('siteMoreMlsListing');
Route::get('/properties', 'SiteManager@siteProperties')->name('siteProperties');
Route::get('/properties-details/{id}', 'SiteManager@sitePropertiesDetails')->name('sitePropertiesDetails');

Route::get('/properties-view/{id}', 'SiteManager@myproDetails')->name('myproDetails');
Route::get('/feature-properties-view/{id}', 'SiteManager@FeaturemyproDetails')->name('FeaturemyproDetails');




Route::get('/blog-news', 'SiteManager@siteBlogNews')->name('siteBlogNews');
Route::get('/privacy-policy', 'SiteManager@privicyPolicy')->name('privicyPolicy');


Route::get('/property-list/{id}', 'SiteManager@propCatList')->name('propCatList');
Route::get('/property-cat-details/{id}', 'SiteManager@prop_cat_details')->name('prop_cat_details');
Route::get('/community-details/{id}', 'SiteManager@siteCommunityDetails')->name('siteCommunityDetails');
Route::get('/propertyPrint/{id}', 'SiteManager@propertyPrint')->name('propertyPrint');
Route::get('/propertyPrintDetail/{id}', 'SiteManager@propertyPrintDetail')->name('propertyPrintDetail');





Route::get('/community-details-view/{slug}', 'SiteManager@siteCommunityDetailsView')->name('siteCommunityDetailsView');


Route::get('/team-details/{id}', 'SiteManager@viewTeamMemberDetails')->name('viewTeamMemberDetails');
Route::get('/team/{slug}', 'SiteManager@teamPage')->name('teamPage');





Route::get('/contact', 'SiteManager@siteContactView')->name('siteContactView');
Route::get('/selling', 'SiteManager@sellingView')->name('siteContactView');
Route::get('/news-blogs', 'SiteManager@siteNewsBlog')->name('siteNewsBlog');
Route::get('/banners-list', 'SiteManager@bannersList')->name('bannersList');



Route::post('/saveContactData', 'SiteManager@saveContactData')->name('saveContactData');
Route::post('/setAllcheckboxAction', 'SiteManager@setAllcheckboxAction')->name('setAllcheckboxAction');
Route::post('/setAllcheckboxAction1', 'SiteManager@setAllcheckboxAction1')->name('setAllcheckboxAction1');

Route::post('/sendContactData', 'SiteManager@sendContactData')->name('sendContactData');



Route::post('/saveContactValuableData', 'SiteManager@saveContactValuableData')->name('saveContactValuableData');

Route::post('/savePropertyLeadData', 'SiteManager@savePropertyLeadData')->name('savePropertyLeadData');
Route::post('/getSetPublishMyListing', 'SiteManager@getSetPublishMyListing')->name('getSetPublishMyListing');
Route::post('/sendToFrined', 'SiteManager@sendToFrined')->name('sendToFrined');


Route::get('blog-details/{id}', 'SiteManager@blogDetails')->name('blogDetails');

Route::get('email-test', function(){
  
   // $details['email'] = 'ajayit2020@gmail.com';
    $data = ['message' => 'This is a test!'];
    $data = ['email' => 'ajayit2020@gmail.com'];
  
    dispatch(new App\Jobs\SendEmailJob($data));
  
    dd('done');
});

Route::group(['middleware' => ['auth']], function() {

    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('products','ProductController');

    //vendor
    Route::get('/vendors', 'VendorController@getVendorsList')->name('getVendorsList');
    Route::get('/add-vendor', 'VendorController@addVendor')->name('addVendor');    
    Route::post('/saveVendor', 'VendorController@saveVendor')->name('saveVendor');
    Route::post('/saveAdminWebsiteSettings', 'VendorController@saveAdminWebsiteSettings')->name('saveAdminWebsiteSettings');
    Route::post('/updateVendor', 'VendorController@updateVendor')->name('updateVendor');
    Route::get('/my-profile', 'SiteManager@myProfile')->name('myProfile');
    Route::post('/UserResetPassword','UserController@UserResetPassword')->name('UserResetPassword');
    
    Route::post('/getAjaxVendorsList', 'VendorController@getAjaxVendorsList')->name('getAjaxVendorsList');
    Route::post('/getAjaxPagesList', 'SiteManager@getAjaxPagesList')->name('getAjaxPagesList');
    Route::post('/getAjaxVendorStaticBlockList', 'SiteManager@getAjaxVendorStaticBlockList')->name('getAjaxVendorStaticBlockList');
    Route::post('/getAjaxVendorBannerList', 'SiteManager@getAjaxVendorBannerList')->name('getAjaxVendorBannerList');

    

    Route::post('/getAjaxVendorNewsCategory', 'SiteManager@getAjaxVendorNewsCategory')->name('getAjaxVendorStaticBlockList');
    Route::post('/getAjaxPropertyList', 'PropertyManager@getAjaxPropertyList')->name('getAjaxPropertyList');
    Route::post('/getAjaxContactList', 'PropertyManager@getAjaxContactList')->name('getAjaxContactList');
    Route::post('/getAjaxRentalList', 'PropertyManager@getAjaxRentalList')->name('getAjaxRentalList');


    

    Route::post('/getContactMessage', 'PropertyManager@getContactMessage')->name('getContactMessage');
    Route::post('/getValueLeadDetails', 'SiteManager@getValueLeadDetails')->name('getValueLeadDetails');

    
    Route::post('/getAjaxContactValueList', 'PropertyManager@getAjaxContactValueList')->name('getAjaxContactValueList');
    Route::post('/getAjaxCommunityList', 'SiteManager@getAjaxCommunityList')->name('getAjaxCommunityList');
    Route::post('/getAjaxNewsList', 'SiteManager@getAjaxNewsList')->name('getAjaxNewsList');
    Route::post('/getAjaxTestimonialList', 'SiteManager@getAjaxTestimonialList')->name('getAjaxTestimonialList');
    Route::post('/getAjaxDesignationList', 'SiteManager@getAjaxDesignationList')->name('getAjaxDesignationList');
    Route::post('/getAjaxTeamList', 'SiteManager@getAjaxTeamList')->name('getAjaxTeamList');
    Route::post('/getAjaxMyListingData', 'SiteManager@getAjaxMyListingData')->name('getAjaxMyListingData');
    Route::post('/getAjaxMypropertyData', 'SiteManager@getAjaxMypropertyData')->name('getAjaxMypropertyData');

    
    

    


    Route::get('/edit-vendor/{vid}', 'VendorController@editVdendor')->name('editVdendor');
    Route::get('/edit-designation/{vid}', 'SiteManager@editDesignation')->name('editDesignation');
    Route::get('/edit-community/{vid}', 'SiteManager@editCommunity')->name('editCommunity');

    Route::get('/edit-banner/{id}', 'SiteManager@editBannerList')->name('editBannerList');

    

    


    Route::get('/edit-property-lead/{vid}', 'SiteManager@editPropertyLead')->name('editPropertyLead');

    



    Route::get('/edit-static-block/{id}', 'SiteManager@editvendorStaticBlock')->name('editvendorStaticBlock');
    
    Route::get('/edit-news-category/{id}', 'SiteManager@editNewsCategory')->name('editNewsCategory');
    Route::post('/updateNewsCategory', 'SiteManager@updateNewsCategory')->name('updateNewsCategory');
    



    Route::get('/web-settings', 'VendorController@myWebsettings')->name('myWebsettings');  
    
    //vendor
    //Site Management
   // Route::get('/admin-menu', 'SiteManager@adminMenu')->name('admin_menu');

    Route::get('menu','MenuController@index')->name('admin_menu');
    Route::get('site-menu','MenuController@SiteMenu')->name('site_menu');

    Route::get('pages/add','SiteManager@adminPages')->name('admin_pages');
    Route::get('pages','SiteManager@adminPagesList')->name('adminPagesList');
    Route::get('sites-pages','SiteManager@vendorPagesList')->name('vendorPagesList');


    Route::get('add-contact','SiteManager@siteContact')->name('site_contact');
    Route::get('site-contact','SiteManager@siteContactUser')->name('site_contact_user');
    Route::post('/saveContact', 'SiteManager@saveContact')->name('saveContact');
    Route::post('/saveAdminBlocks', 'SiteManager@saveAdminBlocks')->name('saveAdminBlocks');
    Route::post('/saveAdminBanner', 'SiteManager@saveAdminBanner')->name('saveAdminBanner');
    
    Route::post('/saveCommunity', 'SiteManager@saveCommunity')->name('saveCommunity');
    Route::post('/saveUpdateCommunity', 'SiteManager@saveUpdateCommunity')->name('saveUpdateCommunity');

    
    Route::post('/saveNews', 'SiteManager@saveNews')->name('saveNews');
    


    

    Route::post('ckeditor/image_upload', 'SiteManager@upload')->name('upload');
    Route::post('saveStaticPages', 'SiteManager@saveStaticPages')->name('saveStaticPages');
   
    Route::get('admin-static-block', 'SiteManager@adminStaticBlock')->name('adminStaticBlock');
    Route::get('add-static-block', 'SiteManager@addAdminStaticBlock')->name('add_admin_static_block');
    
    
    Route::get('static-block', 'SiteManager@vendorStaticBlock')->name('vendorStaticBlock');
    
    Route::get('edit-static-block', 'SiteManager@addVendorStaticBlock')->name('add_vendor_static_block');
    
    //category

    Route::get('news-category', 'SiteManager@newsCategory')->name('newsCategory');
    Route::get('add-news-category', 'SiteManager@addNewsCategory')->name('add_news_category');
    Route::get('add-community', 'SiteManager@addCommunity')->name('add_community');

    

    Route::post('saveNewsCategory', 'SiteManager@saveNewsCategory')->name('saveNewsCategory');
    
    Route::get('listings', 'PropertyManager@propertyLeadView')->name('property_lead_list_view');
    Route::get('property-lead', 'SiteManager@property_lead')->name('property_lead');



   //contact_lead
   Route::get('contact-leads', 'PropertyManager@contactLeads')->name('contact_lead');
   Route::get('rental-leads', 'PropertyManager@RentalLeads')->name('RentalLeads');

   
   Route::get('valueable-leads', 'PropertyManager@valueableLeads')->name('valueable_lead');
   Route::get('community-list', 'SiteManager@siteCommunity')->name('site_community');
   Route::get('news', 'SiteManager@siteNews')->name('siteNews');
   
   Route::get('add-news', 'SiteManager@addNews')->name('add_news');
   Route::get('team', 'SiteManager@siteTeam')->name('siteTeam');
   Route::get('add-team', 'SiteManager@siteAddTeam')->name('siteAddTeam');

   Route::get('testimonial', 'SiteManager@testimonial')->name('testimonial');
   Route::get('add-testimonial', 'SiteManager@addTestimonial')->name('add_testimonial');

   Route::get('edit-news/{id}', 'SiteManager@editNews')->name('editNews');
   Route::get('edit-team/{id}', 'SiteManager@editTeams')->name('editTeams');
  

  
   Route::get('designation', 'SiteManager@siteDesignation')->name('siteDesignation');
   Route::get('add-designation', 'SiteManager@siteAddDesignation')->name('siteAddDesignation');

   Route::get('edit-testimonial/{id}', 'SiteManager@ediTestimonial')->name('ediTestimonial');
   Route::post('updateNews', 'SiteManager@updateNews')->name('updateNews');
   Route::post('updateTestimonial', 'SiteManager@updateTestimonial')->name('updateTestimonial');
   Route::post('saveTestimonial', 'SiteManager@saveTestimonial')->name('saveTestimonial');
   
   Route::post('saveDesignation', 'SiteManager@saveDesignation')->name('saveDesignation');
   Route::post('updateDesignation', 'SiteManager@updateDesignation')->name('updateDesignation');

   Route::post('saveTeams', 'SiteManager@saveTeams')->name('saveTeams');
   Route::post('updateTeams', 'SiteManager@updateTeams')->name('updateTeams');


   Route::get('my-listing', 'PropertyManager@myListing')->name('myListing');
   Route::get('add-property', 'PropertyManager@AddNewProperty')->name('add_new_property');
   Route::post('saveMyListing', 'PropertyManager@saveMyListing')->name('saveMyListing');
   Route::post('updateMyListing', 'PropertyManager@updateMyListing')->name('updateMyListing');
   Route::get('edit-my-list/{id}', 'PropertyManager@editMyList')->name('editMyList');

   





    


    //Site Management

});

Route::get('/{slug}', array('as' => 'page.show', 'uses' => 'SiteManager@show'));
