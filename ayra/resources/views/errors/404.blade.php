
<!doctype html>
<html lang="en">

<head>
    <title>Diamond Properties</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('ayra/front/css/bootstrap.min.css')}}"> 
    <link href="{{ asset('ayra/front/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('ayra/front/css/owl.theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ayra/front/css/style.css')}}">
    <link rel='stylesheet' id='font-awesome-css' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.8.12' type='text/css' media='all' />

</head>

<body>
	<section class="inner-banner contact-bg position-relative">

	
    

    	<!-- BANNER CONTENT -->
    	<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-12">
					<h1 class="text-white text-center mt-5">404</h1>
			    </div>
			</div>
		</div>
    </section>


    <section class="text-center mt-neg">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-8 col-lg-7 mx-auto">
    				<div class="error-box p-5 shadow">
						<h2 class="text-black">Page Not Found</h2>
						<h4 class="mt-3 mb-4">The page you're looking for doesn't exist</h4>
						<a href="{{BASE_PATH}}" class="blue-btn">Go To Home</a>
					</div>
    			</div>
    		</div>
    	</div>
    </section>


    