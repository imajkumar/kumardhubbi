<!DOCTYPE html>
<html>
<?php 

$mydomain_arr=AyraHelp::getVendorbyDomain(HOST_DOMAIN);

$web_name=optional($mydomain_arr)->web_name =="" ? "Diamond":optional($mydomain_arr)->web_name;
$web_setting_arr=AyraHelp::getWebSettings($mydomain_arr->vid);
$default_logo="default.png";
$web_logoLogin=optional($web_setting_arr)->web_logoLogin =="" ? $default_logo:optional($web_setting_arr)->web_logoLogin;

$web_logo_URL=asset('uploads/img/logo')."/".$web_logoLogin;

?>

    <head>
    <title>Login | {{$web_name}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{ asset('ayra/core/login/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('ayra/core/login/css/login.css')}}">
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
          <meta name="BASE_URL" content="{{ url('/') }}" />       
         <meta name="csrf-token" content="{{ csrf_token() }}">


    </head>
    <body>
        
        <div class="wrapper">
        	<div>
        		<figure class="text-center mb-4">
        			<img src="{{$web_logo_URL}}" width= "250" alt="Diamond Properties">
        		</figure>

        		<div class="login-box">
        			
                    <form class="mb-4" action="{{ route('login') }}" method="post">
                    @csrf
					    <div class="form-group">
					        <label for="email">Username</label>
					        <input autocomplete="on" class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">

					    </div>
					    <div class="form-group position-relative">
					        <label for="pwd">Password</label>
					       <input autocomplete="on" class="form-control" type="password" placeholder="Password" name="password">
					        <span class="view-pwd" onclick="viewPwd()"><img src="{{ asset('ayra/core/login/images/view-pwd.png')}}" alt=""></span>
					    </div>
					    <div class="form-group form-check">
					        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember me </label>

					        <!-- <a href="forget-pwd.html" class="float-right">Forgot password?</a> -->
                            @if (Route::has('password.request'))
                                    <a class="float-right" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif

					    </div>
					    
                        <button id="kt_login_signin_submit" class="btn btn-submit">Login</button>

					</form>
					<h6 class="text-center text-light">Copyright © 2020 Diamond Properties. All Rights Reserved.</h6>
        		</div>
        	</div>
        </div>
        <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d785f",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>

        <script type="text/javascript" src="{{ asset('ayra/core/login/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('ayra/core/login/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('ayra/core/js/plugins.bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/scripts.bundle.js')}}" type="text/javascript"></script>
    
        <script type="text/javascript">
                            	BASE_URL=$('meta[name="BASE_URL"]').attr('content');
                                CSRF_TOKEN=$('meta[name="csrf-token"]').attr('content');
														//	location.reload(1);
    </script>


    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('ayra/core/js/login_general.js')}}" type="text/javascript"></script>

       
    </body>
</html>
