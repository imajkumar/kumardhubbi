<!DOCTYPE html>

<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />

    <title>Login | Diamond Properties</title>
    <meta name="Login" content="Diamond Properties">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="BASE_URL" content="{{ url('/') }}" />       
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->


    <!--begin::Page Custom Styles(used by this page) -->

    <link rel="stylesheet" href="{{ asset('ayra/core/css/login3.css')}}">

    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('ayra/core/css/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <link href="{{ asset('ayra/core/css/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/css/menu_light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/css/dark.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/css/aside_dark.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Layout Skins -->


    <link rel="shortcut icon" type="image/x-icon" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="144x144" href=" https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="57x57" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />





</head>
<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url('https://keenthemes.com/metronic/themes/metronic/theme/default/demo1/dist/assets/media/bg/bg-3.jpg');">
                <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__logo">
                            <a href="#">
                                <img src="https://www.diamondproperties.ky/assets/images/powrpanel_logo.png">
                            </a>
                        </div>
                        <div class="kt-login__signin">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">Forgot Password ?</h3>


                                </h6>
                            </div>
                            <form class="kt-form" action="#" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Enter your email address below to reset your password.

                                    </label>
                                    <input type="email" name="email" class="form-control" aria-describedby="emailHelp" placeholder="Email">
                                    <span class="form-text text-muted"><b>Note:</b> Forgot password will sent to you email.</span>
                                </div>                                
                                <div class="kt-login__actions">
                                    <button id="kt_login_signin_submit_forget" class="btn btn-brand btn-elevate kt-login__btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                       
                       
                        <div class="kt-login__account">
                            <span class="kt-login__account-msg">
                            
                            </span>
                            &nbsp;&nbsp;
                            <a href="/login" id="" class="kt-login__account-link">Login ?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Page -->


    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d785f",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>
    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('ayra/core/js/plugins.bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->

    <script type="text/javascript">
                            	BASE_URL=$('meta[name="BASE_URL"]').attr('content');
                                CSRF_TOKEN=$('meta[name="csrf-token"]').attr('content');
														//	location.reload(1);
    </script>
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('ayra/core/js/login_general.js')}}" type="text/javascript"></script>
    <!--end::Page Scripts -->
    
</body>
<!-- end::Body -->

</html>