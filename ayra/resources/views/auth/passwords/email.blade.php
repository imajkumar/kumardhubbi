<!DOCTYPE html>
<html>
<?php 

$mydomain_arr=AyraHelp::getVendorbyDomain(HOST_DOMAIN);

$web_name=optional($mydomain_arr)->web_name =="" ? "Diamond":optional($mydomain_arr)->web_name;
$web_setting_arr=AyraHelp::getWebSettings($mydomain_arr->vid);
$default_logo="default.png";
$web_logo=optional($web_setting_arr)->web_logo =="" ? $default_logo:optional($web_setting_arr)->web_logo;
$web_logo_URL=asset('uploads/img/logo')."/".$web_logo;

?>

    <head>
    <title>Login | {{$web_name}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{ asset('ayra/core/login/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('ayra/core/login/css/login.css')}}">
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
          <meta name="BASE_URL" content="{{ url('/') }}" />       
         <meta name="csrf-token" content="{{ csrf_token() }}">


    </head>
    <body>
        
        <div class="wrapper">
        	<div>
        		<figure class="text-center mb-4">
        			<img src="{{$web_logo_URL}}" alt="Diamond Properties">
        		</figure>

        		<div class="login-box">
                    <h3>Forgot password?</h3>
                    
        			<form action="{{route('validatePasswordRequestA')}}" method="post" class="mb-4">
                        @csrf
					    <div class="form-group">
					        <label for="email">Enter your email address</label>
					        <input type="email" class="form-control" name="email" id="email" />
                        </div>
                        <div class="form-group otpClass" style="display: none;">
					        <label for="email">Enter OTP</label>
					        <input type="text" class="form-control" name="otp" id="otp" />
                        </div>
                        
                        <div class="form-group otpClass" style="display: none;">
					        <label for="email">Enter new Password</label>
					        <input type="text" class="form-control" name="new_password" id="otp" />
                        </div>
					    <button type="submit" id="kt_login_signin_submit_forget" class="btn btn-submit">Submit</button>
					</form>
					<h6 class="text-center text-light">Don’t forgot your password. <a href="/login">Login</a></h6>
        		</div>
        	</div>
        </div>

        <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d785f",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>

        <script type="text/javascript" src="{{ asset('ayra/core/login/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('ayra/core/login/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('ayra/core/js/plugins.bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/scripts.bundle.js')}}" type="text/javascript"></script>
    
        <script type="text/javascript">
                            	BASE_URL=$('meta[name="BASE_URL"]').attr('content');
                                CSRF_TOKEN=$('meta[name="csrf-token"]').attr('content');
														//	location.reload(1);
    </script>


    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('ayra/core/js/login_general.js')}}" type="text/javascript"></script>
    
       
    </body>
</html>
