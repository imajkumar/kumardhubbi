<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body style="background-color: #E6E6E6; font-family: 'Open Sans';" >

    <table style="width: 700px; margin: 0 auto; text-align: center;" cellspacing="0" cellpadding="0">
	    <tbody>
	        <tr>
	            <td style="text-align: left; vertical-align: top;">
	                <table style="margin: 0 auto; text-align: center; width: 100%; border: 0px;" cellspacing="0" cellpadding="0">
	                    <tbody>
	                        <tr>
	                            <td style="margin: 0px; padding: 0px; background-color: #184483; text-align: left; vertical-align: middle;">
	                                <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px;">
	                                    <tbody>
	                                        <tr style="text-align: center;">
	                                            <td style="line-height: 11px; text-align: center; vertical-align: middle; height: 76px; padding: 10px 0px 10px;">
	                                                <a
	                                                    href="https://www.diamondproperties.ky"
	                                                    title="Diamond Properties"
	                                                    target="_blank"
	                                                >
	                                                    <img src="{{$BASE_PATH}}/email_template/logo-email.png" style="margin-bottom: 2px; max-width: 150px"
	                                                        alt="Diamond Properties"/>
	                                                </a>
	                                            </td>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <table cellpadding="0" cellspacing="0" style="padding: 15px; text-align: left; vertical-align: top; width: 100%; border-left: 1px solid #eee; border-right: 1px solid #eee; background-color: #ffffff;">
	                                    <tbody>
	                                        <tr>
	                                            <td style="padding-top: 10px;">
	                                               
	                                                <br />

										            <table cellspacing="0" cellpadding="0" style="width: 100%; border: 0px;">
										                <tbody>
										                    <tr>
										                        <td width="70%">
										                            <table cellspacing="0" cellpadding="0" style="width: 100%; border: 0px;">
										                                <tbody>
										                                    <tr>
										                                        <td height="24" style="font-family: Verdana, Geneva, sans-serif; color: #333333; font-size: 13px; text-align: left; vertical-align: top;">
										                                            <strong style="color: #1d9ad6;">Property Name:</strong>
										                                            <a title="207 South At The Ritz-carlton Cayman" href="#" style="color: #40608f; text-decoration: none;" target="_blank">
                                                                                    {{$proData->property_title}}
										                                            </a>
										                                        </td>
										                                    </tr>
										                                    <tr>
										                                        <td height="24" style="font-family: Verdana, Geneva, sans-serif; color: #333333; font-size: 13px; text-align: left; vertical-align: top;">
										                                            <strong style="color: #1d9ad6;">Price:</strong> {{$proData->price_currency}} {{$proData->price}}
										                                        </td>
										                                    </tr>
										                                    <tr>
										                                        <td height="24" style="font-family: Verdana, Geneva, sans-serif; color: #333333; font-size: 13px; text-align: left; vertical-align: top;">
										                                            <strong style="color: #1d9ad6;">Location:</strong>{{$proData->location_district}}
										                                    </tr>
										                                    <tr>
										                                        <td height="24" style="font-family: Verdana, Geneva, sans-serif; color: #333333; font-size: 13px; text-align: left; vertical-align: top;">
										                                            <strong style="color: #1d9ad6;">Property Type:</strong> {{$proData->property_type}}
										                                        </td>
										                                    </tr>
										                                    <tr>
										                                        <td height="24" style="font-family: Verdana, Geneva, sans-serif; color: #333333; font-size: 13px; text-align: left; vertical-align: top;">
										                                            <strong style="color: #1d9ad6;">MLS#:</strong> {{$proData->prop_id}}
										                                        </td>
										                                    </tr>
										                                    <tr>
										                                        <td height="24" style="font-family: Verdana, Geneva, sans-serif; color: #333333; font-size: 13px; text-align: left; vertical-align: top;">
										                                            <strong style="color: #1d9ad6;">Beds:</strong> 
										                                        </td>
										                                    </tr>
										                                    <tr>
										                                        <td height="24" style="font-family: Verdana, Geneva, sans-serif; color: #333333; font-size: 13px; text-align: left; vertical-align: top;">
										                                            <strong style="color: #1d9ad6;">Baths:</strong> 
										                                        </td>
										                                    </tr>
										                                    <tr>
										                                        <td height="24" style="font-family: Verdana, Geneva, sans-serif; color: #333333; font-size: 13px; text-align: left; vertical-align: top;">
										                                            <strong style="color: #1d9ad6;">Sq. Ft.:</strong> {{$proData->area}}
										                                        </td>
										                                    </tr>
										                                    <tr>
										                                        <td height="24" style="font-family: Verdana, Geneva, sans-serif; color: #333333; font-size: 13px; text-align: left; vertical-align: top;">
										                                            <strong style="color: #1d9ad6;">Year:</strong> {{$proData->year_built}}
										                                        </td>
										                                    </tr>
										                                    
										                                </tbody>
										                            </table>
										                        </td>
										                        <td width="50%" valign="top" style="padding-top: 5px;">
										                            <a
										                                href="#"
										                                title="207 South At The Ritz-carlton Cayman"
										                                target="_blank"
										                            >
										                                <img
										                                    src="{{$BASE_PATH}}/email_template/email-property.jpg"
										                                    style="border: 1px none gray; width: 270px; height: 180px;"
										                                    alt="207 South At The Ritz-carlton Cayman"
										                                />
										                            </a>
										                        </td>
										                    </tr>
										                </tbody>
										            </table>
	                                            </td>
	                                        </tr>

	                                        <tr>
	                                            <td>
	                                                <p style="text-align:end;">
	                                                    <span style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #1d9ad6; font-weight: bold;">Best Regards,</span><br />
	                                                    <a
	                                                        href="https://www.diamondproperties.ky/"
	                                                        style="color: #40608f; text-decoration: none; font-family: Verdana, Geneva, sans-serif; font-size: 13px;"
	                                                        title="Diamond Properties"
	                                                        target="_blank">
	                                                        Diamond Properties
	                                                    </a>
	                                                </p>
	                                            </td>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	            </td>
	        </tr>
	        <tr>
	            <td>
	                <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #184483;">
	                    <tbody>
	                        <tr>
	                            <td style="text-align: center; vertical-align: middle;">
	                                <table cellpadding="0" cellspacing="0" style="margin: 15px auto 0px; width: 200px;">
	                                    <tbody>
	                                        <tr>
	                                            <td style="text-align: center;">
	                                                <a
	                                                    href="https://www.facebook.com/diamondproperties.ky/"
	                                                    title="Follow Us on Facebook"
	                                                    style="margin-right: 10px;"
	                                                    target="_blank" >
	                                                    <img src="{{$BASE_PATH}}/email_template/facebook.png" alt="Diamond Properties Facebook"/>
	                                                </a>
	                                                <a
	                                                    href="https://www.linkedin.com/company/diamond-properties---grand-cayman"
	                                                    title="Follow Us on Linkedin"
	                                                    style="margin-right: 10px;"
	                                                    target="_blank" >
	                                                    <img src="{{$BASE_PATH}}/email_template/linkedin.png" alt="Diamond Properties Linkedin"/>
	                                                </a>
	                                                <a
	                                                    href="https://www.instagram.com/diamondpropertiesky/"
	                                                    title="Follow Us on Instagram"
	                                                    style="margin-right: 10px;"
	                                                    target="_blank" >
	                                                    <img src="{{$BASE_PATH}}/email_template/instagram.png" alt="Diamond Properties Instagram"/>
	                                                </a>
	                                                
	                                            </td>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td style="padding: 10px 0; text-align: center; vertical-align: middle; font-size: 12px; color: #fff; font-family: Verdana, Geneva, sans-serif;">Copyright © 2020 Diamond Properties - All Rights Reserved.</td>
	                        </tr>
	                    </tbody>
	                </table>
	            </td>
	        </tr>
	    </tbody>
	</table>

</body>

</html>
