<!DOCTYPE html>

<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />

    <title>@get('title')</title>
    <meta name="description" content="">
    <meta name="author" content="@get('author')">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->
    <meta name="BASE_URL" content="{{ url('/') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="UUID" content="{{Auth::user()->id}}" />

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('ayra/core/light/css/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/light/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <link href="{{ asset('ayra/core/light/css/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/light/css/menu_light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/light/css/brand_light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/light/css/aside_light.css')}}" rel="stylesheet" type="text/css" />



    <!--end::Layout Skins -->
    <link href="{{ asset('ayra/public/vendor/harimayco-menu/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" type="image/x-icon" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="144x144" href=" https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="57x57" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />



    <link type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />



</head>
<!-- end::Head -->

<!-- begin::Body -->
<?php
$mydomain_arr = AyraHelp::getVendorbyDomain(request()->getHost());
$web_name = optional($mydomain_arr)->web_name == "" ? "Diamond" : optional($mydomain_arr)->web_name;
$web_setting_arr = AyraHelp::getWebSettings($mydomain_arr->vid);
$default_logo = "default.png";
$web_logo = optional($web_setting_arr)->web_logo == "" ? $default_logo : optional($web_setting_arr)->web_logo;
$web_logo_URL = asset('uploads/img/logo') . "/" . $web_logo;

?>
<style>
    #kt_table_1_contact_lead_length {
        width: 100%
    }
</style>

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="/">
                <img alt="Logo" src="{{$web_logo_URL}}" width="80" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>



            <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
        </div>
    </div>
    <!-- end:: Header Mobile -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            <!-- begin:: Aside -->

            <!-- Uncomment this to display the close button of the panel -->
            <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>


            <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
                <!-- begin:: Aside -->
                <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                    <div class="kt-aside__brand-logo">
                        <a href="/">
                            <img alt="Logo" src="{{$web_logo_URL}}" width="80" />
                        </a>
                    </div>

                    <div class="kt-aside__brand-tools">
                        <button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
                            <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
                                        <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
                                    </g>
                                </svg></span>
                            <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                    </g>
                                </svg></span>
                        </button>
                        <!--
			<button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
			-->
                    </div>
                </div>
                <!-- end:: Aside -->
                @partial('aside')


            </div>
            <!-- end:: Aside -->

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                <!-- begin:: Header -->
                @partial('header')
                <!-- begin:: Content -->
                @content()
                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            @partial('footer')
            <!-- end:: Footer -->
        </div>
    </div>
    </div>
    <!-- end:: Page -->








    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <!-- end::Scrolltop -->








    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>
    <!-- end::Global Config -->


    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('ayra/core/js/plugins.bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->
    <script type="text/javascript">
        BASE_URL = $('meta[name="BASE_URL"]').attr('content');
        CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    </script>
    <script>
        var menus = {
            "oneThemeLocationNoMenus": "",
            "moveUp": "Move up",
            "moveDown": "Mover down",
            "moveToTop": "Move top",
            "moveUnder": "Move under of %s",
            "moveOutFrom": "Out from under  %s",
            "under": "Under %s",
            "outFrom": "Out from %s",
            "menuFocus": "%1$s. Element menu %2$d of %3$d.",
            "subMenuFocus": "%1$s. Menu of subelement %2$d of %3$s."
        };
        var arraydata = [];
        var addcustommenur = '{{ route("haddcustommenu") }}';
        var updateitemr = '{{ route("hupdateitem")}}';
        var generatemenucontrolr = '{{ route("hgeneratemenucontrol") }}';
        var deleteitemmenur = '{{ route("hdeleteitemmenu") }}';
        var deletemenugr = '{{ route("hdeletemenug") }}';
        var createnewmenur = '{{ route("hcreatenewmenu") }}';
        var csrftoken = "{{ csrf_token() }}";
        var menuwr = "{{ url()->current() }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrftoken
            }
        });
    </script>
    <script type="text/javascript" src="{{asset('ayra/public/vendor/harimayco-menu/scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('ayra/public/vendor/harimayco-menu/scripts2.js')}}"></script>
    <script type="text/javascript" src="{{asset('ayra/public/vendor/harimayco-menu/menu.js')}}"></script>


    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('ayra/core/js/datatables.bundle.js')}}" type="text/javascript"></script>

    <script src="{{ asset('ayra/core/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/advanced-search.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/html-table.js')}}" type="text/javascript"></script>

    <script src="{{ asset('ayra/core/js/dashboard.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/ayra.js')}}" type="text/javascript"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {

            // highgraph
            var ranges = [
                [1246406400000, 14.3, 27.7],
                [1246492800000, 14.5, 27.8],
                [1246579200000, 15.5, 29.6],
                [1246665600000, 16.7, 30.7],
                [1246752000000, 16.5, 25.0],
                [1246838400000, 17.8, 25.7],
                [1246924800000, 13.5, 24.8],
                [1247011200000, 10.5, 21.4],
                [1247097600000, 9.2, 23.8],
                [1247184000000, 11.6, 21.8],
                [1247270400000, 10.7, 23.7],
                [1247356800000, 11.0, 23.3],
                [1247443200000, 11.6, 23.7],
                [1247529600000, 11.8, 20.7],
                [1247616000000, 12.6, 22.4],
                [1247702400000, 13.6, 19.6],
                [1247788800000, 11.4, 22.6],
                [1247875200000, 13.2, 25.0],
                [1247961600000, 14.2, 21.6],
                [1248048000000, 13.1, 17.1],
                [1248134400000, 12.2, 15.5],
                [1248220800000, 12.0, 20.8],
                [1248307200000, 12.0, 17.1],
                [1248393600000, 12.7, 18.3],
                [1248480000000, 12.4, 19.4],
                [1248566400000, 12.6, 19.9],
                [1248652800000, 11.9, 20.2],
                [1248739200000, 11.0, 19.3],
                [1248825600000, 10.8, 17.8],
                [1248912000000, 11.8, 18.5],
                [1248998400000, 10.8, 16.1]
            ]

            Highcharts.chart('container', {
                chart: {
                    type: 'area'
                },
                accessibility: {
                    description: 'Display of various data of dashboard'
                },
                title: {
                    text: 'Visual Graph of Web Access etc'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    allowDecimals: false,
                    
                    labels: {
                        formatter: function() {
                            return this.value; // clean, unformatted number for year
                        }
                    },
                    type: 'datetime',
                    accessibility: {
                        rangeDescription: 'Range: Jul 1st 2009 to Jul 31st 2009.'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Various Data Display'
                    },
                    labels: {
                        formatter: function() {
                            return this.value / 1000 + 'k';
                        }
                    }
                },
                tooltip: {
                    pointFormat: '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'Web Hit',
                    data: ranges
                }, ]
            });


            // highgraph
            $('#btnContactSend').click(function() {

                // ajax 
                var formData = {
                    'first_name': $('#first_name').val(),
                    'last_name': $('#last_name').val(),
                    'email': $('#email').val(),
                    'phone': $('#phone').val(),
                    'message': $('#message').val(),

                    '_token': $('meta[name="csrf-token"]').attr('content')
                };
                $.ajax({
                    url: BASE_URL + '/sendContactData',
                    type: 'POST',
                    data: formData,
                    success: function(res) {
                        if (res == 1) {
                            $('#mess_info').html('Successfully Submitted');
                        }
                    }
                });
                // ajax 


            });

        });
    </script>

    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/19.0.0/balloon/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('summary_ckeditor', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('summary_ckeditor_1', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    <!--end::Page Scripts -->
    <script>
        function saveArt() {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }


            return false;

        }

        // BalloonEditor
        //     .create( document.querySelector( '#editor' ) )
        //     .catch( error => {
        //         console.error( error );
        //     } );
    </script>

    <!--end::Page Scripts -->



    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script>
        var map;
        var marker;

        function initMap() {
            var myLatlng = {
                lat: -25.363,
                lng: 131.044
            };

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: myLatlng
            });

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map
            });

            // map.addListener('click', function(e) {
            //     document.getElementById('lat').innerHTML = e.latLng;
            // console.log(e.latLng);

            //     marker.setPosition(e.latLng);
            // });
            google.maps.event.addListener(map, "click", function(event) {

                // Get lat lng coordinates.
                // This method returns the position of the click on the map.
                var lat = event.latLng.lat().toFixed(6);
                var lng = event.latLng.lng().toFixed(6);

                // Call createMarker() function to create a marker on the map.
                createMarker(lat, lng);

                // getCoords() function inserts lat and lng values into text boxes.
                getCoords(lat, lng);

            });


        }

        // Function that creates the marker.
        function createMarker(lat, lng) {

            // The purpose is to create a single marker, so
            // check if there is already a marker on the map.
            // With a new click on the map the previous
            // marker is removed and a new one is created.

            // If the marker variable contains a value
            if (marker) {
                // remove that marker from the map
                marker.setMap(null);
                // empty marker variable
                marker = "";
            }

            // Set marker variable with new location
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                draggable: true, // Set draggable option as true
                map: map
            });


            // This event detects the drag movement of the marker.
            // The event is fired when left button is released.
            google.maps.event.addListener(marker, 'dragend', function() {

                // Updates lat and lng position of the marker.
                marker.position = marker.getPosition();

                // Get lat and lng coordinates.
                var lat = marker.position.lat().toFixed(6);
                var lng = marker.position.lng().toFixed(6);

                // Update lat and lng values into text boxes.
                getCoords(lat, lng);

            });
        }

        function getCoords(lat, lng) {

            // Reference input html element with id="lat".
            var coords_lat = document.getElementById('lat');

            // Update latitude text box.
            coords_lat.value = lat;

            // Reference input html element with id="lng".
            var coords_lng = document.getElementById('lng');

            // Update longitude text box.
            coords_lng.value = lng;
        }
        initMap();
    </script>

    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
    <script>
        $(document).ready(function() {
            $("#kt_table_1_contact_lead tbody tr .dt-checkboxes-cell sorting_1 .dt-checkboxes").removeClass("dt-checkboxes");
            // $("#kt_table_1_contact_lead tbody tr .dt-checkboxes-cell sorting_1").find(".dt-checkboxes").removeClass("dt-checkboxes");
        });
    </script>
    <script>
function goBack() {
  window.history.back();
}
</script>

</body>
<!-- end::Body -->

</html>