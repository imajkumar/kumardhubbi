<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<!--begin::Portlet-->
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Add New Vendor
						</h3>
					</div>
				</div>
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1" method ="post" data-redirect="vendors" action="{{route('saveVendor')}}">
					@csrf
					<div class="kt-portlet__body">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="name" placeholder="Enter your full name">
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="email" placeholder="Enter your email">
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Phone *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="phone" placeholder="Enter your phone">
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">URL *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="input-group">
									<input type="text" class="form-control" name="web_url" placeholder="Enter your URL">
									<div class="input-group-append"><span class="input-group-text">domain</span></div>
								</div>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Web Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="input-group">
									<input type="text" class="form-control" name="web_name" placeholder="Enter your URL">
									<div class="input-group-append"><span class="input-group-text">Website Name</span></div>
								</div>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						



						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Remarks *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<textarea class="form-control" name="notes" placeholder="Enter a Notes" rows="4"></textarea>
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="kt-separator kt-separator--border-dashed kt-separator--space-xl"></div>




					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">

									<button class="btn btn-brand"data-ktwizard-type="action-submit">
										Submit
									</button>

									<button type="reset" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>



	</div>
</div>
<!-- end:: Content -->