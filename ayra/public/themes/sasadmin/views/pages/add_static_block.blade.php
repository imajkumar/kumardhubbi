<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin::Portlet-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Modify Page Contents
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">

                                <a href="javascript:void(0)" onclick="goBack()" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-angle-double-left"></i>
                                    BACK
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <?php 
               // print_r($sb_arr_data);
                //die;
                ?>

                <form enctype="multipart/form-data" class="kt-form kt-form--label-right" id="kt_form_1" method="post" data-redirect="static-block" action="{{route('saveAdminBlocks')}}">
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="kt-section__content">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* TITLE:</label>
                                        <input type="text" value="{{optional($sb_arr_data)->parent_title}}" name="block_title" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* SUB TITLE:</label>
                                        <input type="text" value="{{optional($sb_arr_data)->parent_sub_title}}" name="block_subtitle" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* SUB TITLE TAG:</label>
                                        <input type="text"  value="{{optional($sb_arr_data)->parent_sub_sub_title}}" name="block_sub_tag" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* Title Content:</label>
                                        <input type="text" value="{{optional($sb_arr_data)->block_title_content}}" name="block_title_content" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>
                                <input type="hidden" name="block_parent" value="{{optional($sb_arr_data)->module_id}}" >
                                
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* SELECT IMAGE </label>
                                        <div class="custom-file">
                                            <input type="file" name="banner_img" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile"></label>
                                        </div>
                                        <span class="form-text text-muted">Recommended image size is of Width 500px * Height 500px, only *.jpg, *.jpeg, *.png, *.gif image formats are supported. Image should be maximum size of 15 MB.</span>
                                        <!-- <div id="drag-drop-area"></div>                                        -->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* SELECT VIDEO BACKGROUND IMAGE </label>
                                        <div class="custom-file">
                                            <input type="file" name="banner_video_img" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile"></label>
                                        </div>
                                        <span class="form-text text-muted">Recommended image size is of Width 500px * Height 500px, only *.jpg, *.jpeg, *.png, *.gif image formats are supported. Image should be maximum size of 15 MB.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* SELECT VIDEO </label>
                                        <div class="custom-file">
                                            <input type="file" name="video_file" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile"></label>
                                        </div>
                                        <span class="form-text text-muted">(Recommended Video should be maximum size of 15 MB) </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* DESCRIPTION </label>
                                        <textarea class="form-control" id="summary-ckeditor" name="summary_ckeditor">{{optional($sb_arr_data)->static_content}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* Property DESCRIPTION </label>
                                        <textarea class="form-control" id="summary-ckeditor_1" name="summary_ckeditor_1">{{optional($sb_arr_data)->sub_static_content}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* LINK:</label>
                                        <input type="text" name="block_link" id="kt_maxlength_1" value="{{optional($sb_arr_data)->external_link}}" maxlength="160" class="form-control" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* SELECT MAP IMAGE </label>
                                        <div class="custom-file">
                                            <input type="file" name="banner_map_img" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">Map</label>
                                        </div>
                                        <span class="form-text text-muted">Recommended image size is of Width 500px * Height 500px, only *.jpg, *.jpeg, *.png, *.gif image formats are supported. Image should be maximum size of 15 MB.</span>
                                    </div>
                                </div>
                                






                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="form-control-label">* PUBLISH/ UNPUBLISH *:</label>
                                        <div class="form-group">

                                            <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--brand">
                                                    <input type="radio" value="1" name="rdo_page_status"> <strong>Publish</strong>
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand">
                                                    <input checked type="radio" value="0" name="rdo_page_status"> <strong>Unpublish</strong>
                                                    <span></span>
                                                </label>

                                            </div>

                                        </div>

                                    </div>

                                </div>



                            </div>
                        </div>




                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" onclick="saveArt()" class="btn btn-brand" data-ktwizard-type="action-submit">Save & Exit</button>                                    
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>



    </div>
</div>
<!-- end:: Content -->