<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin::Portlet-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                           Edit Banner
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">

                                <a href="javascript:void(0)" onclick="goBack()" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-angle-double-left"></i>
                                    BACK
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <?php 
               // print_r($sb_arr_data);
                //die;
                ?>

                <form enctype="multipart/form-data" class="kt-form kt-form--label-right" id="kt_form_1" method="post" data-redirect="banners-list" action="{{route('saveAdminBanner')}}">
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="kt-section__content">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* TITLE:</label>
                                        <input type="text" readonly  value="{{optional($data)->banner_name}}" name="banner_title" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>                             
                                
                               
                                <input type="hidden" name="bannerID" value="{{optional($data)->id}}" >
                                
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* SELECT IMAGE </label>
                                        <div class="custom-file">
                                            <input type="file" name="banner_img" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile"></label>
                                        </div>
                                        <span class="form-text text-muted">Recommended image size is of Width 500px * Height 500px, only *.jpg, *.jpeg, *.png, *.gif image formats are supported. Image should be maximum size of 15 MB.</span>
                                        <!-- <div id="drag-drop-area"></div>                                        -->
                                    </div>
                                </div>
                                
                                
                               
                                

                                
                                






                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="form-control-label">* PUBLISH/ UNPUBLISH *:</label>
                                        <div class="form-group">

                                            <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--brand">
                                                    <input type="radio" value="1" name="rdo_page_status"> <strong>Publish</strong>
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand">
                                                    <input checked type="radio" value="0" name="rdo_page_status"> <strong>Unpublish</strong>
                                                    <span></span>
                                                </label>

                                            </div>

                                        </div>

                                    </div>

                                </div>



                            </div>
                        </div>




                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" onclick="saveArt()" class="btn btn-brand" data-ktwizard-type="action-submit">Save & Exit</button>                                    
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>



    </div>
</div>
<!-- end:: Content -->