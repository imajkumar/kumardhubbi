<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin::Portlet-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Edit Team
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">

                                <a href="javascript:void(0)" onclick="goBack()" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-angle-double-left"></i>
                                    BACK
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <?php
                // print_r($sb_arr_data);
                //die;
                ?>

                <form enctype="multipart/form-data" class="kt-form kt-form--label-right" id="kt_form_1" method="post" data-redirect="team" action="{{route('updateTeams')}}">
                    @csrf
                    <input type="hidden" name="txtID" value="{{$data_arr->id}}">
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="kt-section__content">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* NAME:</label>
                                        <input type="text" value="{{$data_arr->name}}" name="name" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* SLUG:</label>
                                        <input type="text" value="{{ strtolower(str_replace(' ', '-', optional($data_arr)->name))}}" name="slug" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label"> Cireba Agent No:</label>
                                        <input type="text" value="{{$data_arr->agent_no}}"  name="agent_no" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label"> DESIGNATION:</label>
                                        <input type="text" value="{{$data_arr->designation}}"  name="designation" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label"> ASSIGN USER:</label>
                                        <input type="text" value="{{$data_arr->assign_user}}"  name="assign_user" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                               
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* SELECT IMAGE </label>
                                        <div class="custom-file">
                                            <input type="file" name="photo" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile"></label>
                                        </div>
                                        <span class="form-text text-muted">Recommended image size is of Width 500px * Height 500px, only *.jpg, *.jpeg, *.png, *.gif image formats are supported. Image should be maximum size of 15 MB.</span>
                                        <!-- <div id="drag-drop-area"></div>                                        -->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label"> DESCRIPTION </label>
                                        <textarea class="form-control" id="summary-ckeditor" name="summary_ckeditor">{{$data_arr->info_data}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* Email:</label>
                                        <input type="text" value="{{$data_arr->email}}" name="email" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* Phone No:</label>
                                        <input type="text"  value="{{$data_arr->phone}}" name="phone" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label"> Address:</label>
                                        <input type="text" value="{{$data_arr->address}}"  name="address" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label"> Facebook:</label>
                                        <input type="text" value="{{$data_arr->facebook}}"  name="facebook" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* Linkedin:</label>
                                        <input type="text" value="{{$data_arr->linkedin}}" name="linkedin" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label"> Instagram:</label>
                                        <input type="text"  value="{{$data_arr->instagram}}" name="instagram" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* Youtube:</label>
                                        <input type="text" value="{{$data_arr->youtube}}"  name="youtube" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* Twitter:</label>
                                        <input type="text" value="{{$data_arr->twitter}}"  name="twitter" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>















                                <h3>SEO Information</h3>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label"> META TITLE:</label>
                                        <input type="text" value="{{$data_arr->meta_title}}"  name="cm_meta_title" id="kt_maxlength_1" maxlength="160" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="form-control-label"> META KEYWORDS:</label>
                                        <input type="text" value="{{$data_arr->meta_key}}" name="cm_meta_keywords" id="kt_maxlength_2" maxlength="160" class="form-control" placeholder="">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="form-control-label"> META DESCRIPTION:</label>
                                        <input type="text" value="{{$data_arr->meta_discption}}" name="cm_meta_discription" id="kt_maxlength_3" maxlength="260" class="form-control" placeholder="">
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                   
                                    <div class="col-lg-6">
                                        <label class="form-control-label">PUBLISH/ UNPUBLISH *:</label>
                                        <div class="form-group">

                                            <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--brand">
                                                    <input <?php echo $data_arr->is_published ==1 ? "checked" :"" ?> type="radio" value="1" name="rdo_page_status"> <strong>Publish</strong>
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand">
                                                    <input <?php echo $data_arr->is_published ==0 ? "checked" :"" ?>  type="radio" value="0" name="rdo_page_status"> <strong>Unpublish</strong>
                                                    <span></span>
                                                </label>

                                            </div>

                                        </div>

                                    </div>

                                </div>



                            </div>
                        </div>




                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" onclick="saveArt()" class="btn btn-brand" data-ktwizard-type="action-submit">Save & Exit</button>
                                    
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>



    </div>
</div>
<!-- end:: Content -->