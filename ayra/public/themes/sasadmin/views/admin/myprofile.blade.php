<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin::Portlet-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                        Password Reset
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">

                                <a href="javascript:void(0)" onclick="goBack()" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-angle-double-left"></i>
                                    BACK
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <?php 
               // print_r($sb_arr_data);
                //die;
                ?>
								
									
									<div class="col-xl-8">
										<div class="m-tabs-content" id="m_sections">

											<!--begin::Section 1-->
											<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_1">
												
												<div class="m-accordion m-accordion--section m-accordion--padding-lg" id="m_section_1_content">

													<!--begin::Item-->
													<div class="m-accordion__item">

														<div class="m-accordion__item-body collapse show" id="m_section_1_content_1_body" role="tabpanel" aria-labelledby="m_section_1_content_1_head" data-parent="#m_section_1_content">
															<div class="m-accordion__item-content">
															   <!-- password reset -->
                                 <!--begin::Portlet-->



 									<!--begin::Form-->
 									<form class="m-form m-form--state m-form--fit m-form--label-align-right" id="m_form_2_reset">
 										<div class="m-portlet__body">
 											<div class="m-form__content">
 												<div class="m-alert m-alert--icon alert alert-default m--hide" role="alert" id="m_form_2_msg">
 													<div class="m-alert__icon">
 														<i class="la la-warning"></i>
 													</div>


 												</div>
 											</div>
 										
 											<div class="form-group m-form__group row">
 												<label class="col-form-label col-lg-4 col-sm-12">Current Password</label>
 												<div class="col-lg-8 col-md-8 col-sm-12">
 													<div class="m-input-icon m-input-icon--left">
 														<input type="text" class="form-control m-input" name="current"  id="current" placeholder="*************************">
 														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-key"></i></span></span>
 													</div>
 													<span class="m-form__help"></span>
 												</div>
											 </div>
											 <div class="form-group m-form__group row">
												<label class="col-form-label col-lg-4 col-sm-12">New Password</label>
												<div class="col-lg-8 col-md-8 col-sm-12">
													<div class="m-input-icon m-input-icon--left">
														<input type="text" class="form-control m-input" name="password" id="password"  placeholder="">
														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-key"></i></span></span>
													</div>
													<span class="m-form__help"></span>
												</div>
											</div>
											<div class="form-group m-form__group row">
												<label class="col-form-label col-lg-4 col-sm-12">Confirm Password</label>
												<div class="col-lg-8 col-md-8 col-sm-12">
													<div class="m-input-icon m-input-icon--left">
														<input type="text" class="form-control m-input"  name="confirmed" id="confirmed" placeholder="">
														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-key"></i></span></span>
													</div>
													<span class="m-form__help"></span>
												</div>
											</div>		
 										
 										</div>
 										<div class="m-portlet__foot m-portlet__foot--fit">
 											<div class="m-form__actions m-form__actions">
 												<div class="row">
 													<div class="col-lg-9 ml-lg-auto">
 														<button type="button" id="btnPasswordReset" class="btn btn-warning">Reset Password</button>
 														<button type="reset" class="btn btn-secondary">Cancel</button>
 													</div>
 												</div>
 											</div>
 										</div>
 									</form>

 									<!--end::Form-->
 								

 								<!--end::Portlet-->


															   <!-- password reset -->

															</div>
														</div>
													</div>

													<!--end::Item-->



												</div>
											</div>

										


										</div>
									</div>
								
                            

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>



    </div>
</div>
<!-- end:: Content -->