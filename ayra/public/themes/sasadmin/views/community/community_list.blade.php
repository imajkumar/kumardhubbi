<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin::Portlet-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Community
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">

                                <a href="{{route('add_community')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus"></i>
                                    ADD COMMUNITY
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                <!--begin: Search Form -->
									<form class="kt-form kt-form--fit kt-margin-b-20">
										
										<div class="row kt-margin-b-20">
											<!-- <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
												<label>RecordID:</label>
												<input type="text" class="form-control kt-input" placeholder="E.g: 4590" data-col-index="2">
											</div> -->

											<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
												<label>Type:</label>
												<select class="form-control kt-input" data-col-index="4">
													
												<option value="1">Featured</option>
												<option value="2">Un Featured</option>
												</select>
											</div>
										</div>
										
										<div class="row" style="margin-top: -64px;margin-left: 231px;">
											<div class="col-lg-12">
												<button class="btn btn-primary btn-brand--icon" id="kt_search">
													<span>
														<i class="la la-search"></i>
														<span>Search</span>
													</span>
												</button>
												&nbsp;&nbsp;
												<button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
													<span>
														<i class="la la-close"></i>
														<span>Reset</span>
													</span>
												</button>
											</div>
										</div>
									</form>                   
                      <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1_community_List">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Image </th>
                                <th>Name</th>
                                <th>Discription</th>                               
                              
                                <th>Publish</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
            <!--end::Portlet-->
        </div>



    </div>
</div>
<!-- end:: Content -->