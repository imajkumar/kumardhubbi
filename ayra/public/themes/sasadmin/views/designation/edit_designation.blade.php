<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin::Portlet-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Edit Designation
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">

                                <a href="javascript:void(0)" onclick="goBack()" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-angle-double-left"></i>
                                    BACK
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <?php
                // print_r($sb_arr_data);
                //die;
                ?>

                <form enctype="multipart/form-data" class="kt-form kt-form--label-right" id="kt_form_1" method="post" data-redirect="designation" action="{{route('updateDesignation')}}">

                <input type="hidden" name="txtID" value="{{$data_arr->id}}">

                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="kt-section__content">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label class="form-control-label">* NAME:</label>
                                        <input type="text" value="{{$data_arr->name}}" name="name" id="kt_maxlength_4" maxlength="120" class="form-control" placeholder="">

                                    </div>
                                </div>






                                <h3>Display Information</h3>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="form-control-label">* Display Order:</label>
                                        <input type="text" value="{{$data_arr->order_no}}"  name="disp_order" id="kt_maxlength_1" maxlength="160" class="form-control" placeholder="">

                                    </div>
                                    <div class="col-lg-6">
                                        <label class="form-control-label">* PUBLISH/ UNPUBLISH *:</label>
                                        <div class="form-group">

                                            <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--brand">
                                                    <input type="radio" value="1" name="rdo_page_status"> <strong>Publish</strong>
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand">
                                                    <input checked type="radio" value="0" name="rdo_page_status"> <strong>Unpublish</strong>
                                                    <span></span>
                                                </label>

                                            </div>

                                        </div>

                                    </div>

                                </div>



                            </div>
                        </div>




                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" onclick="saveArt()" class="btn btn-brand" data-ktwizard-type="action-submit">Save & Exit</button>
                                    
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>



    </div>
</div>
<!-- end:: Content -->