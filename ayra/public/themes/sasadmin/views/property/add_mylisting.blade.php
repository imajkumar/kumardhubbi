<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin::Portlet-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Add New Property
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <?php

use App\Helpers\AyraHelp;

$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);

                $agent_arr = AyraHelp::getmyTeamByHOST_HOME_NOLIMIT($web_arr_->vid);
                $community_arr = AyraHelp::getCommunitiesByHOSTHOME($web_arr_->vid);

                ?>

                <form enctype="multipart/form-data" class="kt-form kt-form--label-right" id="kt_form_1" method="post" data-redirect="my-listing" action="{{route('saveMyListing')}}">
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Primary Agent:</label>
                                <select class="form-control" id="name" name="p_agent">
                                    <option>-SELECT-</option>
                                    <?php

                                    foreach ($agent_arr as $key => $row) {
                                    ?>
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    <?php
                                    }
                                    ?>

                                </select>

                            </div>
                            <div class="col-lg-6">
                                <label class="">Seconday Agent:</label>
                                <select class="form-control" id="name" name="s_agent">
                                    <option>-SELECT-</option>
                                    <?php

                                    foreach ($agent_arr as $key => $row) {
                                    ?>
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    <?php
                                    }
                                    ?>

                                </select>

                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Property Type:</label>
                                

                                <select class="form-control" id="name" name="p_type">
                                    <option value="">-SELECT-</option>
                                    <?php 
                                $data_arr=AyraHelp::getProprtyCat();
                                foreach ($data_arr as $key => $rowData) {
                                  ?>
                                   <option value="{{$rowData->id}}">{{$rowData->property_type}}</option>
                                  <?php
                                }
                                ?>
                                </select>



                            </div>
                            <div class="col-lg-6">
                                <label class="">Listing Type:</label>

                                <select class="form-control" id="name" name="listing_type">
                                    <option value="">-SELECT-</option>
                                    <option value="1">Commercial</option>
                                    <option value="2">Residential</option>
                                </select>


                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label>Property Name:</label>

                                <input type="text" name="p_name" class="form-control" placeholder="">

                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label>Currency:</label>
                                <select class="form-control" id="name" name="currency">
                                    <option value="">-SELECT-</option>
                                    <option value="1">US($)</option>
                                    <option value="2">CI($)</option>

                                </select>


                            </div>
                            <div class="col-lg-4">
                                <label>Monthly Rent:</label>
                                <input type="text" name="monthly_rent" class="form-control" placeholder="">


                            </div>
                            <div class="col-lg-4">
                                <label>Available From:</label>
                                <input type="text" id="kt_datepicker_1" name="available_from" class="form-control" placeholder="">

                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="form-control-label">* DESCRIPTION </label>
                                <textarea class="form-control" id="summary-ckeditor" name="summary_ckeditor"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Select Community:</label>
                                <div class="kt-input-icon">
                                    <select class="form-control" id="name" name="community_id">
                                        <option value="">-SELECT-</option>
                                        <?php

                                        foreach ($community_arr as $key => $row) {
                                        ?>
                                            <option value="{{$row->id}}">{{$row->title}}</option>
                                        <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                                <label>Address:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="address" class="form-control" placeholder="Enter your address">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
                                </div>


                                <div class="coordenadas">

                                    <label for="lat">Latitude:</label>
                                    <input class="form-control" type="text" name="lat" id="lat" value="0" />
                                    <br>
                                    <label for="lng">longitude:</label>
                                    <input class="form-control" type="text" name="lng" id="lng" value="0" />
                                </div>

                            </div>
                            <div class="col-lg-6">
                                <div id="map" style="width: 100%; height: 300px;"></div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="form-control-label">* SELECT Brochure </label>
                                <div class="custom-file">
                                    <input type="file" name="brochure_img" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Brochure</label>
                                </div>
                                <span class="form-text text-muted">Recommended image size is of Width 500px * Height 500px, only *.jpg, *.jpeg, *.png, *.gif image formats are supported. Image should be maximum size of 15 MB.</span>
                                <!-- <div id="drag-drop-area"></div>                                        -->
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="form-control-label">* SELECT Images </label>
                                <div class="custom-file">
                                    <input type="file" name="prop_img" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Brochure</label>
                                </div>
                                <span class="form-text text-muted">Recommended image size is of Width 500px * Height 500px, only *.jpg, *.jpeg, *.png, *.gif image formats are supported. Image should be maximum size of 15 MB.</span>
                                <!-- <div id="drag-drop-area"></div>                                        -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="form-control-label">* SELECT VIDEO </label>
                                <div class="custom-file">
                                    <input type="file" name="pro_video" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">logo</label>
                                </div>
                                <span class="form-text text-muted">(Recommended Video should be maximum size of 15 MB) </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Status:</label>
                                <select class="form-control" id="name" name="prop_status">
                                    <option value="">Select Status</option>
                                    <option value="1">
                                        Current </option>
                                    <option value="2">
                                        Rented </option>
                                    <option value="3">
                                        Expired </option>
                                    <option value="4">
                                        Pending </option>
                                    <option value="5">
                                        Reduced </option>
                                    <option value="6">
                                        Suspended </option>
                                    <option value="7">
                                        Withdrawn </option>
                                    <option value="8">
                                        Pending/Conditional </option>
                                    <option value="9">
                                        New </option>
                                    <option value="10">
                                        Increased </option>
                                    <option value="11">
                                        Change </option>
                                    <option value="12">
                                        Back On The Market </option>
                                    <option value="13">
                                        Not Active </option>
                                </select>


                            </div>
                            <div class="col-lg-6">
                                <label class="">Set Expiry:</label>
                                <input type="text" id="kt_datepicker_1" name="prop_exp" class="form-control" placeholder="">
                            </div>
                        </div>


                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" onclick="saveArt()" class="btn btn-brand" data-ktwizard-type="action-submit">Save & Exit</button>
                                    
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>



    </div>
</div>
<!-- end:: Content -->