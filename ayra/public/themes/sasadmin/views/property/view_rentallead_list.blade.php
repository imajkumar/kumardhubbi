<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin::Portlet-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Rental Lead
                        </h3>
                    </div>
                  
                    <!-- <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">

                                <a href="#" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus"></i>
                                    ADD NEW
                                </a>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="kt-portlet__head-toolbar">
                        <button class="btn btn-brand" id="btngetAllUserSelectedS">Delete</button>
                    </div>
                  

                <div class="kt-portlet__body">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1_Rental_lead">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>

                                <th>ID</th>

                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Message</th>
                                <th>Created on</th>
                                
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
            <!--end::Portlet-->
        </div>



    </div>
</div>
<!-- end:: Content -->

<!--begin::Modal-->
<div class="modal fade" id="kt_modal_4_viewProp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            X</button>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Property Detail</h5>

            </div>
            <div class="modal-body">
                <!--begin::Section-->
                <div class="kt-section">

                    <div class="kt-section__content">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Table heading</th>
                                        <th>Table heading</th>
                                        <th>Table heading</th>
                                        <th>Table heading</th>
                                        <th>Table heading</th>
                                        <th>Table heading</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Table cell</td>
                                        <td>Table cell</td>
                                        <td>Table cell</td>
                                        <td>Table cell</td>
                                        <td>Table cell</td>
                                        <td>Table cell</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Section-->

            </div>

        </div>
    </div>
</div>
<!--end::Modal-->
<!-- kt_modal_4_viewProp_message -->
<!--begin::Modal-->
<div class="modal fade" id="kt_modal_4_viewPropContact_message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            X</button>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Message</h5>

            </div>
            <div class="modal-body">
                <!--begin::Section-->
                <div class="kt-section">

                    <div class="kt-section__content">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>

                                <tbody>
                                    <tr>
                                        <th id="showmemsg" scope="row">1</th>

                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Section-->

            </div>

        </div>
    </div>
</div>
<!--end::Modal-->