<?php

use App\Helpers\AyraHelp;

$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$data_lead_arr = AyraHelp::getContactLead($web_arr_->vid);
$data_lead_value_arr = AyraHelp::valueable_leads($web_arr_->vid);
$data_prop_lead_value_arr = AyraHelp::propValue_leads($web_arr_->vid);


$web_arr = DB::table('webmobilehits')->where('vid', $web_arr_->vid)->get();
$wh = 0;
$mh = 0;
foreach ($web_arr as $key => $rowData) {
	$wh = $wh + $rowData->webhits;
	$mh = $mh + $rowData->mobhits;
}



?>
<!-- begin:: Content -->
<div class="container">
	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
		<div class="row">
			<div class="col-12">
				<div class="white-box">
					<div class="d-flex mb-4">
						<div class="color-box box1">
							<div class="d-flex align-items-center mb-2">
								<!-- <img src="images/web-hits.png"> -->
								<i class="la la-laptop"></i>
								<h6>Web Hits</h6>
							</div>
							<h4>{{$wh}}</h4>
						</div>

						<div class="color-box box2">
							<div class="d-flex align-items-center mb-2">
								<!-- <img src="images/mobile-hits.png"> -->
								<i class="la la-mobile"></i>
								<h6>Mobile Hits</h6>
							</div>
							<h4>{{$mh}}</h4>
						</div>

						<div class="color-box box3">
							<div class="d-flex align-items-center mb-2">
								<!-- <img src="images/contact-leads.png"> -->
								<i class="la la-user"></i>
								<h6>Contact Leads</h6>
							</div>
							<h4>{{count($data_lead_arr)}}</h4>
						</div>

						<div class="color-box box4">
							<div class="d-flex align-items-center mb-2">
								<!-- <img src="images/property-leads.png"> -->
								<i class="la la-home"></i>
								<h6>Property Hits</h6>
							</div>
							<h4>{{count($data_lead_arr)}}</h4>
						</div>

						<div class="color-box box5">
							<div class="d-flex align-items-center mb-2">
								<!-- <img src="images/valuation-leads.png"> -->
								<i class="la la-dollar"></i>
								<h6>Valuation Hits</h6>
							</div>
							<h4>{{count($data_prop_lead_value_arr)}}</h4>
						</div>
					</div>
					<style>
						.highcharts-figure,
						.highcharts-data-table table {
							min-width: 320px;
							max-width: 800px;
							margin: 1em auto;
						}

						#container {
							height: 450px;
						}

						.highcharts-data-table table {
							font-family: Verdana, sans-serif;
							border-collapse: collapse;
							border: 1px solid #EBEBEB;
							margin: 10px auto;
							text-align: center;
							width: 100%;
							max-width: 500px;
						}

						.highcharts-data-table caption {
							padding: 1em 0;
							font-size: 1.2em;
							color: #555;
						}

						.highcharts-data-table th {
							font-weight: 600;
							padding: 0.5em;
						}

						.highcharts-data-table td,
						.highcharts-data-table th,
						.highcharts-data-table caption {
							padding: 0.5em;
						}

						.highcharts-data-table thead tr,
						.highcharts-data-table tr:nth-child(even) {
							background: #f8f8f8;
						}

						.highcharts-data-table tr:hover {
							background: #f1f7ff;
						}
					</style>
					<!-- <img src="{{ asset('ayra/core/img/graph.png')}}" alt="" style="max-width: 100%;"> -->
					<!-- <div id="chart_div" style="width: 100%; height: 150;"></div> -->
					<figure class="highcharts-figure">
						<div id="container"></div>

					</figure>


				</div>
			</div>
		</div>

	</div>



	<div class="row">
		<div class="col-md-6">
			<h3>Contact Leads</h3>
			<div class="white-box">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email ID</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($data_lead_arr as $key => $rowata) {
							$create_at = date('j F Y', strtotime($rowata->created_at));
						?>
							<tr>
								<td>{{$rowata->first_name}} {{$rowata->last_name}}</td>
								<td>{{$rowata->email}}</td>
								<td>{{$create_at}}</td>
							</tr>
						<?php
						}
						?>


					</tbody>
				</table>
				<p class="text-right"><a href="{{route('contact_lead')}}">View All</a></p>
			</div>
		</div>
		<div class="col-md-6">
			<h3>Property Leads</h3>
			<div class="white-box">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email ID</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>

						<?php

						foreach ($data_prop_lead_value_arr as $key => $rowata) {
							$create_at = date('j F Y', strtotime($rowata->created_at));
						?>
							<tr>
								<td>{{$rowata->first_name}} {{$rowata->last_name}}</td>
								<td>{{$rowata->email}}</td>
								<td>{{$create_at}}</td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="text-right"><a href="{{route('property_lead')}}">View All</a></p>
			</div>
		</div>
		<div class="col-md-6">
			<h3>Valuation Leads</h3>
			<div class="white-box">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email ID</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($data_lead_value_arr as $key => $rowata) {
							$create_at = date('j F Y', strtotime($rowata->created_at));
						?>
							<tr>
								<td>{{$rowata->name}} </td>
								<td>{{$rowata->email}}</td>
								<td>{{$create_at}}</td>
							</tr>
						<?php
						}
						?>

					</tbody>
				</table>
				<p class="text-right"><a href="{{route('valueable_lead')}}">View All</a></p>
			</div>
		</div>
	</div>




	<!-- aj -->

</div>
<!-- end:: Content -->



<!--begin::Modal-->
<div class="modal fade" id="kt_modal_4_Support" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Support Ticket</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="z-index-0 position-relative bg-white agent-info p-4">
					<h3 id="mess_info" style="color:#008080"></h3>
					<form id="kt_form_1" id="contact_formAjaxSubmit" method="post" action="{{route('sendContactData')}}" class="contact-form">
						@csrf
						<div class="row">
							<div class="form-group col-md-6">
								<input type="text" required id="first_name" name="first_name" class="form-control" placeholder="First Name*">

							</div>
							<div class="form-group col-md-6">
								<input type="text" required id="last_name" name="last_name" class="form-control" placeholder="Last Name*">

							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								<input type="email" required id="email" name="email" class="form-control" placeholder="Email Address*">

							</div>
							<div class="form-group col-md-6">
								<input type="text" required id="phone" name="phone" class="form-control" placeholder="Phone No.*">

							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<textarea class="form-control" id="message" required name="message" placeholder="Your Message*" rows="5"></textarea>

							</div>

							<div class="col-12 text-center text-md-right mt-3">

								<button type="button" class="btn send-btn" id="btnContactSend">
									Send Now <i class="fa fa-caret-right ml-3"></i>
								</button>

							</div>
						</div>
					</form>
				</div>
			</div>

		</div>

	</div>
</div>
</div>

<!--end::Modal-->