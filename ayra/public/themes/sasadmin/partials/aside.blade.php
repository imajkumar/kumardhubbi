 <!-- begin:: Aside Menu -->
 <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">

     <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">

         <ul class="kt-menu__nav ">
             <li class="kt-menu__item " aria-haspopup="true"><a href="#1" class="kt-menu__link "><span class="kt-menu__link-icon">
                         <i class="la la-tachometer"></i>
                     </span><span class="kt-menu__link-text">CRM</span></a></li>



             <!-- my listing -->
             <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                         <i class="la la-cog"></i>
                     </span><span class="kt-menu__link-text">Listings</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                 <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                     <ul class="kt-menu__subnav">
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('myListing')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Manage Listings</span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('property_lead_list_view')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Manage Featured Properties </span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('site_community')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Manage Communities </span>
                             </a>
                         </li>


                     </ul>
                 </div>
             </li>

             <!-- my listing -->


             <!-- site management -->

             <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon"><i class="la la-tv"></i></span><span class="kt-menu__link-text">Site Management</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                 <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                     <ul class="kt-menu__subnav">
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('vendorStaticBlock')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Pages</span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('vendorPagesList')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">SEO Pages</span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('bannersList')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Banners</span>
                             </a>
                         </li>

                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('site_menu')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Menu Manager </span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('testimonial')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Testimonials</span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('siteTeam')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Teams</span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('siteDesignation')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Designations</span>
                             </a>
                         </li>










                     </ul>
                 </div>
             </li>
             <!-- site management -->

             <!-- Lead management  -->

             <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                         <i class="la la-list"></i>
                     </span><span class="kt-menu__link-text">Leads Center</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                 <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                     <ul class="kt-menu__subnav">
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('contact_lead')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Contact Leads </span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('property_lead')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Property Leads</span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('RentalLeads')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text"> Rental Leads</span>
                             </a>
                         </li>

                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('valueable_lead')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Valuation Leads</span>
                             </a>
                         </li>






                     </ul>
                 </div>
             </li>
             <!-- Lead management  -->


             <!-- blog -->
             <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                         <i class="la la-file-text-o"></i>
                     </span><span class="kt-menu__link-text">Blog</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                 <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                     <ul class="kt-menu__subnav">
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('newsCategory')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text"> Blog Categories </span>
                             </a>
                         </li>


                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('siteNews')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Blog</span>
                             </a>
                         </li>
                     </ul>
                 </div>
             </li>
             <!-- blog -->



             <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                         <i class="la la-cog"></i>
                     </span><span class="kt-menu__link-text">Settings</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                 <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                     <ul class="kt-menu__subnav">
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('myWebsettings')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Basic Settings</span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="{{route('site_contact_user')}}" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Contact Information </span>
                             </a>
                         </li>
                         <li class="kt-menu__item " aria-haspopup="true">
                             <a href="#" class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">API Keys </span>
                             </a>
                         </li>




                     </ul>
                 </div>
             </li>

             <!-- content sites  -->
             <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                         <i class="la la-file-text-o"></i>
                     </span><span class="kt-menu__link-text">Support</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                 <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                     <ul class="kt-menu__subnav">



                     <li class="kt-menu__item " aria-haspopup="true">
                             <a href="javascript:void(0)" data-toggle="modal" data-target="#kt_modal_4_Support"  class="kt-menu__link ">
                                 <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                 <span class="kt-menu__link-text">Support Tickets</span>
                             </a>
                         </li>


                     </ul>
                 </div>
             </li>
             <!-- content sites  -->




         </ul>
     </div>
 </div>
 <!-- end:: Aside Menu -->

