<?php /*<!DOCTYPE html>
<html lang="en">

    <head>
        {!! meta_init() !!}
        <meta name="keywords" content="@get('keywords')">
        <meta name="description" content="@get('description')">
        <meta name="author" content="@get('author')">
    
        <title>@get('title')</title>

        @styles()
        
    </head>

    <body>
        @partial('header')

        @content()

        @partial('footer')

        @scripts()
    </body>

</html>
*/?>




<!DOCTYPE html>

<html lang="en" >
    <!-- begin::Head -->
<head>
        <meta charset="utf-8"/>
        
        <title>@get('title')</title>
        
        {!! meta_init() !!}
        <meta name="keywords" content="@get('keywords')">
        <meta name="description" content="@get('description')">
        <meta name="author" content="@get('author')">




        <!--begin::Fonts -->
        <script src="http://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
            WebFont.load({
                google: {"families":["Lato:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>
        <!--end::Fonts -->

        <!--begin::Page Vendors Styles(used by this page) -->        
        <link rel="stylesheet" href="{{ asset('ayra/public/themes/user/ayracore/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}">
        <link rel="stylesheet" href="{{ asset('ayra/public/themes/user/ayracore/assets/vendors/base/vendors.bundle.css')}}">        
        <link rel="stylesheet" href="{{ asset('ayra/public/themes/user/ayracore/assets/demo/default/base/style.bundle.css')}}">       
        <link rel="stylesheet" href="{{ asset('ayra/public/themes/user/ayracore/assets/demo/default/skins/header/base/light.css')}}">                
        <link rel="stylesheet" href="{{ asset('ayra/public/themes/user/ayracore/assets/demo/default/skins/header/menu/light.css')}}">        
        <link rel="stylesheet" href="{{ asset('ayra/public/themes/user/ayracore/assets/demo/default/skins/brand/dark.css')}}">        
        <link rel="stylesheet" href="{{ asset('ayra/public/themes/user/ayracore/assets/demo/default/skins/aside/dark.css')}}">               
       

        <link rel="shortcut icon" href="{{ asset('ayra/public/themes/user/ayracore/assets/media/logos/favicon.ico')}} " />
    
</head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body  class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading"  >

    @partial('header')   
    @content()
    @partial('footer')
