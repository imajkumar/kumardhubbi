<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
   

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Vendors
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                       
                        <a href="{{route('addVendor')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New Vendor
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
            <!--begin: Search Form -->
            <form class="kt-form kt-form--fit kt-margin-b-20">
                <div class="row kt-margin-b-20">
                    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                        <label>Vendor ID:</label>
                        <input type="text" class="form-control kt-input" placeholder="" data-col-index="1">
                    </div>
                    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                        <label>Name:</label>
                        <input type="text" class="form-control kt-input" placeholder="" data-col-index="2">
                    </div>
                   
                    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                        <label>Phone:</label>
                        <input type="text" class="form-control kt-input" placeholder="" data-col-index="4">
                    </div>
                    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <button style="margin-top:25px" class="btn btn-warning btn-brand--icon" id="kt_search">
                            <span>
                                <i class="la la-search"></i>
                                <span>Search</span>
                            </span>
                     </button>
                     <button style="margin-top:25px" class="btn btn-secondary btn-secondary--icon" id="kt_reset">
                            <span>
                                <i class="la la-close"></i>
                                <span>Reset</span>
                            </span>
                        </button>

                    </div>
                </div>

             

             

               
            </form>
            <!--begin: Datatable -->

            <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>

            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1_vendorList">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>VID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Web URL</th>
                        <th>Website</th>
                        <th>Created</th>
                        <th>Actions</th>
                    </tr>
                </thead>


                
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->