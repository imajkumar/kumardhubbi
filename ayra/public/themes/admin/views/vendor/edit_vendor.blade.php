<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<!--begin::Portlet-->
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Vendor Details
						</h3>
					</div>
                </div>
                

                <!-- tab  -->
                <div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-toolbar">
                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-success nav-tabs-line-2x" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_portlet_basic_vendero_tab_content" role="tab">
                                <i class="la la-cog"></i> Basic
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_web_settings_content" role="tab">
                                <i class="la la-briefcase"></i> Web Settings
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_logs_content" role="tab">
                                <i class="la la-bell-o"></i>Logs
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane active" id="kt_portlet_basic_vendero_tab_content" role="tabpanel">
                        <!-- basic form -->
                        <!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1" method="post" data-redirect="vendors" action="{{route('updateVendor')}}">
					@csrf
					<div class="kt-portlet__body">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
                        </div>
                       
                        <input type="hidden" name="vid" value="{{$vendor_data->vid}}">
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" value="{{optional($vendor_data)->name}}" name="name" placeholder="Enter your full name">
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" value="{{optional($vendor_data)->email}}"  name="email" placeholder="Enter your email">
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Phone *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" value="{{optional($vendor_data)->phone}}" name="phone" placeholder="Enter your phone">
								<span class="form-text text-muted"></span>
							</div>
                        </div>
                        
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">URL *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="input-group">
									<input type="text" class="form-control" value="{{optional($vendor_data)->web_url}}"  name="web_url" placeholder="Enter your URL">
									<div class="input-group-append"><span  class="input-group-text">domain</span></div>
								</div>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Web Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="input-group">
									<input type="text" class="form-control" value="{{optional($vendor_data)->web_name}}" name="web_name" placeholder="Enter your URL">
									<div class="input-group-append"><span class="input-group-text">Website Name</span></div>
								</div>
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Remarks *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<textarea class="form-control" name="notes" placeholder="Enter a Notes" rows="4">{{optional($vendor_data)->notes}}</textarea>
								<span class="form-text text-muted"></span>
							</div>
						</div>


					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">

									<button class="btn btn-brand" data-ktwizard-type="action-submit">
										Submit
									</button>

									<button type="reset" onclick="goBack()" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->

                        <!-- basic form -->
                    </div>
                    <div class="tab-pane" id="kt_portlet_base_web_settings_content" role="tabpanel">
                        <!-- web settigs -->
                        <!--begin::Form-->
				<form enctype="multipart/form-data" class="kt-form kt-form--label-right" id="kt_form_1_web_settigs_admin" method="post" data-redirect="vendors" action="{{route('saveAdminWebsiteSettings')}}">
					@csrf
					<div class="kt-portlet__body">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
                        </div>
                        <input type="hidden" name="vid" value="{{$vendor_data->vid}}">
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Logo *</label>
							<div class="col-lg-6 col-md-6 col-sm-12">                           
                                <div class="custom-file">
                                <input type="file" name="logo" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">logo</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <?php
                                if(optional($web_setting)->web_logo!=null){
                                    $aa=asset('uploads/img/logo')."/".optional($web_setting)->web_logo;

                                    ?>
                                   
                                <img width="60" src="{{$aa}}">
                                    <?php
                                }
                                
                                ?>
								
							</div>
						</div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Favicon *</label>
							<div class="col-lg-6 col-md-6 col-sm-12">                           
                                <div class="custom-file">
                                <input type="file" name="favicon" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">logo</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                            <?php
                                if(optional($web_setting)->favicon!=null){
                                    $aa=asset('uploads/img/logo')."/".optional($web_setting)->favicon;

                                    ?>
                                   
                                <img width="60" src="{{$aa}}">
                                    <?php
                                }
                                
                                ?>

							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Meta keywords *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea class="form-control" name="meta_keywords" id="exampleTextarea" rows="2">{{optional($web_setting)->meta_keywords}}</textarea>

								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Meta Discription *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea class="form-control" name="meta_discription"  id="exampleTextarea" rows="2">{{optional($web_setting)->meta_discription}}</textarea>

								<span class="form-text text-muted"></span>
							</div>
						</div>
						

						


					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">

									<button class="btn btn-brand" data-ktwizard-type="action-submit">
										Submit
									</button>

									<button type="reset" onclick="goBack()" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</form>
                <!--end::Form-->
                
                        <!-- web settigs -->
                    </div>
                    <div class="tab-pane" id="kt_portlet_base_logs_content" role="tabpanel">
                        <!-- aj log -->
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="post" data-redirect="vendors" action="{{route('updateVendor')}}">
					@csrf
					<div class="kt-portlet__body">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
                        </div>
                       
                        <input type="hidden" name="vid" value="{{$vendor_data->vid}}">
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" value="{{optional($vendor_data)->name}}" name="name" placeholder="Enter your full name">
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" value="{{optional($vendor_data)->email}}"  name="email" placeholder="Enter your email">
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Phone *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" value="{{optional($vendor_data)->phone}}" name="phone" placeholder="Enter your phone">
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Select Images *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" value="{{optional($vendor_data)->phone}}" name="phone" placeholder="Enter your phone">
								<span class="form-text text-muted"></span>
							</div>
                        </div>
                        
						
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Web Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="input-group">
									<input type="text" class="form-control" value="{{optional($vendor_data)->web_name}}" name="web_name" placeholder="Enter your URL">
									<div class="input-group-append"><span class="input-group-text">Website Name</span></div>
								</div>
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Remarks *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<textarea class="form-control" name="notes" placeholder="Enter a Notes" rows="4">{{optional($vendor_data)->notes}}</textarea>
								<span class="form-text text-muted"></span>
							</div>
						</div>


					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">

									<button class="btn btn-brand" data-ktwizard-type="action-submit">
										Submit
									</button>

									<button type="reset" onclick="goBack()" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</form>
                        <!-- aj log -->
                    </div>
                </div>      
            </div>
        </div>
                <!-- tab  -->
				
			</div>
			<!--end::Portlet-->
		</div>



	</div>
</div>
<!-- end:: Content -->