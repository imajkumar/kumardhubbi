<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<!--begin::Portlet-->
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Static Block
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">

								<a href="{{route('add_admin_static_block')}}"  class="btn btn-brand btn-elevate btn-icon-sm">
									<i class="la la-plus"></i>
									ADD STATIC PAGE
								</a>
							</div>
						</div>
					</div>
				</div>
                <div class="kt-portlet__body">
            

            <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>

            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1_vendor_static_block_List">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Block Title</th>
                        <th>Module</th>
                        <th>Status</th>                                              
                        <th>Created By</th>                                              
                        <th>Actions</th>
                    </tr>
                </thead>


                
            </table>
            <!--end: Datatable -->
        </div>
			</div>
			<!--end::Portlet-->
		</div>



	</div>
</div>
<!-- end:: Content -->