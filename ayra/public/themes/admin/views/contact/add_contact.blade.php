<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<!--begin::Portlet-->
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Add Contact 
						</h3>
					</div>
				</div>
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1" method="post" data-redirect="vendors" action="{{route('saveContact')}}">
					@csrf
					<div class="kt-portlet__body">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">vendor *</label>
                            
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control" name="vid" id="">
                                <?php 
                            $vendors = DB::table('vendors')->get();
                            foreach ($vendors as $key => $rowData) {
                               ?>
                                <option value="{{$rowData->vid}}">{{$rowData->name}}</option>
                               <?php
                            }

                            ?>
                                </select>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="name" placeholder="Enter your full name">
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="email" placeholder="Enter your email">
								<span class="form-text text-muted"></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Phone *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="phone" placeholder="Enter your phone">
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Office Phone *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="office_phone" placeholder="Enter your phone">
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Fax No *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="fax_phone" placeholder="Enter your phone">
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12"> Office Address*</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="site_address" placeholder="Enter your phone">
								<span class="form-text text-muted"></span>
							</div>
                        </div>
                        <div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Facebook *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="facebook" placeholder="Facebook">
								<span class="form-text text-muted"></span>
							</div>
                        </div>
                        <div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Twitter *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="twitter" placeholder="Twitter">
								<span class="form-text text-muted"></span>
							</div>
                        </div>
                        <div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Instagram *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="instagram" placeholder="Instagram">
								<span class="form-text text-muted"></span>
							</div>
                        </div>
                        <div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">LinkedIn *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="linkedin" placeholder="LinkedIn">
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Youtube *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="youtube" placeholder="Youtube">
								<span class="form-text text-muted"></span>
							</div>
						</div>
						
						

						


					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">

									<button class="btn btn-brand" data-ktwizard-type="action-submit">
										Submit
									</button>

									<button type="button" onclick="goBack()" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>



	</div>
</div>
<!-- end:: Content -->