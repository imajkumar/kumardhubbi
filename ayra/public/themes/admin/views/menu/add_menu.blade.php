
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<!--begin::Portlet-->

	
	

	<div class="row">
		<div class="col-lg-4">
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Menu Item
						</h3>
						<ul class="kt-nav">
							<li class="kt-nav__head">
								<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Create the parent as well as child menus for the desired location. You can use this input/these inputs, if you wish to create menu which does not contain data in it, but wants to redirect users to particular page."></i>
							</li>
						</ul>
					</div>
				</div>
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1" method="post" data-redirect="vendors" action="{{route('saveVendor')}}">
					@csrf
					<div class="kt-portlet__body">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Title *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="name" placeholder="Enter your Title">
								<span class="form-text text-muted"></span>
							</div>
						</div>


						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">URL *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="input-group">
									<input type="text" class="form-control" name="web_url" placeholder="http://www.samplewebsite.com/mychoicepage">

								</div>
								<span class="form-text text-muted">You can specify internal as well as external web link.</span>
							</div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">

									<button class="btn btn-brand" data-ktwizard-type="action-submit">
										Add
									</button>

									<button type="reset" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Portlet-->

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Pages
						</h3>
						<!-- <ul class="kt-nav">
							<li class="kt-nav__head">
								<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Create the parent as well as child menus for the desired location. You can use this input/these inputs, if you wish to create menu which does not contain data in it, but wants to redirect users to particular page."></i>
							</li>
						</ul> -->
					</div>
				</div>
				<!--begin::Form-->


				<!--end::Form-->
			</div>
			<!--end::Portlet-->

		</div>

		<div class="col-lg-8">
			<style>
				.kt-wizard-v2 .kt-wizard-v2__aside {
					-webkit-box-flex: 0;
					-ms-flex: 0 0 200px;
					flex: 0 0 200px;
					display: -webkit-box;
					display: -ms-flexbox;
					display: flex;
					width: 200px;
					border-right: 1px solid #eeeef4;
					padding: 0.5rem 2.5rem 0.5rem 1.5rem;
				}
			</style>
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Header Menu
						</h3>
						<ul class="kt-nav">
							<li class="kt-nav__head">
								<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Create the parent as well as child menus for the desired location. You can use this input/these inputs, if you wish to create menu which does not contain data in it, but wants to redirect users to particular page."></i>
							</li>
						</ul>
					</div>
				</div>
				<!--begin::Form-->
				<div class="kt-portlet__body">

					<!-- ajcode -->
					<!-- begin:: Content -->



					<div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
						<div class="kt-grid__item kt-wizard-v2__aside">
							<!--begin: Form Wizard Nav -->
							<div class="kt-wizard-v2__nav">
								<!--doc: Remove "kt-wizard-v2__nav-items--clickable" class and also set 'clickableSteps: false' in the JS init to disable manually clicking step titles -->
								<div class="kt-wizard-v2__nav-items kt-wizard-v2__nav-items--clickable">
									<div class="kt-wizard-v2__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
										<div class="kt-wizard-v2__nav-body">

											<div class="kt-wizard-v2__nav-label">
												<div class="kt-wizard-v2__nav-label-title">
													Header Menu
												</div>

											</div>
										</div>
									</div>
									<div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
										<div class="kt-wizard-v2__nav-body">

											<div class="kt-wizard-v2__nav-label">
												<div class="kt-wizard-v2__nav-label-title">
													Footer Menu
												</div>

											</div>
										</div>
									</div>
									<div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
										<div class="kt-wizard-v2__nav-body">

											<div class="kt-wizard-v2__nav-label">
												<div class="kt-wizard-v2__nav-label-title">
													Privacy Policy
												</div>

											</div>
										</div>
									</div>



								</div>
							</div>
							<!--end: Form Wizard Nav -->
						</div>
						<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">
							<!--begin: Form Wizard Form-->
							<form class="kt-form" id="kt_form">
								<!--begin: Form Wizard Step 1-->
								<div class="kt-wizard-v2__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
									<!-- header -->
									<div class="dd" id="nestable_list_2">
						<ol class="dd-list">
							<li class="dd-item" data-id="13">
								<div class="dd-handle"> Item 13 </div>
							</li>
							<li class="dd-item" data-id="14">
								<div class="dd-handle"> Item 14 </div>
							</li>
							<li class="dd-item" data-id="15">
								<div class="dd-handle"> Item 15 </div>
								<ol class="dd-list">
									<li class="dd-item" data-id="16">
										<div class="dd-handle"> Item 16 </div>
									</li>
									<li class="dd-item" data-id="17">
										<div class="dd-handle"> Item 17 </div>
									</li>
									<li class="dd-item" data-id="18">
										<div class="dd-handle"> Item 18 </div>
									</li>
								</ol>
							</li>
						</ol>
					</div>
									<!-- header -->
								</div>
								<!--end: Form Wizard Step 1-->

								<!--begin: Form Wizard Step 2-->
								<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
									<div class="kt-heading kt-heading--md">Setup Your Current Location</div>
									<div class="kt-form__section kt-form__section--first">
										<div class="kt-wizard-v2__form">
											<div class="row">
												<div class="col-xl-6">
													<div class="form-group">
														<label>Address Line 1</label>
														<input type="text" class="form-control" name="address1" placeholder="Address Line 1" value="Address Line 1">
														<span class="form-text text-muted">Please enter your Address.</span>
													</div>
												</div>
												<div class="col-xl-6">
													<div class="form-group">
														<label>Address Line 2</label>
														<input type="text" class="form-control" name="address2" placeholder="Address Line 2" value="Address Line 2">
														<span class="form-text text-muted">Please enter your Address.</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xl-6">
													<div class="form-group">
														<label>Postcode</label>
														<input type="text" class="form-control" name="postcode" placeholder="Postcode" value="3000">
														<span class="form-text text-muted">Please enter your Postcode.</span>
													</div>
												</div>
												<div class="col-xl-6">
													<div class="form-group">
														<label>City</label>
														<input type="text" class="form-control" name="city" placeholder="City" value="Melbourne">
														<span class="form-text text-muted">Please enter your City.</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xl-6">
													<div class="form-group">
														<label>State</label>
														<input type="text" class="form-control" name="state" placeholder="State" value="VIC">
														<span class="form-text text-muted">Please enter your Postcode.</span>
													</div>
												</div>
												<div class="col-xl-6">
													<div class="form-group">
														<label>Country:</label>
														<select name="country" class="form-control">
															<option value="">Select</option>
															<option value="AF">Afghanistan</option>
															<option value="AX">Åland Islands</option>
															<option value="AL">Albania</option>
															<option value="DZ">Algeria</option>
															<option value="AS">American Samoa</option>
															<option value="AD">Andorra</option>
															<option value="AO">Angola</option>
															<option value="AI">Anguilla</option>
															<option value="AQ">Antarctica</option>
															<option value="AG">Antigua and Barbuda</option>
															<option value="AR">Argentina</option>
															<option value="AM">Armenia</option>
															<option value="AW">Aruba</option>
															<option value="AU" selected>Australia</option>
															<option value="AT">Austria</option>
															<option value="AZ">Azerbaijan</option>
															<option value="BS">Bahamas</option>
															<option value="BH">Bahrain</option>
															<option value="BD">Bangladesh</option>
															<option value="BB">Barbados</option>
															<option value="BY">Belarus</option>
															<option value="BE">Belgium</option>
															<option value="BZ">Belize</option>
															<option value="BJ">Benin</option>
															<option value="BM">Bermuda</option>
															<option value="BT">Bhutan</option>
															<option value="BO">Bolivia, Plurinational State of</option>
															<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
															<option value="BA">Bosnia and Herzegovina</option>
															<option value="BW">Botswana</option>
															<option value="BV">Bouvet Island</option>
															<option value="BR">Brazil</option>
															<option value="IO">British Indian Ocean Territory</option>
															<option value="BN">Brunei Darussalam</option>
															<option value="BG">Bulgaria</option>
															<option value="BF">Burkina Faso</option>
															<option value="BI">Burundi</option>
															<option value="KH">Cambodia</option>
															<option value="CM">Cameroon</option>
															<option value="CA">Canada</option>
															<option value="CV">Cape Verde</option>
															<option value="KY">Cayman Islands</option>
															<option value="CF">Central African Republic</option>
															<option value="TD">Chad</option>
															<option value="CL">Chile</option>
															<option value="CN">China</option>
															<option value="CX">Christmas Island</option>
															<option value="CC">Cocos (Keeling) Islands</option>
															<option value="CO">Colombia</option>
															<option value="KM">Comoros</option>
															<option value="CG">Congo</option>
															<option value="CD">Congo, the Democratic Republic of the</option>
															<option value="CK">Cook Islands</option>
															<option value="CR">Costa Rica</option>
															<option value="CI">Côte d'Ivoire</option>
															<option value="HR">Croatia</option>
															<option value="CU">Cuba</option>
															<option value="CW">Curaçao</option>
															<option value="CY">Cyprus</option>
															<option value="CZ">Czech Republic</option>
															<option value="DK">Denmark</option>
															<option value="DJ">Djibouti</option>
															<option value="DM">Dominica</option>
															<option value="DO">Dominican Republic</option>
															<option value="EC">Ecuador</option>
															<option value="EG">Egypt</option>
															<option value="SV">El Salvador</option>
															<option value="GQ">Equatorial Guinea</option>
															<option value="ER">Eritrea</option>
															<option value="EE">Estonia</option>
															<option value="ET">Ethiopia</option>
															<option value="FK">Falkland Islands (Malvinas)</option>
															<option value="FO">Faroe Islands</option>
															<option value="FJ">Fiji</option>
															<option value="FI">Finland</option>
															<option value="FR">France</option>
															<option value="GF">French Guiana</option>
															<option value="PF">French Polynesia</option>
															<option value="TF">French Southern Territories</option>
															<option value="GA">Gabon</option>
															<option value="GM">Gambia</option>
															<option value="GE">Georgia</option>
															<option value="DE">Germany</option>
															<option value="GH">Ghana</option>
															<option value="GI">Gibraltar</option>
															<option value="GR">Greece</option>
															<option value="GL">Greenland</option>
															<option value="GD">Grenada</option>
															<option value="GP">Guadeloupe</option>
															<option value="GU">Guam</option>
															<option value="GT">Guatemala</option>
															<option value="GG">Guernsey</option>
															<option value="GN">Guinea</option>
															<option value="GW">Guinea-Bissau</option>
															<option value="GY">Guyana</option>
															<option value="HT">Haiti</option>
															<option value="HM">Heard Island and McDonald Islands</option>
															<option value="VA">Holy See (Vatican City State)</option>
															<option value="HN">Honduras</option>
															<option value="HK">Hong Kong</option>
															<option value="HU">Hungary</option>
															<option value="IS">Iceland</option>
															<option value="IN">India</option>
															<option value="ID">Indonesia</option>
															<option value="IR">Iran, Islamic Republic of</option>
															<option value="IQ">Iraq</option>
															<option value="IE">Ireland</option>
															<option value="IM">Isle of Man</option>
															<option value="IL">Israel</option>
															<option value="IT">Italy</option>
															<option value="JM">Jamaica</option>
															<option value="JP">Japan</option>
															<option value="JE">Jersey</option>
															<option value="JO">Jordan</option>
															<option value="KZ">Kazakhstan</option>
															<option value="KE">Kenya</option>
															<option value="KI">Kiribati</option>
															<option value="KP">Korea, Democratic People's Republic of</option>
															<option value="KR">Korea, Republic of</option>
															<option value="KW">Kuwait</option>
															<option value="KG">Kyrgyzstan</option>
															<option value="LA">Lao People's Democratic Republic</option>
															<option value="LV">Latvia</option>
															<option value="LB">Lebanon</option>
															<option value="LS">Lesotho</option>
															<option value="LR">Liberia</option>
															<option value="LY">Libya</option>
															<option value="LI">Liechtenstein</option>
															<option value="LT">Lithuania</option>
															<option value="LU">Luxembourg</option>
															<option value="MO">Macao</option>
															<option value="MK">Macedonia, the former Yugoslav Republic of</option>
															<option value="MG">Madagascar</option>
															<option value="MW">Malawi</option>
															<option value="MY">Malaysia</option>
															<option value="MV">Maldives</option>
															<option value="ML">Mali</option>
															<option value="MT">Malta</option>
															<option value="MH">Marshall Islands</option>
															<option value="MQ">Martinique</option>
															<option value="MR">Mauritania</option>
															<option value="MU">Mauritius</option>
															<option value="YT">Mayotte</option>
															<option value="MX">Mexico</option>
															<option value="FM">Micronesia, Federated States of</option>
															<option value="MD">Moldova, Republic of</option>
															<option value="MC">Monaco</option>
															<option value="MN">Mongolia</option>
															<option value="ME">Montenegro</option>
															<option value="MS">Montserrat</option>
															<option value="MA">Morocco</option>
															<option value="MZ">Mozambique</option>
															<option value="MM">Myanmar</option>
															<option value="NA">Namibia</option>
															<option value="NR">Nauru</option>
															<option value="NP">Nepal</option>
															<option value="NL">Netherlands</option>
															<option value="NC">New Caledonia</option>
															<option value="NZ">New Zealand</option>
															<option value="NI">Nicaragua</option>
															<option value="NE">Niger</option>
															<option value="NG">Nigeria</option>
															<option value="NU">Niue</option>
															<option value="NF">Norfolk Island</option>
															<option value="MP">Northern Mariana Islands</option>
															<option value="NO">Norway</option>
															<option value="OM">Oman</option>
															<option value="PK">Pakistan</option>
															<option value="PW">Palau</option>
															<option value="PS">Palestinian Territory, Occupied</option>
															<option value="PA">Panama</option>
															<option value="PG">Papua New Guinea</option>
															<option value="PY">Paraguay</option>
															<option value="PE">Peru</option>
															<option value="PH">Philippines</option>
															<option value="PN">Pitcairn</option>
															<option value="PL">Poland</option>
															<option value="PT">Portugal</option>
															<option value="PR">Puerto Rico</option>
															<option value="QA">Qatar</option>
															<option value="RE">Réunion</option>
															<option value="RO">Romania</option>
															<option value="RU">Russian Federation</option>
															<option value="RW">Rwanda</option>
															<option value="BL">Saint Barthélemy</option>
															<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
															<option value="KN">Saint Kitts and Nevis</option>
															<option value="LC">Saint Lucia</option>
															<option value="MF">Saint Martin (French part)</option>
															<option value="PM">Saint Pierre and Miquelon</option>
															<option value="VC">Saint Vincent and the Grenadines</option>
															<option value="WS">Samoa</option>
															<option value="SM">San Marino</option>
															<option value="ST">Sao Tome and Principe</option>
															<option value="SA">Saudi Arabia</option>
															<option value="SN">Senegal</option>
															<option value="RS">Serbia</option>
															<option value="SC">Seychelles</option>
															<option value="SL">Sierra Leone</option>
															<option value="SG">Singapore</option>
															<option value="SX">Sint Maarten (Dutch part)</option>
															<option value="SK">Slovakia</option>
															<option value="SI">Slovenia</option>
															<option value="SB">Solomon Islands</option>
															<option value="SO">Somalia</option>
															<option value="ZA">South Africa</option>
															<option value="GS">South Georgia and the South Sandwich Islands</option>
															<option value="SS">South Sudan</option>
															<option value="ES">Spain</option>
															<option value="LK">Sri Lanka</option>
															<option value="SD">Sudan</option>
															<option value="SR">Suriname</option>
															<option value="SJ">Svalbard and Jan Mayen</option>
															<option value="SZ">Swaziland</option>
															<option value="SE">Sweden</option>
															<option value="CH">Switzerland</option>
															<option value="SY">Syrian Arab Republic</option>
															<option value="TW">Taiwan, Province of China</option>
															<option value="TJ">Tajikistan</option>
															<option value="TZ">Tanzania, United Republic of</option>
															<option value="TH">Thailand</option>
															<option value="TL">Timor-Leste</option>
															<option value="TG">Togo</option>
															<option value="TK">Tokelau</option>
															<option value="TO">Tonga</option>
															<option value="TT">Trinidad and Tobago</option>
															<option value="TN">Tunisia</option>
															<option value="TR">Turkey</option>
															<option value="TM">Turkmenistan</option>
															<option value="TC">Turks and Caicos Islands</option>
															<option value="TV">Tuvalu</option>
															<option value="UG">Uganda</option>
															<option value="UA">Ukraine</option>
															<option value="AE">United Arab Emirates</option>
															<option value="GB">United Kingdom</option>
															<option value="US">United States</option>
															<option value="UM">United States Minor Outlying Islands</option>
															<option value="UY">Uruguay</option>
															<option value="UZ">Uzbekistan</option>
															<option value="VU">Vanuatu</option>
															<option value="VE">Venezuela, Bolivarian Republic of</option>
															<option value="VN">Viet Nam</option>
															<option value="VG">Virgin Islands, British</option>
															<option value="VI">Virgin Islands, U.S.</option>
															<option value="WF">Wallis and Futuna</option>
															<option value="EH">Western Sahara</option>
															<option value="YE">Yemen</option>
															<option value="ZM">Zambia</option>
															<option value="ZW">Zimbabwe</option>
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end: Form Wizard Step 2-->

								<!--begin: Form Wizard Step 3-->
								<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
									<div class="kt-heading kt-heading--md">Select your Services</div>
									<div class="kt-form__section kt-form__section--first">
										<div class="kt-wizard-v2__form">
											<div class="form-group">
												<label>Delivery Type</label>
												<select name="delivery" class="form-control">
													<option value="">Select a Service Type Option</option>
													<option value="overnight" selected>Overnight Delivery (within 48 hours)</option>
													<option value="express">Express Delivery (within 5 working days)</option>
													<option value="basic">Basic Delivery (within 5 - 10 working days)</option>
												</select>
											</div>
											<div class="form-group">
												<label>Packaging Type</label>
												<select name="packaging" class="form-control">
													<option value="">Select a Packaging Type Option</option>
													<option value="regular" selected>Regular Packaging</option>
													<option value="oversized">Oversized Packaging</option>
													<option value="fragile">Fragile Packaging</option>
													<option value="frozen">Frozen Packaging</option>
												</select>
											</div>
											<div class="form-group">
												<label>Preferred Delivery Window</label>
												<select name="preferreddelivery" class="form-control">
													<option value="">Select a Preferred Delivery Option</option>
													<option value="morning" selected>Morning Delivery (8:00AM - 11:00AM)</option>
													<option value="afternoon">Afternoon Delivery (11:00AM - 3:00PM)</option>
													<option value="evening">Evening Delivery (3:00PM - 7:00PM)</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<!--end: Form Wizard Step 3-->




							</form>
							<!--end: Form Wizard Form-->
						</div>
					</div>


					<!-- end:: Content -->
					<!-- ajcode -->




					<!-- <div id="kt_tree_5" class="tree-demo">
					</div> -->
					<!--
					<div class="dd" id="nestable_list_2"><ol class="dd-list menu_list_set"><li class="dd-item" data-order="1" data-id="1"><div class="dd-handle">
					<a href="/">Home</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="1" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a></div></li>
				<li class="dd-item" data-order="2" data-id="1875"><div class="dd-handle">
					<a href="about">About</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="1875" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox1875" class="activeItem" type="checkbox" checked="" value="1875">
						<label title="Deactive Menu" for="checkbox1875">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span><span class="megamenu_info">
					<span class="md-checkbox megamenu_view menu_active">
							<input style="opacity:0" id="checkbox1875-megaMenu" class="megaMenu" type="checkbox" value="1875">
								<label title="Add mega menu" for="checkbox1875-megaMenu">
								<span></span>
								<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Remove mega menu"></span>
								<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Add mega menu"></span>
							</label>
						</span>
						
						</span>
						</div><ol class="dd-list menu_list_set"><li class="dd-item" data-order="1" data-id="2957"><div class="dd-handle">
					<a href="about">Diamond Properties</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2957" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2957" class="activeItem" type="checkbox" checked="" value="2957">
						<label title="Deactive Menu" for="checkbox2957">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="2" data-id="2959"><div class="dd-handle">
					<a href="the-cayman-islands">The Cayman Islands</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2959" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2959" class="activeItem" type="checkbox" checked="" value="2959">
						<label title="Deactive Menu" for="checkbox2959">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="3" data-id="2924"><div class="dd-handle">
					<a href="communities">Communities</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2924" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2924" class="activeItem" type="checkbox" checked="" value="2924">
						<label title="Deactive Menu" for="checkbox2924">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="4" data-id="2958"><div class="dd-handle">
					<a href="cireba">CIREBA</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2958" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2958" class="activeItem" type="checkbox" checked="" value="2958">
						<label title="Deactive Menu" for="checkbox2958">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li></ol><input type="hidden" value="Y" id="menuActive"></li><li class="dd-item" data-order="3" data-id="7"><div class="dd-handle">
					<a href="contact-diamond-properties">Contact</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="7" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox7" class="activeItem" type="checkbox" checked="" value="7">
						<label title="Deactive Menu" for="checkbox7">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="4" data-id="4"><div class="dd-handle">
					<a href="our-team">Our Team</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="4" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox4" class="activeItem" type="checkbox" checked="" value="4">
						<label title="Deactive Menu" for="checkbox4">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span><span class="megamenu_info">
					<span class="md-checkbox megamenu_view menu_active">
							<input style="opacity:0" id="checkbox4-megaMenu" class="megaMenu" type="checkbox" value="4">
								<label title="Add mega menu" for="checkbox4-megaMenu">
								<span></span>
								<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Remove mega menu"></span>
								<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Add mega menu"></span>
							</label>
						</span>
						
						</span>
						</div><ol class="dd-list menu_list_set"><li class="dd-item" data-order="1" data-id="2960"><div class="dd-handle">
					<a href="testimonials">Testimonials</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2960" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2960" class="activeItem" type="checkbox" checked="" value="2960">
						<label title="Deactive Menu" for="checkbox2960">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li></ol><input type="hidden" value="Y" id="menuActive"></li><li class="dd-item" data-order="5" data-id="2923"><div class="dd-handle">
					<a href="buying">Buying</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2923" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2923" class="activeItem" type="checkbox" checked="" value="2923">
						<label title="Deactive Menu" for="checkbox2923">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="6" data-id="2966"><div class="dd-handle">
					<a href="rentals">Rentals</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2966" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2966" class="activeItem" type="checkbox" checked="" value="2966">
						<label title="Deactive Menu" for="checkbox2966">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="7" data-id="2921"><div class="dd-handle">
					<a href="selling">Selling</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2921" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2921" class="activeItem" type="checkbox" checked="" value="2921">
						<label title="Deactive Menu" for="checkbox2921">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="8" data-id="2964"><div class="dd-handle">
					<a href="ldx-feed">More MLS Listings (LDX)</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2964" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2964" class="activeItem" type="checkbox" checked="" value="2964">
						<label title="Deactive Menu" for="checkbox2964">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="9" data-id="1652"><div class="dd-handle">
					<a href="news-blog">News &amp; Blog</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="1652" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox1652" class="activeItem" type="checkbox" checked="" value="1652">
						<label title="Deactive Menu" for="checkbox1652">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="10" data-id="2947"><div class="dd-handle">
					<a href="all-properties">All Properties</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2947" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2947" class="activeItem" type="checkbox" checked="" value="2947">
						<label title="Deactive Menu" for="checkbox2947">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label></span></div></li><li class="dd-item" data-order="11" data-id="2971"><div class="dd-handle">
					<a href="contact-diamond-properties">Contact</a>
					</div>
				<div class="actions"><a data-toggle="tooltip" data-placement="top" title="" data-id="2971" class="editItem edit_icon_bg" data-original-title="Edit">
				<i class="fa fa-pencil"></i></a><span class="md-checkbox menu_active">
					<input style="opacity:0" id="checkbox2971" class="activeItem" type="checkbox" checked="" value="2971">
						<label title="Deactive Menu" for="checkbox2971">
							<span></span>
							<span class="check tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Deactive Menu &amp; unpublish"></span>
							<span class="box tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Active Menu &amp; publish"></span>
						</label>
					</span>
				</div>
			</li>
		</ol>
		<input type="hidden" value="Y" id="menuActive"></div>
									-->
					<!-- <div class="overflow_select">
						<select class="form-control menu_control" size="3" id="menuPosition">
							<option class="active" value="1">Header Menu</option>
							<option value="2">Footer Menu</option>
							<option value="3">Privacy Policy</option>
						</select>
					</div> -->


					

				</div>
				<!--end::Form-->
			</div>
			<!--end::Portlet-->



		</div>



	</div>
</div>
<!-- end:: Content -->