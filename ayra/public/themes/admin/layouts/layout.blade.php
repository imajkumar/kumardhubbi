<?php /*
<!DOCTYPE html>
<html lang="en">

    <head>
        {!! meta_init() !!}
        <meta name="keywords" content="@get('keywords')">
        <meta name="description" content="@get('description')">
        <meta name="author" content="@get('author')">
    
        <title>@get('title')</title>

        @styles()
        
    </head>

    <body>
        @partial('header')

        @content()

        @partial('footer')

        @scripts()
    </body>

</html>
*/
?>

<!DOCTYPE html>

<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />

    <title>Control Access | Diamond Properties</title>
    <meta name="description" content="Aside light skin example">
    <meta name="author" content="@get('author')">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->
    <meta name="BASE_URL" content="{{ url('/') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('ayra/core/css/jstree.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css" rel="stylesheet" type="text/css" />
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('ayra/core/light/css/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/light/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/css/jstree.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/css/wizard-2.css')}}" rel="stylesheet" type="text/css" />
    <!-- <link href="{{ asset('ayra/core/css/dropzone.css')}}" rel="stylesheet" type="text/css" /> -->
    <link href="https://transloadit.edgly.net/releases/uppy/v1.14.1/uppy.min.css" rel="stylesheet">

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <link href="{{ asset('ayra/core/light/css/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/light/css/menu_light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/light/css/brand_light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/core/light/css/aside_light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('ayra/public/vendor/harimayco-menu/style.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Layout Skins -->


    <link rel="shortcut icon" type="image/x-icon" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="144x144" href=" https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="57x57" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />



</head>
<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="#">
                <img alt="Logo" src="https://www.diamondproperties.ky/assets/images/powrpanel_logo.png" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>



            <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
        </div>
    </div>
    <!-- end:: Header Mobile -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            <!-- begin:: Aside -->

            <!-- Uncomment this to display the close button of the panel -->
            <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>


            <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
                <!-- begin:: Aside -->
                <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                    <div class="kt-aside__brand-logo">
                        <a href="">
                            <img alt="Logo" src="https://www.diamondproperties.ky/assets/images/powrpanel_logo.png" width="80" />
                        </a>
                    </div>

                    <div class="kt-aside__brand-tools">
                        <button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
                            <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
                                        <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
                                    </g>
                                </svg></span>
                            <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                    </g>
                                </svg></span>
                        </button>
                        <!--
			<button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
			-->
                    </div>
                </div>
                <!-- end:: Aside -->
                @partial('aside')


            </div>
            <!-- end:: Aside -->

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                <!-- begin:: Header -->
                @partial('header')
                <!-- begin:: Content -->
                @content()
                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            @partial('footer')
            <!-- end:: Footer -->
        </div>
    </div>
    </div>
    <!-- end:: Page -->
    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <!-- end::Scrolltop -->
    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>
    <!-- end::Global Config -->


    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('ayra/core/js/plugins.bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->
    <script type="text/javascript">
        BASE_URL = $('meta[name="BASE_URL"]').attr('content');
        CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    </script>


    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('ayra/core/js/datatables.bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/advanced-search.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/jstree.bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/treeview.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/css/dropzone_bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/dropzone.js')}}" type="text/javascript"></script>
    <!-- <script src="{{ asset('ayra/core/js/wizard-2.js')}}" type="text/javascript"></script> -->
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js" type="text/javascript"></script> -->
    <script src="https://transloadit.edgly.net/releases/uppy/v1.14.1/uppy.min.js"></script>
    <script>
        var uppy = Uppy.Core()
            .use(Uppy.Dashboard, {
                inline: true,
                target: '#drag-drop-area'
            })
            .use(Uppy.Tus, {
                endpoint: 'https://master.ftus.io/files/'
            })


        uppy.on('complete', (result) => {
            console.log('Upload complete! We’ve uploaded these files:', result.successful)
        })
    </script>
    <script src="{{ asset('ayra/core/js/dashboard.js')}}" type="text/javascript"></script>
    <script src="{{ asset('ayra/core/js/ayra.js')}}" type="text/javascript"></script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>


    <script>
        var menus = {
            "oneThemeLocationNoMenus": "",
            "moveUp": "Move up",
            "moveDown": "Mover down",
            "moveToTop": "Move top",
            "moveUnder": "Move under of %s",
            "moveOutFrom": "Out from under  %s",
            "under": "Under %s",
            "outFrom": "Out from %s",
            "menuFocus": "%1$s. Element menu %2$d of %3$d.",
            "subMenuFocus": "%1$s. Menu of subelement %2$d of %3$s."
        };
        var arraydata = [];
        var addcustommenur = '{{ route("haddcustommenu") }}';
        var updateitemr = '{{ route("hupdateitem")}}';
        var generatemenucontrolr = '{{ route("hgeneratemenucontrol") }}';
        var deleteitemmenur = '{{ route("hdeleteitemmenu") }}';
        var deletemenugr = '{{ route("hdeletemenug") }}';
        var createnewmenur = '{{ route("hcreatenewmenu") }}';
        var csrftoken = "{{ csrf_token() }}";
        var menuwr = "{{ url()->current() }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrftoken
            }
        });
    </script>
    <script type="text/javascript" src="{{asset('ayra/public/vendor/harimayco-menu/scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('ayra/public/vendor/harimayco-menu/scripts2.js')}}"></script>
    <script type="text/javascript" src="{{asset('ayra/public/vendor/harimayco-menu/menu.js')}}"></script>

    <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/19.0.0/balloon/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('summary_ckeditor', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('summary_ckeditor_1', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    <!--end::Page Scripts -->
    <script>
        function saveArt() {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }


            return false;

        }

        // BalloonEditor
        //     .create( document.querySelector( '#editor' ) )
        //     .catch( error => {
        //         console.error( error );
        //     } );
    </script>

</body>
<!-- end::Body -->

</html>