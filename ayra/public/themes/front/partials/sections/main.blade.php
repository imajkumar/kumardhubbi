<?php

use App\Helpers\AyraHelp;

$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 1);
$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;
$map_img = asset('uploads/img/logo') . "/" . optional($static_block_data)->map_img;

use Illuminate\Support\Str;
?>
<!-- BANNER CONTENT -->
<div class="container text-center">
	<div class="row">
		<div class="col-lg-10 col-xl-9 mx-auto animate fadeInUp" data-animate="fadeInUp" data-duration="1s">
			<h1 class="text-white text-center position-relative pb-sm-5 mb-3 mt-5 mt-md-0">
				<span class="ml-lg-5 pl-xl-5 d-block">{{optional($static_block_data)->parent_title}}</span>
				{{optional($static_block_data)->parent_sub_title}}
				<span>{{optional($static_block_data)->parent_sub_sub_title}}</span>
			</h1>
			<p class="mb-4 text-white">{{optional($static_block_data)->block_title_content}}</p>
			<a href="{{route('siteProperties')}}" class="blue-btn">Search Properties</a>
		</div>
	</div>
</div>
</section>


<!-- WELCOME SECTION -->
<section class="welcome-bg">
	<div class="container-fluid p-0">
		<div class="row justify-content-end align-items-center no-gutters">
			<div class="col-10 col-md-6 col-xl-5 mx-auto mr-md-0 animate fadeInUp" data-animate="fadeInUp" data-delay="0.3s" data-duration="1s">
				<h2><span class="d-block mb-2">Welcome To</span> {{optional($web_arr_)->name}}</h2>
				<p class="my-4">

					{!! optional($static_block_data)->static_content !!}
				</p>
				<br>

				<a href="{{route('siteAbout')}}" class="blue-btn">Know More</a>
			</div>
			<div class="col-md-5 ml-xl-auto">



				<a data-fancybox="" href="https://www.youtube.com/embed/g39isQ3F3-c">
					<img class="card-img-top img-fluid" src="{{$wecomeIMG}}">
				</a>

			</div>
		</div>
	</div>
</section>

<?php
$feature_arr = AyraHelp::MyFeaturePropHOSTHOME($web_arr_->vid);

?>
<!-- FEATURED PROPERTIES -->
<section class="py-5">
	<div class="container-fluid px-0 pt-md-5">
		<div class="row justify-content-end no-gutters">
			<div class="col-lg-11 ml-lg-auto">
				<div class="row align-items-center no-gutters mb-4">
					<div class="col-10 col-md-8 mx-auto ml-md-0">
						<h2 class="text-center text-lg-left">Our Featured Properties</h2>
					</div>
					<div class="col-md-3 text-center">
						<a href="{{route('siteProperties')}}" class="text-blue">View All</a>
					</div>
				</div>
				<div class="row no-gutters">
					<div class="col-12 overflow-hidden">
						<div class="owl-carousel owl-theme" id="featured-slider">

							<?php

							foreach ($feature_arr as $key => $rowData) {
								//$photo = asset('uploads/img/logo') . "/" . $rowData->photo;
							?>
								<div class="item">
									<a href="{{route('FeaturemyproDetails',$rowData->id)}}" class="featured-box">
										<figure class="mb-0">
											<img src="{{$rowData->picture_url}}" alt="{{$rowData->property_title}}">
										</figure>
										<figcaption>
											<h5 class="mb-1">{{$rowData->property_title}}</h5>
											<p class="small">{{$rowData->location_district}}</p>
											<ul class="list-unstyled d-flex align-items-start my-2">
												<li>{{$rowData->mlsId}}</li>

											</ul>
											<p class="mb-2">US {{$rowData->price}}</p>
											<img src="{{ asset('ayra/front/img/arrow.png')}}" alt="">
										</figcaption>
									</a>
								</div>

							<?php
							}
							?>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- MAP -->
<section class="py-5 d-none d-xl-block" style="background: #f7f7f7;">
	<div class="container-fluid">
		<div class="row no-gutters">
			<div class="col-md-10 mx-auto mb-5">
				<h2 class="mb-4">Search Property by Community</h2>

				{!! optional($static_block_data)->sub_static_content !!}
			</div>
		</div>

	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="map-container">
					<img src="{{$map_img}}" alt="map">
					<div class="map-pins">
						<!--Grand Cayman-->
						<?php
						$commArr =	AyraHelp::getCommunitiesByHOSTHOME($web_arr_->vid);
						foreach ($commArr as $key => $rowData) {
							switch ($rowData->id) {
								case 1:
									$comSlug1 = $rowData->slug;
									break;
								case 2:
									$comSlug2 = $rowData->slug;
									break;
								case 3:
									$comSlug3 = $rowData->slug;
									break;
								case 4:
									$comSlug4 = $rowData->slug;
									break;
								case 5:
									$comSlug5 = $rowData->slug;
									break;
								case 6:
									$comSlug6 = $rowData->slug;
									break;
								case 7:
									$comSlug7 = $rowData->slug;
									break;
								case 8:
									$comSlug8 = $rowData->slug;
									break;
								case 9:
									$comSlug9 = $rowData->slug;
									break;
								case 10:
									$comSlug10 = $rowData->slug;
									break;
								case 11:
									$comSlug11 = $rowData->slug;
									break;
								case 12:
									$comSlug12 = $rowData->slug;
									break;
								case 13:
									$comSlug13 = $rowData->slug;
									break;
								case 14:
									$comSlug14 = $rowData->slug;
									break;
								case 15:
									$comSlug15 = $rowData->slug;
									break;
							
							}
						}

						?>

						<a id="west-bay" href="{{BASE_PATH.'community-details-view/'.$comSlug1}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="{{optional(AyraHelp::getCommunitiesByHOSTHOME_ID($web_arr_->vid,1))->title}}"></a>

						<a id="seven-mile-corridor" href="{{BASE_PATH.'community-details-view/'.$comSlug2}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Seven Mile Corridor"></a>

						<a id="west-bay" href="{{BASE_PATH.'community-details-view/'.$comSlug2}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="{{optional(AyraHelp::getCommunitiesByHOSTHOME_ID($web_arr_->vid,1))->title}}"></a>

						<a id="seven-mile-corridor" href="{{BASE_PATH.'community-details-view/'.$comSlug2}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Seven Mile Corridor"></a>

						<a id="seven-mile-beach" href="{{BASE_PATH.'community-details-view/'.$comSlug3}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Seven Mile Beach"></a>

						<a id="george-town" href="{{BASE_PATH.'community-details-view/'.$comSlug4}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="George Town"></a>

						<a id="south-sound" href="{{BASE_PATH.'community-details-view/'.$comSlug5}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="South Sound"></a>

						<a id="prospect" href="{{BASE_PATH.'community-details-view/'.$comSlug6}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Prospect"></a>
						<a id="spotts" href="{{BASE_PATH.'community-details-view/'.$comSlug7}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Spotts"></a>
						<a id="savannah" href="{{BASE_PATH.'community-details-view/'.$comSlug8}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Savannah"></a>
						<a id="bodden-town" href="{{BASE_PATH.'community-details-view/'.$comSlug9}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Bodden Town"></a>
						<a id="breakers" href="{{BASE_PATH.'community-details-view/'.$comSlug10}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Breakers"></a>
						<a id="east-end" href="{{BASE_PATH.'community-details-view/'.$comSlug11}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="East End"></a>
						<a id="north-side" href="{{BASE_PATH.'community-details-view/'.$comSlug12}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="North Side"></a>
						<a id="rum-point" href="{{BASE_PATH.'community-details-view/'.$comSlug13}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Rum Point"></a>

						<!--Cayman Brac-->
						<!-- <a id="cayman-brac-west" href="{{BASE_PATH.'community-details/18'}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Cayman Brac West"></a> -->

						<a id="cayman-brac-central" href="{{BASE_PATH.'community-details-view/'.$comSlug15}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Cayman Brac"></a>

						<!-- <a id="cayman-brac-east" href="{{BASE_PATH.'community-details/16'}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Cayman Brac East"></a> -->

						<!--Little Cayman-->
						<a id="little-cayman-west" href="{{BASE_PATH.'community-details-view/'.$comSlug15}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Little Cayman"></a>

						<!-- <a id="little-cayman-east" href="{{BASE_PATH.'community-details/14'}}" class="map-pin animate fadeInDown" data-animate="fadeInDown" data-duration="1.5s" data-toggle="tooltip" data-placement="top" title="Little Cayman East"></a> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<?php
$data_pros = AyraHelp::getProprtyCat();
?>
<!-- GRID -->
<section class="pt-5 pb-5 pt-xl-0">
	<div class="container-fluid p-0">
		<div class="row no-gutters">

			<?php
			$i = 0;
			foreach ($data_pros as $key => $rowData) {
				$i++;
				$photoA = asset('ayra/front/img') . "/grid" . $i . ".jpg";

			?>


				<div class="col-md-6 col-lg-4">
					<a href="{{route('propCatList',$rowData->property_type)}}" class="d-block grid-box m-1">
						<img src="{{$photoA}}" alt="">
						<h3 class="text-white">
							<span>{{$rowData->property_type}}</span>
						</h3>
					</a>
				</div>
			<?php

			}
			?>









		</div>
	</div>
</section>



<!-- INSTAGRAM FEED -->
<section class="insta-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-7 col-lg-6 col-xl-5 mx-auto mr-lg-0">
				<div class="insta-box text-center">
					<img src="{{ asset('ayra/front/img/insta.png')}}" alt="" class="mt-3">
					<h2 class="mt-2 mb-3">@diamondpropertyky</h2>
					<a href="https://www.instagram.com/diamondpropertiesky" target="_blank" class="blue-btn ghost-btn">Follow Now</a>

					<ul class="list-unstyled d-flex flex-wrap justify-content-between mt-4">
						<li>
							<img src="{{ asset('ayra/front/img/insta1.jpg')}}">
						</li>
						<li>
							<img src="{{ asset('ayra/front/img/insta2.jpg')}}">
						</li>
						<li>
							<img src="{{ asset('ayra/front/img/insta3.jpg')}}">
						</li>

						<li>
							<img src="{{ asset('ayra/front/img/insta4.jpg')}}">
						<li>
							<img src="{{ asset('ayra/front/img/insta5.jpg')}}">
						</li>
						<li>
							<img src="{{ asset('ayra/front/img/insta6.jpg')}}">
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- BLOGS -->
<?php
$data_arr = AyraHelp::getmyNewsByHOST_HOME($web_arr_->vid);
foreach ($data_arr as $key => $rowData) {
}
?>
<section class="py-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="text-center mb-4">Blogs <br>Here’s what’s happening</h2>
				<div class="owl-carousel owl-theme" id="blogs">
					<?php
					foreach ($data_arr as $key => $rowData) {
						$created_at = date('F j ,Y', strtotime($rowData->start_date));
					?>
						<a href="{{route('blogDetails',$rowData->id)}}">
							<div class="item">
								<div class="blog-box">
									<p class="text-blue"> {{$created_at}}</p>
									<h5 class="font-weight-bold my-2">{{optional($rowData)->title}}</h5>
									<h6>{!! Str::limit(optional($rowData)->info, 150,' (...)') !!}</h6>
								</div>
							</div>
						</a>

					<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>
</section>


<!-- TESTIMONIALS -->
<?php
$data_arr = AyraHelp::getTestimonialkByHOST_HOME($web_arr_->vid);



?>
<section class="testimonial-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-lg-6 mx-auto mr-lg-0">
				<h2 class="mb-3">Testimonials <br>Glowing Words from Our Clients</h2>

				<div class="owl-carousel owl-theme" id="testimonials">
					<?php
					foreach ($data_arr as $key => $rowData) {
						$photo = asset('uploads/img/logo') . "/" . $rowData->photo;

					?>
						<div class="item">
							<div class="testimonial-box">
								<h3>{{$rowData->title}}</h3>
								<h6>
									{!! $rowData->info_data !!}

								</h6>

								<div class="d-md-flex align-items-center">
									<img src="{{$photo}}" alt="" class="client-img">
									<h5>{{$rowData->created_by}}</h5>
								</div>
							</div>
						</div>

					<?php

					}

					?>



				</div>
			</div>
		</div>
	</div>
</section>


<!-- CONTACT FORM -->
<section class="py-5">
	<div class="container">
		<div class="row">
			@if(Session::has('success'))
			<div class="alert alert-success">
				{{ Session::get('success') }}
			</div>
			@endif
			<div class="col-lg-8 mx-auto">
				<h2 class="text-center mb-3"></h2>
				<div class="z-index-0 position-relative bg-white agent-info p-4">
					<h3 id="mess_info" style="color:#008080"></h3>
					<form id="kt_form_1" id="contact_formAjaxSubmit" method="post" action="{{route('saveContactData')}}" class="contact-form">
						@csrf
						<div class="row">
							<div class="form-group col-md-6">
								<input type="text" required id="first_name" name="first_name" class="form-control" placeholder="First Name*">
								<label>First Name*</label>
							</div>
							<div class="form-group col-md-6">
								<input type="text" required id="last_name" name="last_name" class="form-control" placeholder="Last Name*">
								<label>Last Name*</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								<input type="email" required id="email" name="email" class="form-control" placeholder="Email Address*">
								<label>Email Address*</label>
							</div>
							<div class="form-group col-md-6">
								<input type="text" required id="phone" name="phone" class="form-control" placeholder="Phone No.*">
								<label>Phone No.*</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<textarea class="form-control" id="message" required name="message" placeholder="Your Message*" rows="5"></textarea>
								<label>Your Message*</label>
							</div>
							<div class="col-12">
								<div class="form-check">
									<input required type="checkbox" class="form-check-input" id="exampleCheck1">
									<span class="form-check-label" for="exampleCheck1">
										I accept Privacy Policy & Terms & Conditions
									</span>
								</div>
							</div>
							<div class="col-12 text-center text-md-right mt-3">

								<button type="button" class="btn send-btn" id="btnContactSave">
									Send Message <i class="fa fa-caret-right ml-3"></i>
								</button>

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>