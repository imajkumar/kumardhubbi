 <!-- FOOTER -->
 <?php 
$WEBSETTING = config('web_settings');

$public_menu=$WEBSETTING['public_menu_footer'];
$web_contact=$WEBSETTING['web_contact'];

$LOGO1 = optional($WEBSETTING['web_general'])->web_logo == "" ? 'default.png' : $WEBSETTING['web_general']->web_logo;
$LOGO = IMAGE_PATH_SETTINGS . $LOGO1;


?>

 <footer>
    	<div class="container">
    		<div class="row mb-4 mb-md-5">
    			<div class="text-center text-md-left col-lg-3 mb-5 mb-lg-0">
    				<img src="{{$LOGO}}" alt="" class="logo">
    			</div>

    			<div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                    <h4 class="mb-4">Main Links</h4>
                    @if($public_menu)
						<ul class="list-unstyled">
							@foreach($public_menu as $menu)
							<li>
								<a href="{{  BASE_PATH.$menu['link'] }}"   class="" title="">{{ $menu['label'] }}</a>
								@if( $menu['child'] )
								<ul class="sub-menu">
									@foreach( $menu['child'] as $child )
										<li class=""><a href="{{  BASE_PATH.$child['link'] }}" title="">{{ $child['label'] }}</a></li>
									@endforeach
								</ul><!-- /.sub-menu -->
								@endif
							</li>
							@endforeach
                        @endif
                        

    				
    			</div> 


    			<div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
    				<h4 class="mb-4">Contact Us</h4>

    				<ul class="list-unstyled mb-">
    					<li><a href="tel:{{optional($web_contact)->phone}}">Cell: {{optional($web_contact)->phone}}</a></li>
						<li><a href="tel:{{optional($web_contact)->office_phone}}">Office: {{optional($web_contact)->office_phone}}</a></li>
						<li><a href="#">Fax: {{optional($web_contact)->fax_phone}}</a></li>
    				</ul>

    				<h4 class="my-4">Address</h4>

    				<ul class="list-unstyled">
    					<li>
                        {{optional($web_contact)->site_address}}

                            </li>
						<li><a href="mailto:{{optional($web_contact)->email}}">Email:  {{optional($web_contact)->email}} </a></li>
    				</ul>
    			</div>    			

    			<div class="col-md-6 col-lg-2 text-center text-lg-left mx-auto">
    				<img src="{{ asset('ayra/front/img/cireba.png')}}" alt="">
    				<ul class="list-unstyled social-links mt-3">
	                	<li class="d-inline-block"><a href="{{optional($web_contact)->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
	                	<li class="d-inline-block"><a href="{{optional($web_contact)->linkedin}}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<li class="d-inline-block"><a href="{{optional($web_contact)->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<li class="d-inline-block"><a href="{{optional($web_contact)->youtube}}" target="_blank"><i class="fa fa-youtube"></i></a></li>
	                	
	                </ul>
    			</div>
    		</div>

    		<div class="row py-3 border-top">
    			<div class="col-md-8 col-lg-7">
    				<p class="text-center text-md-left mb-2 mb-md-0">
					Copyright © {{date('Y')}} Eyecay LDX Connect. All Rights Reserved		
				</p>
				</div> 
				<div class="col-md-4 col-lg-5">
					<ul class="list-unstyled text-center text-lg-right">
    					<li class="d-inline-block mb-0"><a href="{{BASE_PATH.'privacy-policy'}}">Privacy Policy</a></li>
    					<li class="d-inline-block mb-0 ml-3"><a href="{{BASE_PATH.'sitemap'}}">Site map</a></li>
    				</ul>
				</div>                                                                           
    		</div>
    	</div>
    </footer>
