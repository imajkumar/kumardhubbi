<!-- BANNER CONTENT -->

<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-10 col-xl-9 mx-auto">
					<h1 class="text-white text-center position-relative pb-sm-5 mb-3">
						<span class="ml-lg-5 pl-xl-5 d-block">TH55E WORLD’S BEST DIAMOND</span> 
						Properties 
						<span>Committed Excellence</span>
					</h1>
			        <p class="mb-4 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			        <a href="" class="blue-btn">Read More</a>
			    </div>
			</div>
		</div>
       </section>


    <!-- WELCOME SECTION -->
    <section class="welcome-bg">
    	<div class="container-fluid p-0">
    		<div class="row justify-content-end align-items-center no-gutters">
    			<div class="col-10 col-md-6 col-xl-5 mx-auto mr-md-0">
    				<h2><span class="d-block mb-2">Welcome To</span> Diamond Properties</h2>
    				<p class="my-4">When it comes to investing in today’s fast-paced real estate market, Paula McCartney knows you need an experienced, fully Licensed agency like Diamond Properties.</p>
    				<a href="" class="blue-btn">Know More</a>
    			</div>
    			<div class="col-md-5 ml-xl-auto">
    				<a href="" class="d-block">
    					<img src="{{ asset('ayra/front/img/welcome-right.png')}}">
    				</a>
    			</div>
    		</div>
    	</div>
    </section>


    <!-- FEATURED PROPERTIES -->
    <section class="py-5">
    	<div class="container-fluid px-0 pt-md-5">
    		<div class="row justify-content-end no-gutters">
    			<div class="col-lg-11 ml-lg-auto">
    				<div class="row align-items-center no-gutters mb-4">
		    			<div class="col-10 col-md-8 mx-auto ml-md-0">
		    				<h2 class="text-center text-md-left">Our Featured Properties</h2>
		    			</div>
		    			<div class="col-md-3 text-center text-lg-right">
		    				<a href="" class="text-blue">View All</a>
		    			</div>
		    		</div>
		    		<div class="row no-gutters">
		    			<div class="col-12 overflow-hidden">
		    				<div class="owl-carousel owl-theme" id="featured-slider">
		                        <div class="item">
		                            <a href="" class="featured-box">
		                            	<figure class="mb-0">
			                            	<img src="{{ asset('ayra/front/img/featured1.png')}}" alt="Air Conditioner">
			                            </figure>
		                                <figcaption>
		                                	<h4 class="mb-1">The Cayman Islander Hotel</h4>
		                                	<p class="small">West Bay</p>
		                                	<ul class="list-unstyled d-flex align-items-start my-2">
		                                		<li>495</li>
		                                		<li>280</li>
		                                	</ul>
		                                	<p class="mb-2">US $1,000,00</p>
		                                	<img src="{{ asset('ayra/front/img/arrow.png')}}" alt="">
		                                </figcaption>
		                            </a>
		                        </div>
		                    </div>
		    			</div>
		    		</div>
		    	</div>
    		</div>
    	</div>
    </section>


    <!-- MAP -->
    <section>
    	<div class="container">
    		<div class="row">
    			<div class="col-md-10 col-xl-8 mb-5">
    				<h2 class="mb-4">Search Property by Community</h2>
    				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    			</div>

    			<div class="col-lg-11">
    				<img src="{{ asset('ayra/front/img/map.png')}}" alt="map">
    			</div>
    		</div>
    	</div>
    </section>


    <!-- GRID -->
    <section class="py-5">
    	<div class="container-fluid p-0">
    		<div class="row no-gutters">
    			<div class="col-md-6 col-lg-4">
    				<a href="" class="d-block grid-box p-1">
    					<img src="{{ asset('ayra/front/img/grid1.jpg')}}" alt="">
    					<h3 class="text-white">
    						<span>Residential</span>
    					</h3>
    				</a>
    			</div>
    			<div class="col-md-6 col-lg-4">
    				<a href="" class="d-block grid-box p-1">
    					<img src="{{ asset('ayra/front/img/grid2.jpg')}}" alt="">
    					<h3 class="text-white">
    						<span>Multi-unit</span>
    					</h3>
    				</a>
    			</div>
    			<div class="col-md-6 col-lg-4">
    				<a href="" class="d-block grid-box p-1">
    					<img src="{{ asset('ayra/front/img/grid3.jpg')}}" alt="">
    					<h3 class="text-white">
    						<span>Business</span>
    					</h3>
    				</a>
    			</div>
    			<div class="col-md-6 col-lg-4">
    				<a href="" class="d-block grid-box p-1">
    					<img src="{{ asset('ayra/front/img/grid4.jpg')}}" alt="">
    					<h3 class="text-white">
    						<span>Commercial</span>
    					</h3>
    				</a>
    			</div>
    			<div class="col-md-6 col-lg-4">
    				<a href="" class="d-block grid-box p-1">
    					<img src="{{ asset('ayra/front/img/grid5.jpg')}}" alt="">
    					<h3 class="text-white">
    						<span>Land</span>
    					</h3>
    				</a>
    			</div>
    			<div class="col-md-6 col-lg-4">
    				<a href="" class="d-block grid-box p-1">
    					<img src="{{ asset('ayra/front/img/grid6.jpg')}}" alt="">
    					<h3 class="text-white">
    						<span>All Properties</span>
    					</h3>
    				</a>
    			</div>
    		</div>	
    	</div>
    </section>



    <!-- INSTAGRAM FEED -->
    <section class="insta-bg">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-7 col-lg-6 col-xl-5 mx-auto mr-lg-0">
    				<div class="insta-box text-center">
    					<img src="img/insta.png" alt="" class="mt-3">
    					<h2 class="mt-2 mb-3">@diamondpropertyky</h2>
    					<a href="" class="blue-btn ghost-btn">Follow Now</a>

    					<ul class="list-unstyled d-flex flex-wrap justify-content-between mt-4">
    						<li>
    							<img src="img/insta1.jpg">
    						</li>
    						<li>
    							<img src="img/insta2.jpg">
    						</li>
    						<li>
    							<img src="img/insta3.jpg">
    						</li>

    						<li>
    							<img src="img/insta4.jpg">
    						</li>
    						<li>
    							<img src="img/insta5.jpg">
    						</li>
    						<li>
    							<img src="img/insta6.jpg">
    						</li>
    					</ul>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>


    <!-- BLOGS -->
    <section class="py-5">
    	<div class="container">
    		<div class="row">
    			<div class="col-12">
    				<h2 class="text-center mb-4">Blogs <br>Here’s what’s happening</h2>
    				<div class="owl-carousel owl-theme" id="blogs">
                        <div class="item">
                            <div class="blog-box">
                            	<p class="text-blue">January 31, 2020</p>
                            	<h5 class="font-weight-bold my-2">Benefits of Buying property or land in the Sister Islands</h5>
                            	<h6>Grand Cayman is the largest of the three sister islands making up the Cayman Islands, and while it is the largest and most populated of the three, home to over 52,000 people, it is also considered the most expensive.</h6>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-box">
                            	<p class="text-blue">January 31, 2020</p>
                            	<h5 class="font-weight-bold my-2">Benefits of Buying property or land in the Sister Islands</h5>
                            	<h6>Grand Cayman is the largest of the three sister islands making up the Cayman Islands, and while it is the largest and most populated of the three, home to over 52,000 people, it is also considered the most expensive.</h6>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-box">
                            	<p class="text-blue">January 31, 2020</p>
                            	<h5 class="font-weight-bold my-2">Benefits of Buying property or land in the Sister Islands</h5>
                            	<h6>Grand Cayman is the largest of the three sister islands making up the Cayman Islands, and while it is the largest and most populated of the three, home to over 52,000 people, it is also considered the most expensive.</h6>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-box">
                            	<p class="text-blue">January 31, 2020</p>
                            	<h5 class="font-weight-bold my-2">Benefits of Buying property or land in the Sister Islands</h5>
                            	<h6>Grand Cayman is the largest of the three sister islands making up the Cayman Islands, and while it is the largest and most populated of the three, home to over 52,000 people, it is also considered the most expensive.</h6>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-box">
                            	<p class="text-blue">January 31, 2020</p>
                            	<h5 class="font-weight-bold my-2">Benefits of Buying property or land in the Sister Islands</h5>
                            	<h6>Grand Cayman is the largest of the three sister islands making up the Cayman Islands, and while it is the largest and most populated of the three, home to over 52,000 people, it is also considered the most expensive.</h6>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-box">
                            	<p class="text-blue">January 31, 2020</p>
                            	<h5 class="font-weight-bold my-2">Benefits of Buying property or land in the Sister Islands</h5>
                            	<h6>Grand Cayman is the largest of the three sister islands making up the Cayman Islands, and while it is the largest and most populated of the three, home to over 52,000 people, it is also considered the most expensive.</h6>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-box">
                            	<p class="text-blue">January 31, 2020</p>
                            	<h5 class="font-weight-bold my-2">Benefits of Buying property or land in the Sister Islands</h5>
                            	<h6>Grand Cayman is the largest of the three sister islands making up the Cayman Islands, and while it is the largest and most populated of the three, home to over 52,000 people, it is also considered the most expensive.</h6>
                            </div>
                        </div>
                    </div>
    			</div>
    		</div>
    	</div>
    </section>


    <!-- TESTIMONIALS -->
    <section class="testimonial-bg">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-6 mx-auto mr-lg-0">
    				<h2 class="mb-3">Testimonials <br>Glowing Words from Our Clients</h2>

    				<div class="owl-carousel owl-theme" id="testimonials">
                        <div class="item">
                            <div class="testimonial-box">
                            	<h3>Great Experience</h3>
                            	<h6>Great experience from start to finish with Diamond Properties. Paula was utterly focused on our house sale. From finding us a buyer to closing, she held our hand from start to finish.</h6>

                            	<div class="d-md-flex align-items-center">
                            		<img src="img/client.png" alt="" class="client-img">
                            		<h5>Catherine & Adrian Porter</h5>
                            	</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                            	<h3>Great Experience</h3>
                            	<h6>Great experience from start to finish with Diamond Properties. Paula was utterly focused on our house sale. From finding us a buyer to closing, she held our hand from start to finish.</h6>

                            	<div class="d-md-flex align-items-center">
                            		<img src="img/client.png" alt="" class="client-img">
                            		<h5>Catherine & Adrian Porter</h5>
                            	</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                            	<h3>Great Experience</h3>
                            	<h6>Great experience from start to finish with Diamond Properties. Paula was utterly focused on our house sale. From finding us a buyer to closing, she held our hand from start to finish.</h6>

                            	<div class="d-md-flex align-items-center">
                            		<img src="img/client.png" alt="" class="client-img">
                            		<h5>Catherine & Adrian Porter</h5>
                            	</div>
                            </div>
                        </div>
                    </div>
    			</div>
    		</div>
    	</div>
    </section>


    <!-- CONTACT FORM -->
    <section>
    	<div class="container">
    		<div class="row">
    			<div class="col-xl-10 mx-auto">
    				<h2 class="text-center mb-3">Let’s talk...</h2>

    				<form method="" action="" class="contact-form">
    					<div class="row">
	    					<div class="form-group col-md-6">
	    						<input type="text" name="" class="form-control" placeholder="First Name*">
	    						<label>First Name*</label>
	    					</div>	    					
	    					<div class="form-group col-md-6">
	    						<input type="text" name="" class="form-control" placeholder="Last Name*">
	    						<label>Last Name*</label>
	    					</div>
	    				</div>
	    				<div class="row">
	    					<div class="form-group col-md-6">
	    						<input type="email" name="" class="form-control" placeholder="Email Address*">
	    						<label>Email Address*</label>
	    					</div>	    					
	    					<div class="form-group col-md-6">
	    						<input type="text" name="" class="form-control" placeholder="Phone No.*">
	    						<label>Phone No.*</label>
	    					</div>
	    				</div>
	    				<div class="row">
	    					<div class="form-group col-md-12">
	    						<textarea class="form-control" placeholder="Your Message*" rows="5"></textarea>
	    						<label>Your Message*</label>
	    					</div>
	    					<div class="col-12 text-center text-md-right">
	    						<button type="submit" class="btn send-btn">Send Message <i class="fa fa-caret-right ml-3"></i></button>
	    					</div>	
	    				</div>
    				</form>
    			</div>
    		</div>
    	</div>
    </section>
