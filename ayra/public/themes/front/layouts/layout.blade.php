<?php

$WEBSETTING = config('web_settings');

$web_settings = $WEBSETTING['web_settings'];

$url_segment = \Request::segment(1);
$myclass = '';

switch ($url_segment) {
    case 'about':
        $myclass = "inner-banner about-bg position-relative";

        break;
    case 'contact':
        $myclass = "inner-banner contact-bg position-relative";
        break;
    case 'selling':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'buying':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'rentals':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'more-mls-listing':
        $myclass = "inner-banner property-bg position-relative";
        break;

    case 'properties':
        $myclass = "inner-banner position-relative";
        break;
    case 'blog-details':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'blog-news':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'property-cat-detail':
        $myclass = "inner-banner position-relative";
        break;
    case 'property-list':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'our-team':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'community':
        $myclass = "inner-banner property-bg position-relative";
        break;

    case 'community-details':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'team-details':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'properties-view':
        $myclass = "inner-banner position-relative";
        break;
    case 'property-cat-details':
        $myclass = "inner-banner position-relative";
        break;
    case 'team':
        $myclass = "inner-banner property-bg position-relative";
        break;
    case 'feature-properties-view':
        $myclass = "inner-banner property-bg position-relative";
        break;
        case 'privacy-policy':
            $myclass = "inner-banner position-relative";
            break;

       


    default:
        $myclass = "banner position-relative";
        break;
}



?>

<!doctype html>
<html lang="en">

<head>
    <title> {{optional($web_settings)->name}} | {{URL_HOME}}</title>
    <meta charset="utf-8">
    <meta name="BASE_URL" content="{{ url('/') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('ayra/front/css/bootstrap.min.css')}}">
    <link href="{{ asset('ayra/front/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('ayra/front/css/owl.theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ayra/front/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ayra/front/css/style.css')}}">
    <link rel='stylesheet' id='font-awesome-css' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.8.12' type='text/css' media='all' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="shortcut icon" type="image/x-icon" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="144x144" href=" https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="57x57" href="https://www.diamondproperties.ky/assets/images/favicon.ico" />


</head>

<body>
    <section class="{{$myclass}}">

        @partial('header')

        @content()


        @partial('footer')

        <script src="{{ asset('ayra/front/js/jquery.min.js')}}"></script>
        <script src="{{ asset('ayra/front/js/popper.min.js')}}"></script>
        <script src="{{ asset('ayra/front/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('ayra/front/js/owl.carousel.js')}}"></script>
        <script src="{{ asset('ayra/front/js/scrolla.jquery.min.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

        <script>
            $('.animate').scrolla({
                mobile: false,
                once: false
            });
            $('.animate').scrolla({
                once: true
            });
        </script>


        <!-- FILTER POPUP JS -->
        <script>
            $(document).ready(function() {
                
                $('#btnCurrview').click(function() {
                    var selectedText = $("#currSelect option:selected").val();
                    var amt = parseInt($('#currAmount').val());
                    if (selectedText == 1) {

                        amtv = amt / 1.22;
                        $('#txtCurrView').show();
                        $('#txtCurrView').val("Your conversion amount is CI$ " + amtv.toFixed(2));

                    } else {

                        amtv = amt * 1.22;
                        $('#txtCurrView').show();
                        $('#txtCurrView').val("Your conversion amount is US$ " + amtv);
                    }


                });

                $("#adv-btn").click(function() {
                    $("#advanced").toggle("slide");
                    var text = $("#symbol").html();
                    if (text == "+") {
                        text = "-";
                    } else {
                        text = "+";
                    }
                    document.getElementById("symbol").innerHTML = text;
                });
            });
        </script>

        <!-- TEAM DETAILS -->
        <script>
            $(document).ready(function() {
                $(".more-text").css('display','none');

                $("#read-more").click(function() {
                    $("#more-text").slideToggle("slow");

                    var text = $("#read-more").html();
                    if (text == "Read More") {
                        text = "Read Less";
                        $(".agent-text").css('display','none');
                    } 
                    else {
                        text = "Read More";
                        $(".agent-text").css('display','block');
                    }                    
                    document.getElementById("read-more").innerHTML = text;
                });
            });
        </script>

        <script>
            $(document).ready(function() {
                $("#featured-slider").owlCarousel({
                    margin: 15,
                    nav: false,
                    dots: false,
                    smartSpeed: 1500,
                    autoplay: false,
                    autoplayTimeout: 4000,
                    loop: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        768: {
                            items: 2
                        },
                        1024: {
                            items: 3
                        },
                        1600: {
                            items: 4
                        }
                    }
                });

                $("#blogs").owlCarousel({
                    margin: 0,
                    nav: false,
                    smartSpeed: 1500,
                    autoplay: false,
                    loop: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        768: {
                            items: 2
                        },
                        992: {
                            items: 3
                        }
                    }
                });

                $("#testimonials").owlCarousel({
                    margin: 0,
                    nav: false,
                    smartSpeed: 1500,
                    autoplay: false,
                    loop: true,
                    responsive: {
                        0: {
                            items: 1
                        }
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <script type="text/javascript">
            BASE_URL = $('meta[name="BASE_URL"]').attr('content');
        </script>
        <script>
            //ajacontct
            $(document).ready(function() {

                //btnsavePropertyLeadData 

                //btnSendToFriend
                $('#btnSendToFriend').click(function() {
                
                    
                   var copyFlag=0;
                    if($('#sendcopy').prop("checked") == true){
                        console.log("Checkbox is checked.");
                        copyFlag=1;
                     }else{
                        console.log("Checkbox is nott.");
                        copyFlag=0;
                     }

                   // ajax 
                   $('#mess_info_data').html('<h5>Please Wait...</h5>');
                   var formData = {
                        'name': $('#name').val(),
                        'email': $('#RepEmail').val(),
                        'myname': $('#myname').val(),
                        'myemail': $('#myemail').val(),
                        'propID': $('#propID').val(),
                        'msg': $('#msg').val(),
                        'copyFlag':copyFlag,
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    };
                    $.ajax({
                        url: BASE_URL + '/sendToFrined',
                        type: 'POST',
                        data: formData,
                        success: function(res) {
                            if (res == 1) {
                               
                                $('#mess_info_data').html('<h5 style="color:#16426b">Successfully sent</h5>');
                            }
                        }
                    });

                  
                });
                //btnSendToFriend


                $('#btnsavePropertyLeadData').click(function() {

                    // ajax 
                    var formData = {
                        'first_name': $('#first_name').val(),
                        'last_name': $('#last_name').val(),
                        'email': $('#email').val(),
                        'phone': $('#phone').val(),
                        'message': $('#message').val(),
                        'frmType': $('#frmType').val(),

                        '_token': $('meta[name="csrf-token"]').attr('content')
                    };
                    $.ajax({
                        url: BASE_URL + '/savePropertyLeadData',
                        type: 'POST',
                        data: formData,
                        success: function(res) {
                            if (res == 1) {
                                $('#mess_info').html('Thanks for contacting');
                            }
                        }
                    });

                });
                //btnsavePropertyLeadData

                
                $('#btnContactSave').click(function() {

                    // ajax 
                    var formData = {
                        'first_name': $('#first_name').val(),
                        'last_name': $('#last_name').val(),
                        'email': $('#email').val(),
                        'phone': $('#phone').val(),
                        'message': $('#message').val(),

                        '_token': $('meta[name="csrf-token"]').attr('content')
                    };
                    $.ajax({
                        url: BASE_URL + '/saveContactData',
                        type: 'POST',
                        data: formData,
                        success: function(res) {
                            if (res == 1) {
                                $('#mess_info').html('Thanks for contacting');
                            }
                        }
                    });
                    // ajax 


                });


            });


            //ajcode 
            $('#term').focusout(function(){
                calcPaym();
            });


            function chkprice(e, ele) {
            

    var _dom = 0;
    _dom = document.all ? 3 : (document.getElementById ? 1 : (document.layers ? 2 : 0));
    if (document.all)
        e = window.event; // for IE
    var ch = '';
    var KeyID = '';
    if (_dom == 2) { // for NN4
        //alert(e.which);
        if (e.which > 0)
            ch = '(' + String.fromCharCode(e.which) + ')';
        KeyID = e.which;
    } else {
        if (_dom == 3) { // for IE
            KeyID = (window.event) ? event.keyCode : e.which;
        } else { // for Mozilla
            if (e.charCode > 0)
                ch = '(' + String.fromCharCode(e.charCode) + ')';
            KeyID = e.charCode;
        }
    }

    if (KeyID == 46) {
        var eleval = document.getElementById(ele).value;
        var seg = eleval.split(".");

        if (seg.length >= 2) {
            return false;
        } else {
            return true;
        }
        return true;
    } else if ((KeyID >= 65 && KeyID <= 90) || (KeyID >= 97 && KeyID <= 122) || (KeyID >= 33 && KeyID <= 47) || (KeyID >= 58 && KeyID <= 64) || (KeyID >= 91 && KeyID <= 96) || (KeyID >= 123 && KeyID <= 126)) {
        return false;
    }


    return true;
}

//Mortgage Calculator
function calcPaym() {
   
    var e = document.mortcalcfrm.tprinc.value.replace(/[^0-9\.]/g, "");
   
    tprinc = round2d(parseFloat(e)), downp = .01 * parseFloat(document.mortcalcfrm.downp.value), isNaN(downp) && (downp = 0), downr = ceil2d(downp * tprinc), princ = tprinc - downr, document.getElementById("princ").innerHTML = princ.formatMoney(), document.getElementById("downr").innerHTML = downr.formatMoney(), irate = .01 * parseFloat(document.mortcalcfrm.percent.value), years = parseFloat(document.mortcalcfrm.years.value), term = parseFloat(document.mortcalcfrm.term.value), term > years && (term = years), 0 == term || 0 == years || 1e-4 > irate || 1 > princ || (v = 1 / (mi = 1 + irate / 12), paym = ceil2d(princ * (mi - 1) / (1 - Math.pow(mi, -12 * years))), 0 > (osp = (princ - v * paym * (1 - Math.pow(v, 12 * term)) / (1 - v)) / Math.pow(v, 12 * term)) && (osp = 0), document.getElementById("paym").innerHTML = roundToCents(100 * Math.round(paym)).formatMoney(), document.getElementById("owed").innerHTML = roundToCents(Math.round(osp)).formatMoney()), "NaN.00" == document.getElementById("princ").innerHTML && (document.getElementById("princ").innerHTML = "--", document.getElementById("downr").innerHTML = "--"), ("NaN.00" == document.getElementById("paym").innerHTML || "NaN.00" == document.getElementById("owed").innerHTML) && (document.getElementById("paym").innerHTML = "--", document.getElementById("owed").innerHTML = "--")
}
function round2d(e) {
    return.01 * Math.round(100 * e)
}
function floor2d(e) {
    return.01 * Math.floor(100 * e)
}
function ceil2d(e) {
    return.01 * Math.ceil(100 * e)
}
function roundToCents(e) {
    return num = 100 * e, Math.round(e) / 100
}
function fmt2d(e, n) {
    var t, r, o;
    if (o = (t = "" + floor2d(e)).length, -1 == (r = t.indexOf(".")) ? t += ".00" : 3 > o - r && (t += ".00".substring(o - r, 3)), o = t.length, 0 != n && n != o)
        if (o > n)
            for (t = "*", o = 1; n > o; o++)
                t += "*";
        else
            for (; n > o; o++)
                t = " " + t;
    workString = t.toString(), x = workString.split("."), x1 = x[0], x2 = x.length > 1 ? "." + x[1] : "";
    for (var a = /(\d+)(\d{3})/; a.test(x1); )
        x1 = x1.replace(a, "$1,$2");
    return console.log(x1 + " and " + x2), roundToCents(x1 + x2)
}
var irate, mi, cmi, years, tprinc, princ, paym, cpaym, osp, v, downp, downr;
$(document).ready(function () {
    $("#mortcalcfrm").keyup(function () {
        calcPaym()
    })
}), Number.prototype.formatMoney = function (e, n, t) {
    var r = this, o = (e = isNaN(e = Math.abs(e)) ? 2 : e, n = void 0 == n ? "." : n, t = void 0 == t ? "," : t, 0 > r ? "-" : ""), a = parseInt(r = Math.abs(+r || 0).toFixed(e)) + "", d = (d = a.length) > 3 ? d % 3 : 0;
    return "$ "+o + (d ? a.substr(0, d) + t : "") + a.substr(d).replace(/(\d{3})(?=\d)/g, "$1" + t) + (e ? n + Math.abs(r - a).toFixed(e).slice(2) : "")
};


            //ajcode 

            //ajacontct

            function myMap() {
                var myLatlng = new google.maps.LatLng(19.3032951, -81.3825279);
                var mapOptions = {
                    zoom: 13,
                    center: myLatlng,
                    mapTypeControl: false,
                    streetViewControl: false,
                    styles: [{
                            "featureType": "all",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                "weight": "2.00"
                            }]
                        },
                        {
                            "featureType": "all",
                            "elementType": "geometry.stroke",
                            "stylers": [{
                                "color": "#F7F7F7"
                            }]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text",
                            "stylers": [{
                                "visibility": "on"
                            }]
                        },
                        {
                            "featureType": "administrative.country",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                    "visibility": "on"
                                },
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "saturation": "-5"
                                },
                                {
                                    "lightness": "-36"
                                },
                                {
                                    "weight": "0.01"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [{
                                "color": "#ffffff"
                            }]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                "color": "#ededed"
                            }]
                        },
                        {
                            "featureType": "landscape.man_made",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                "color": "#ededed"
                            }]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [{
                                "visibility": "off"
                            }]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                "color": "#f7f7f7"
                            }]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [{
                                "color": "#7b7b7b"
                            }]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.stroke",
                            "stylers": [{
                                "color": "#f7f7f7"
                            }]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [{
                                "visibility": "simplified"
                            }]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [{
                                "color": "#000000"
                            }]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [{
                                "visibility": "off"
                            }]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [{
                                "visibility": "off"
                            }]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [{
                                    "color": "#ffffff"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                "color": "#ffffff"
                            }]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [{
                                "color": "#000000"
                            }]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.stroke",
                            "stylers": [{
                                "color": "#000000"
                            }]
                        }
                    ]
                }
                var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

                var image = BASE_URL + "/ayra/front/img/map-pin.png";
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    icon: image
                });

                // To add the marker to the map, call setMap();
                marker.setMap(map);

            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWnIds1wvO1DnKA65_HFOtNUdaQKE0SbM&callback=myMap"></script>
</body>

</html>