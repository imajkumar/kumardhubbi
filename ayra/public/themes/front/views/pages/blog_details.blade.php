<?php
$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 2);
$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;

$created_at = date('F j ,Y', strtotime($data_arr->start_date));

$photoa_blog = asset('uploads/img/logo') . "/" . $data_arr->photo;

?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-white text-center mt-4">Blog Details </h1>
        </div>
    </div>
</div>
</section>


<!-- BLOG DETAILS -->
<section class="pt-5">
    <div class="container pt-xl-5 px-xl-5">
        <div class="row mb-5">
            <div class="col-lg-7 mb-4 mb-lg-0">
                <div class="blog-details mb-4">
                    <img src="{{$photoa_blog}}" alt="">

                    <p class="text-blue">{{$created_at}}</p>
                    <h5 class="font-weight-bold my-2">{{optional($data_arr)->title}}</h5>

                    <h6>{!! optional($data_arr)->info !!}</h6>
                </div>
            </div>

            <div class="col-md-8 col-lg-5 col-xl-4 ml-xl-auto">
                <h3 class="mb-4">Other Blogs</h3>
                <?php
                $data_arrOther = AyraHelp::getmyOtherNewsByHOST_HOME($web_arr_->vid);


                ?>

                <ul class="list-unstyled m-0 blog-list">
                    <?php
                    foreach ($data_arrOther as $key => $row) {
                        if ($data_arr->id == $row->id) {
                        } else {
                            $photoa = asset('uploads/img/logo') . "/" . $row->photo;
                    ?>
                            <li class="d-flex mb-4 pb-2 border-bottom">
                                <figure class="mr-3">
                                    <img src="{{$photoa}}">
                                </figure>
                                <figcaption>
                                    <a href="javascript:void(0)">Tips about How Exactly To Keep Prepared On the Job</a>
                                </figcaption>
                            </li>

                    <?php
                        }
                    }
                    ?>





                </ul>
            </div>
        </div>
    </div>
</section>



<!-- CONTACT FORM -->
<section class="py-5">
    <div class="container">
        <div class="row">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif
            <div class="col-lg-10 mx-auto">
                <h2 class="text-center mb-3">Let’s talk...</h2>
                <div class="z-index-0 position-relative bg-white agent-info p-4">
                    <h3 id="mess_info" style="color:#008080"></h3>
                    <form id="kt_form_1"  id="contact_formAjaxSubmit"  method="post" action="{{route('saveContactData')}}" class="contact-form">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" required id="first_name" name="first_name" class="form-control" placeholder="First Name*">
                                <label>First Name*</label>
                            </div>                          
                            <div class="form-group col-md-6">
                                <input type="text" required id="last_name"  name="last_name" class="form-control" placeholder="Last Name*">
                                <label>Last Name*</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="email" required id="email" name="email" class="form-control" placeholder="Email Address*">
                                <label>Email Address*</label>
                            </div>                          
                            <div class="form-group col-md-6">
                                <input type="text" required id="phone" name="phone" class="form-control" placeholder="Phone No.*">
                                <label>Phone No.*</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <textarea class="form-control" id="message" required name="message" placeholder="Your Message*" rows="5"></textarea>
                                <label>Your Message*</label>
                            </div>
                            <div class="col-12">
                                <div class="form-check">
                                    <input required  type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <span class="form-check-label" for="exampleCheck1">
                                        I accept Privacy Policy & Terms & Conditions
                                    </span>
                                </div>
                            </div>
                            <div class="col-12 text-center text-md-right mt-3">
                                
                                <button type="button" class="btn send-btn"  id="btnContactSave">
                                Send Message <i class="fa fa-caret-right ml-3"></i>
                                    </button>

                            </div>  
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>