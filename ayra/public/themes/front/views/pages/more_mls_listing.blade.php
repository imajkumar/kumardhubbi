<?php 
$web_arr_=AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data=AyraHelp::getBlockByHOST_HOME($web_arr_->vid,2);
$wecomeIMG=asset('uploads/img/logo')."/".optional($static_block_data)->video_img;

$data_arr = AyraHelp::getSellingData();
?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-12">
					<h1 class="text-white text-center mt-4">More MLS Listing</h1>
                    <form method="get" action="/search" class=" mt-4 search-form">
            {{ csrf_field() }}
                <div class="input-group">
                    <input type="text" name="search_text" class="form-control" placeholder="Search by property type, name, city">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>
			    </div>
			</div>`
		</div>
    </section>


    <!-- CONTACT FORM -->
<section class="pt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mx-auto">

            <div class="row mb-5 align-items-center">
                    <div class="col-md-9">
                        <ul class="nav nav-pills property-category mb-3 mb-md-0" role="tablist">
                            <!-- <li class="nav-item">
                                <a class="nav-link "  href="#">All</a>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link "    href="{{route('siteBuying')}}">Buying</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link"  href="{{route('siteContactView')}}">Selling</a>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link "  href="{{route('siteRentals')}}">Rentals</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active"  href="{{route('siteMoreMlsListing')}}">More MLS listing</a>
                            </li>
                        </ul>
                    </div>

                    <!-- <div class="col-md-2">
                        <a href="javascript:void(0)" class="text-blue" data-toggle="modal" data-target="#filterModal">Filter</a>
                    </div> -->
                    
                    <div class="col-md-3 text-md-right">  
                        <div class="filter-options">                                                        
                            <a href="javascript:void(0)" class="text-blue" data-toggle="modal" data-target="#filterModal">Filter</a>

                            <!-- <div class="dropdown filter-dropdown">
                                  <button type="button" class="btn dropdown-toggle blue-btn" title="Share" data-toggle="dropdown">
                                    Filter
                                  </button>
                                  <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Newest</a>
                                    <a class="dropdown-item" href="#">Price High to Low</a>
                                    <a class="dropdown-item" href="#">Price Low to High</a>
                                    <a class="dropdown-item" href="#">Featured</a>
                                  </div>
                            </div> -->
                            
                            <div class="dropdown dropleft share">
                                  <button type="button" class="btn dropdown-toggle share-btn" title="Share" data-toggle="dropdown">
                                    <i class="fa fa-share-alt"></i>
                                  </button>
                                  <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="dropdown-item" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="dropdown-item" href="#"><i class="fa fa-linkedin"></i></a>
                                  </div>
                            </div>  
                            <a href="#" class="text-blue print" title="Print"><i class="fa fa-print"></i></a>
                        </div>
                    </div>
                </div>

                <div class="tab-content">
                    <div id="all" class="container tab-pane active">
                        <div class="row">
                            <?php


                            foreach ($data_arr as $key => $rowData) {
                                //echo "<pre>";
                                // print_r($rowData);
                            ?>
                               <div class="col-md-6 col-lg-4 mb-4">
                                    <a href="{{route('prop_cat_details',$rowData->prop_id)}}" class="property-info text-center">
                                        <figure class="overflow-hidden position-relative">
                                            <img src="{{optional($rowData)->picture_url}}" alt="{{optional($rowData)->property_title}}">
                                        </figure>
                                        <h5 class="font-weight-bold text-blue">{{optional($rowData)->property_title}}</h5>

                                        <p class="font-weight-bold d-flex justify-content-around my-3 text-black">
                                            <span class="mx-lg-2">{{optional($rowData)->listprice_currency}} {{optional($rowData)->listprice}}</span>
                                            <span class="mx-lg-2">MLS# : {{optional($rowData)->mlsId}}</span>
                                        </p>

                                        <ul class="text-light list-unstyled property-amenities mt-4">
                                            <li title="Sq. Ft.">
                                                <i class="fa fa-map"></i> 3850
                                            </li>
                                            <li title="Bedrooms">
                                                <i class="fa fa-bed"></i> {{optional($rowData)->num_bedrooms}}
                                            </li>
                                            <li title="Bathrooms">
                                                <i class="fa fa-bath"></i> {{optional($rowData)->num_full_bathrooms}}
                                            </li>
                                            <li title="Year Built">
                                                <i class="fa fa-calendar"></i> {{optional($rowData)->year_built}}
                                            </li>
                                        </ul>
                                    </a>
                                </div>

                            <?php
                            }
                            ?>

                        </div>
                    </div>

                    <div id="buying" class="container tab-pane fade">
                        <div class="row">
                            <?php


                            foreach ($data_arr as $key => $rowData) {
                                //echo "<pre>";
                                // print_r($rowData);
                            ?>
                                

                            <?php
                            }
                            ?>


                        </div>
                    </div>

                    <div id="selling" class="container tab-pane fade">
                        <div class="row">


                            <!-- CONTACT FORM -->
                            <section class="py-5">
                                <div class="container">
                                    <div class="row">
                                        @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {{ Session::get('success') }}
                                        </div>
                                        @endif
                                        <div class="col-lg-10 mx-auto"><h3>Valuation Form</h3></div>
                                        <div class="col-lg-10 mx-auto">
                                            <form id="kt_form_1" method="post" action="{{route('saveContactValuableData')}}" class="contact-form">
                                                @csrf
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <input type="text" required name="name" class="form-control" placeholder="Your Name*">
                                                        <label>Your Name*</label>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <input type="email" required name="email" class="form-control" placeholder="Email*">
                                                        <label>Email*</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <input type="text" required name="telephone" class="form-control" placeholder="Telephone*">
                                                        <label>Telephone*</label>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <input type="text" required name="address" class="form-control" placeholder="Street Address.*">
                                                        <label>Street Address.*</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <input type="text" name="block" required class="form-control" placeholder="Block*">
                                                        <label>Block*</label>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <input type="text" name="parcel" required class="form-control" placeholder="Parcel.*">
                                                        <label>Parcel.*</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <input type="text" name="square" required class="form-control" placeholder="*">
                                                        <label>Approximate Square Footage*</label>
                                                    </div>
                                                    <div class="form-group col-md-6">

                                                        <select name="view_data" id="" required class="form-control">
                                                            <option value="water-view">Water View</option>
                                                            <option value="water-front">Water Front</option>
                                                        </select>
                                                        <label>View*</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">

                                                        <select name="bedrooms" id="" required class="form-control">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                        </select>

                                                        <label>Bedrooms*</label>
                                                    </div>
                                                    <div class="form-group col-md-6">

                                                        <select name="bathrooms" id="" required class="form-control">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                        </select>

                                                        <label>Bathrooms*</label>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <textarea class="form-control" name="message" placeholder="Your Message*" rows="5"></textarea>
                                                        <label>Special Features - Additional Comments*</label>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-check">
                                                            <input required type="checkbox" class="form-check-input" id="exampleCheck1">
                                                            <span class="form-check-label" for="exampleCheck1">
                                                                By clicking "Send Message", you agree to our Privacy Policy.
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 text-center text-md-right mt-3">

                                                        <button class="btn send-btn" data-ktwizard-type="action-submit">
                                                            Send Message <i class="fa fa-caret-right ml-3"></i>
                                                        </button>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>





                        </div>
                    </div>

                    <div id="rentals" class="container tab-pane fade">
                        <div class="row">


                            <?php


                            foreach ($data_arr as $key => $rowData) {
                                //echo "<pre>";
                                // print_r($rowData);
                            ?>
                                <div class="col-md-6 col-lg-4 mb-4">
                                    <a href="{{route('prop_cat_details',$rowData->prop_id)}}" class="property-info">
                                        <figure class="overflow-hidden position-relative">
                                            <img src="{{optional($rowData)->picture_url}}" alt="">
                                        </figure>
                                        <p class="mb-1 text-blue">{{optional($rowData)->property_title}}</p>
                                        <!-- <h5 class="font-weight-bold">{{optional($rowData)->property_title}}</h5> -->
                                        <ul class="text-light list-unstyled d-flex align-items-start my-1">
                                            <li>{{optional($rowData)->location_district}}, </li>
                                            <li>{{optional($rowData)->location_district}}, </li>
                                        </ul>
                                        <p class="mb-2 text-light">{{optional($rowData)->listprice_currency}} {{optional($rowData)->listprice}}</p>
                                    </a>
                                </div>

                            <?php
                            }
                            ?>


                        </div>
                    </div>

                    <div id="more" class="container tab-pane fade">
                        <div class="row">

                            <?php


                            foreach ($data_arr as $key => $rowData) {
                                //echo "<pre>";
                                // print_r($rowData);
                            ?>
                                <div class="col-md-6 col-lg-4 mb-4">
                                    <a href="{{route('prop_cat_details',$rowData->prop_id)}}" class="property-info">
                                        <figure class="overflow-hidden position-relative">
                                            <img src="{{optional($rowData)->picture_url}}" alt="">
                                        </figure>
                                        <p class="mb-1 text-blue">{{optional($rowData)->property_title}}</p>
                                        <!-- <h5 class="font-weight-bold">{{optional($rowData)->property_title}}</h5> -->
                                        <ul class="text-light list-unstyled d-flex align-items-start my-1">
                                            <li>{{optional($rowData)->location_district}}, </li>
                                            <li>{{optional($rowData)->location_district}}, </li>
                                        </ul>
                                        <p class="mb-2 text-light">{{optional($rowData)->listprice_currency}} {{optional($rowData)->listprice}}</p>
                                    </a>
                                </div>

                            <?php
                            }
                            ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Filter Modal -->
<div class="modal" id="filterModal">
        <div class="modal-dialog">
            <img src="{{asset('ayra/front/img/welcome-bg.png')}}" class="modal-bg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <!-- Modal body -->
                <div class="modal-body text-center">
                    <h2 class="mb-2 mb-md-3">Find Your Next Home</h2>
                    <h5>Search properties for sale in the Cayman Islands</h5>
                    <form class="contact-form mt-4" action="{{route('searchAdvance')}}">
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <input type="text" name="propertyName" class="form-control" id="" placeholder="Property Name or #MLS">
                            </div>
                            <div class="form-group col-lg-6">
                                <select name="propID" class="form-control" id="">
                                    <option selected hidden>Property Type</option>
                                    <?php 
                                $data_arr=AyraHelp::getProprtyCat();
                                foreach ($data_arr as $key => $rowData) {
                                  ?>
                                   <option value="{{$rowData->id}}">{{$rowData->property_type}}</option>
                                  <?php
                                }
                                ?>
                                </select> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <select name="" class="form-control" id="">
                                    <option selected hidden>Location</option>
                                    <option value="bodden-town">Bodden Town</option>  
                                    <option value="breakers">Breakers</option>        
                                </select> 
                            </div>
                            <div class="form-group col-lg-6">
                                <select name="" class="form-control" id="">
                                    <option selected hidden>Price</option>
                                    <option value="bodden-town">Bodden Town</option>  
                                    <option value="breakers">Breakers</option>        
                                </select> 
                            </div>
                        </div>

                        <div id="advanced" style="display: none;">
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <select name="" class="form-control" id="">
                                        <option selected hidden>Beds</option>
                                        <option value="bodden-town">Bodden Town</option>  
                                        <option value="breakers">Breakers</option>        
                                    </select> 
                                </div>
                                <div class="form-group col-lg-6">
                                    <select name="" class="form-control" id="">
                                        <option selected hidden>Baths</option>
                                        <option value="bodden-town">Bodden Town</option>  
                                        <option value="breakers">Breakers</option>        
                                    </select> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <select name="" class="form-control" id="">
                                        <option selected hidden>View</option>
                                        <option value="bodden-town">Bodden Town</option>  
                                        <option value="breakers">Breakers</option>        
                                    </select> 
                                </div>
                                <div class="form-group col-lg-6">
                                    <input type="text" name="" class="form-control" id="" placeholder="MLS">
                                </div>
                            </div>
                        </div>

                        <div>
                            <button type="submit" class="btn blue-btn">Search <i class="fa fa-search"></i></button>
                            <button id="adv-btn" type="button"  class="btn btn-advanced">Advanced Search <span id="symbol">+</span></button>
                            <button type="reset" class="btn btn-reset" value="Reset">Reset <i class="fa fa-refresh"></i></button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- filter model  -->
