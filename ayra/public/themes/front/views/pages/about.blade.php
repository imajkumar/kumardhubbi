<?php

use App\Helpers\AyraHelp;

$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 2);
$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;
$myteam_arr = AyraHelp::getTeambyVID($web_arr_->vid);






?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-white text-center mt-4">About Us</h1>
        </div>
    </div>
</div>
</section>


<!-- WELCOME SECTION -->
<section class="welcome-bg mt-5 mt-xl-0">
    <div class="container-fluid p-0">
        <div class="row justify-content-end align-items-center no-gutters">
            <div class="col-10 col-md-6 col-xl-5 mx-auto mr-md-0">
                <h2>Few Words About {{optional($web_arr_)->name}}</h2>
                <p class="my-4">
                    {!! optional($static_block_data)->static_content !!}
                </p>
                <!-- <h3>Paula McCartney</h3> -->
            </div>
            <div class="col-md-5 ml-xl-auto">
                <a href="" class="d-block text-right">
                    <img src="{{$wecomeIMG}}">
                </a>
            </div>
        </div>
    </div>
</section>


<!-- FEATURED PROPERTIES -->
<?php
$data_arr = AyraHelp::getmyTeamByHOST_HOME($web_arr_->vid);


?>
<section class="py-4 py-lg-5">
    <div class="container py-md-5">
        <div class="row">
            <div class="col-12 mb-4">
                <h2 class="text-center text-md-left">Our team members</h2>	    		
            </div>
        </div>

        <!-- <div class="row">
            <div class="col-12 overflow-hidden">
                <div class="owl-carousel owl-theme" id="featured-slider">
                    <?php
                    foreach ($data_arr as $key => $rowData) {
                        $photo = asset('uploads/img/logo') . "/" . $rowData->photo;

                    ?>
                        
                            <div class="item">
                            <a href="{{route('viewTeamMemberDetails',$rowData->id)}}">
                                <div class="featured-box">
                                    <figure class="mb-0">
                                        <img src="{{$photo}}" alt="Air Conditioner">
                                    </figure>
                                    <figcaption>
                                        <h5 class="mb-1">{{$rowData->name}}</h5>
                                        <p class="small">{$rowData->designation}}</p>
                                        <ul class="list-unstyled my-2">
                                            <li class="d-flex align-items-center mb-1">
                                                <i class="fa fa-envelope mr-2"></i>
                                                <a href="mailto:{{$rowData->email}}">{{$rowData->email}}</a>
                                            </li>
                                            <li class="d-flex align-items-center">
                                                <i class="fa fa-phone mr-2"></i>
                                                <a href="mailto:{{$rowData->email}}">{{$rowData->email}}</a>
                                            </li>
                                        </ul>

                                        <a href="">
                                            <img src="{{ asset('ayra/front/img/arrow.png')}}" alt="">
                                        </a>

                                    </figcaption>
                                </div>
                                </a>
                            </div>
                        

                    <?php
                    }
                    ?>


                </div>
            </div>
        </div> -->
        
        <div class="row justify-content-center">
        <?php
                    foreach ($data_arr as $key => $rowData) {
                        $photo = asset('uploads/img/logo') . "/" . $rowData->photo;

                        $alink= $rowData->id ==1 ? "paulaMccartney" :"MelisaMcTaggart";


                    ?>
            <div class="col-md-6 col-lg-4 mb-5 mb-md-0">
                <div class="featured-box">
                    <figure class="mb-0">
                        <img src="{{$photo}}" alt="Air Conditioner">
                    </figure>
                    <figcaption>
                        <h5 class="mb-1">{{optional($rowData)->name}}</h5>
                        <p class="small">{{optional($rowData)->designation}}</p>
                        <ul class="list-unstyled my-2">
                            <li class="d-flex align-items-center mb-1">
                                <i class="fa fa-envelope mr-2"></i>            
                                <a href="mailto:{{$rowData->email}}">{{$rowData->email}}</a>
                            </li>
                            <li class="d-flex align-items-center">
                                <i class="fa fa-phone mr-2"></i>            
                                <a href="tel:+{{$rowData->phone}}">{{$rowData->phone}}</a>
                            </li>
                        </ul>
                        <a href="{{route($alink)}}"><img src="{{ asset('ayra/front/img/arrow.png')}}" alt=""></a>
                    </figcaption>
                </div>
            </div>
            <?php 
                    }
                    ?>
           
        </div>
    </div>
</section>


<!-- CONTACT FORM -->
<section class="py-5">
    <div class="container">
        <div class="row">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif
            <div class="col-lg-6 mx-auto">
    				<div class="z-index-0 position-relative bg-white agent-info p-4">
						<h3 id="mess_info" style="color:#008080"></h3>
	    				<form id="kt_form_1"  id="contact_formAjaxSubmit"  method="post" action="{{route('saveContactData')}}" class="contact-form">
							@csrf
	    					<div class="row">
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="first_name" name="first_name" class="form-control" placeholder="First Name*">
		    						<label>First Name*</label>
		    					</div>	    					
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="last_name"  name="last_name" class="form-control" placeholder="Last Name*">
		    						<label>Last Name*</label>
		    					</div>
		    				</div>
		    				<div class="row">
		    					<div class="form-group col-md-6">
		    						<input type="email" required id="email" name="email" class="form-control" placeholder="Email Address*">
		    						<label>Email Address*</label>
		    					</div>	    					
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="phone" name="phone" class="form-control" placeholder="Phone No.*">
		    						<label>Phone No.*</label>
		    					</div>
		    				</div>
		    				<div class="row">
		    					<div class="form-group col-md-12">
		    						<textarea class="form-control" id="message" required name="message" placeholder="Your Message*" rows="5"></textarea>
		    						<label>Your Message*</label>
		    					</div>
	                            <div class="col-12">
	                                <div class="form-check">
	                                    <input required  type="checkbox" class="form-check-input" id="exampleCheck1">
	                                    <span class="form-check-label" for="exampleCheck1">
	                                        I accept Privacy Policy & Terms & Conditions
	                                    </span>
	                                </div>
	                            </div>
		    					<div class="col-12 text-center text-md-right mt-3">
									
									<button type="button" class="btn send-btn"  id="btnContactSave">
									Send Message <i class="fa fa-caret-right ml-3"></i>
										</button>

		    					</div>	
		    				</div>
	    				</form>
	    			</div>
    			</div>
        </div>
    </div>
</section>