<?php

use App\Helpers\AyraHelp;

$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 2);
$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;
$data_arr = AyraHelp::getSellingData();
$cmid=Request::segment(2);

$com_arr = DB::table('communities')->where('vid',$web_arr_->vid)->where('id',$cmid)->first();


?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-12">
					<h1 class="text-white text-center mt-4">{{optional($com_arr)->title}}</h1>
			    </div>
			</div>
		</div>
    </section>
<?php 
$data=AyraHelp::getPropertyIncludeCommunity();
//print_r($data);

?>

    <!-- CONTACT FORM -->
    <section class="pt-5">
    	<div class="container">
    		<div class="row mb-5">
    			<div class="col-xl-10 mx-auto">

                  
                    <p>
                    {!! optional($com_arr)->info_data !!}

                </p>
                    
    			</div>
    		</div>
            <div class="row"> 
                <div class="col-md-6">
                    <h3><span>{{count($data)}}</span> Properties Found</h3>
                </div> 
                <div class="col-md-6 text-md-right my-4 mt-md-0">
                    <div class="dropdown">
                        <button type="button" class="blue-btn dropdown-toggle" data-toggle="dropdown">
                            Filter
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Newest</a>
                            <a class="dropdown-item" href="#">Price High to Low</a>
                            <a class="dropdown-item" href="#">Price Low to High</a>
                        </div>
                    </div>
                </div>        
                <?php


                            foreach ($data as $key => $rowData) {
                                //echo "<pre>";
                                // print_r($rowData);
                                $photo=asset('uploads/img/logo')."/".$rowData->photo;
                            ?>
                                <div class="col-md-6 col-lg-4 mb-4">
                                    <a href="" class="property-info text-center">
                                        <figure class="overflow-hidden position-relative">
                                            <img src="{{$photo}}" alt="{{optional($rowData)->property_name}}">
                                        </figure>
                                        <h5 class="font-weight-bold text-blue">{{optional($rowData)->property_name}}</h5>

                                        <p class="font-weight-bold d-flex justify-content-around my-3 text-black">
                                            <span class="mx-lg-2">{{optional($rowData)->listprice_currency}} {{optional($rowData)->monthly_rent}}</span>
                                            <span class="mx-lg-2">MLS# : {{optional($rowData)->id}}</span>
                                        </p>

                                        <ul class="text-light list-unstyled property-amenities mt-4">
                                            <li title="Sq. Ft.">
                                                <i class="fa fa-map"></i> 3850
                                            </li>
                                            <li title="Bedrooms">
                                                <i class="fa fa-bed"></i> NA
                                            </li>
                                            <li title="Bathrooms">
                                                <i class="fa fa-bath"></i>NA
                                            </li>
                                            <li title="Year Built">
                                                <i class="fa fa-calendar"></i> NA
                                            </li>
                                        </ul>
                                    </a>
                                </div>
                            <?php
                            }
                            ?>

              
               

               
                
               

            </div>
    	</div>
    </section>