<?php

use App\Helpers\AyraHelp;

$web_arr_=AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data=AyraHelp::getBlockByHOST_HOME($web_arr_->vid,2);
$wecomeIMG=asset('uploads/img/logo')."/".optional($static_block_data)->video_img;

$prop_type=Request::segment(2); 

$pro_data_arr=AyraHelp::getProprtyBYCat($prop_type);




?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-12">
					<h1 class="text-white text-center mt-4">Properties</h1>
			    </div>
			</div>
		</div>
    </section>


    <!-- CONTACT FORM -->
    <section class="pt-5">
    	<div class="container">
    		<div class="row">
                <div class="col-12">
                    <h2 class="text-center mb-3">{{$prop_type}}</h2>
                </div>
                <?php 
                
                foreach ($pro_data_arr as $key => $rowData) {
                    $prop_data=AyraHelp::getProprtyBYCatDetailBYPropID($rowData->prop_id);
                    $proppic_data=AyraHelp::getProprtyBYCatPictureBYPropID($rowData->prop_id);
                    
                    
                    ?>
                    <!-- <div class="col-md-6 col-lg-4 mb-4">
                    <a href="{{route('prop_cat_details',$rowData->prop_id)}}" class="property-info">
                        <figure class="overflow-hidden position-relative">    
                            <img src="{{optional($proppic_data)->picture_url}}" alt="">
                        </figure>                                
                        <p class="mb-1 text-blue">{{optional($prop_data)->property_title}}</p>
                        <h5 class="font-weight-bold"></h5>
                        <ul class="text-light list-unstyled d-flex align-items-start my-1">
                            <li>{{optional($rowData)->location_district}}</li>
                            <li>{{optional($rowData)->location_address_1}}</li>
                        </ul>
                        <p class="mb-2 text-light">{{optional($prop_data)->listprice_currency}} {{optional($prop_data)->listprice}}</p>
                    </a>
                </div> -->

                <div class="col-md-6 col-lg-4 mb-4">
                                    <a href="{{route('prop_cat_details',$rowData->prop_id)}}" class="property-info text-center">
                                        <figure class="overflow-hidden position-relative">
                                            <img src="{{optional($proppic_data)->picture_url}}" alt="{{optional($proppic_data)->property_title}}">
                                        </figure>
                                        <h5 class="font-weight-bold text-blue">{{optional($prop_data)->property_title}}</h5>

                                        <p class="font-weight-bold d-flex justify-content-around my-3 text-black">
                                            <span class="mx-lg-2">{{optional($prop_data)->listprice_currency}} {{optional($rowData)->listprice}}</span>
                                            <span class="mx-lg-2">MLS# : {{optional($prop_data)->mlsId}}</span>
                                        </p>

                                        <ul class="text-light list-unstyled property-amenities mt-4">
                                            <li title="Sq. Ft.">
                                                <i class="fa fa-map"></i> 3850
                                            </li>
                                            <li title="Bedrooms">
                                                <i class="fa fa-bed"></i> {{optional($prop_data)->num_bedrooms}}
                                            </li>
                                            <li title="Bathrooms">
                                                <i class="fa fa-bath"></i> {{optional($prop_data)->num_full_bathrooms}}
                                            </li>
                                            <li title="Year Built">
                                                <i class="fa fa-calendar"></i> {{optional($prop_data)->year_built}}
                                            </li>
                                        </ul>
                                    </a>
                                </div>

                



                    <?php
                }
                ?>

                
                
                

                    
    			
    		</div>
    	</div>
    </section>