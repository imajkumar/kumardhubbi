<?php 
$web_arr_=AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data=AyraHelp::getBlockByHOST_HOME($web_arr_->vid,3);

$wecomeIMG=asset('uploads/img/logo')."/".optional($static_block_data)->video_img;
$WEBSETTING = config('web_settings');
$public_menu=$WEBSETTING['public_menu_footer'];
$web_contact=$WEBSETTING['web_contact'];
?>


<!-- BANNER CONTENT -->
<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-12">
					<h1 class="text-white text-center mt-4">Contact Us</h1>
			    </div>
			</div>
		</div>
    </section>


    <!-- CONTACT FORM -->
    <section class="py-5">
    	<div class="container py-lg-5">
    		<div class="row align-items-center">
    			<div class="col-lg-6 mb-4 mb-lg-0">
                    <p>
					{!! optional($static_block_data)->static_content !!}
						
					</p>
                    <ul class="mt-4 list-unstyled agent-contact">
                        <li>
                            <span><i class="fa fa-map-marker"></i></span>
							{{optional($web_contact)->site_address}}

                        </li>
                        <li>
                            <span><i class="fa fa-envelope"></i></span>
                            <a href="{{optional($web_contact)->email}}">{{optional($web_contact)->email}}</a>
                        </li>
                        <li>
                            <span><i class="fa fa-phone"></i></span>
                            <a href="tel:+{{optional($web_contact)->phone}}">Cell : {{optional($web_contact)->phone}}</a>
                        </li>
                        <li>
                            <span><i class="fa fa-building-o"></i></span>
                            <a href="tel:+{{optional($web_contact)->office_phone}}">Office : {{optional($web_contact)->office_phone}}</a>
                        </li>
                        <li>
                            <span><i class="fa fa-fax"></i></span>
                            Fax : +{{optional($web_contact)->fax_phone}}
                        </li>
                    </ul>
                </div>


			@if(Session::has('success'))
			   <div class="alert alert-success">
			     {{ Session::get('success') }}
			   </div>
			@endif
    			<div class="col-lg-6 mx-auto">
    				<div class="z-index-0 position-relative bg-white agent-info p-4">
						<h3 id="mess_info" style="color:#008080"></h3>
	    				<form id="kt_form_1"  id="contact_formAjaxSubmit"  method="post" action="{{route('saveContactData')}}" class="contact-form">
							@csrf
	    					<div class="row">
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="first_name" name="first_name" class="form-control" placeholder="First Name*">
		    						<label>First Name*</label>
		    					</div>	    					
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="last_name"  name="last_name" class="form-control" placeholder="Last Name*">
		    						<label>Last Name*</label>
		    					</div>
		    				</div>
		    				<div class="row">
		    					<div class="form-group col-md-6">
		    						<input type="email" required id="email" name="email" class="form-control" placeholder="Email Address*">
		    						<label>Email Address*</label>
		    					</div>	    					
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="phone" name="phone" class="form-control" placeholder="Phone No.*">
		    						<label>Phone No.*</label>
		    					</div>
		    				</div>
		    				<div class="row">
		    					<div class="form-group col-md-12">
		    						<textarea class="form-control" id="message" required name="message" placeholder="Your Message*" rows="5"></textarea>
		    						<label>Your Message*</label>
		    					</div>
	                            <div class="col-12">
	                                <div class="form-check">
	                                    <input required  type="checkbox" class="form-check-input" id="exampleCheck1">
	                                    <span class="form-check-label" for="exampleCheck1">
	                                        I accept Privacy Policy & Terms & Conditions
	                                    </span>
	                                </div>
	                            </div>
		    					<div class="col-12 text-center text-md-right mt-3">
									
									<button type="button" class="btn send-btn"  id="btnContactSave">
									Send Message <i class="fa fa-caret-right ml-3"></i>
										</button>

		    					</div>	
		    				</div>
	    				</form>
	    			</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="border">
        <div id="googleMap"></div>
    </section>