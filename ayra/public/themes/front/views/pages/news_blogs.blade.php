<!-- BANNER CONTENT -->
<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-12">
					<h1 class="text-white text-center mt-4">News and Blogs</h1>
			    </div>
			</div>
		</div>
    </section>


    <!-- BLOGS -->
    <section class="pt-5">
        <div class="container pt-xl-5 px-xl-5">
            <div class="row align-items-center mb-5">
                <div class="col-md-6 mb-4 mb-md-0">
                    <p class="text-blue">January 31, 2020</p>
                    <h5 class="font-weight-bold my-2">Benefits of Buying property or land in the Sister Islands</h5>
                    <h6>Grand Cayman is the largest of the three sister islands making up the Cayman Islands, and while it is the largest and most populated of the three, home to over 52,000 people, it is also considered the most expensive.</h6>
                    <a href="" class="blue-btn mt-3">Read More</a>
                </div>
                <div class="col-md-6">
                    <img src="img/news1.png" alt="">
                </div>
            </div>

            <div class="row align-items-center">
                <div class="col-md-6 mb-4 mb-md-0">
                    <p class="text-blue">January 31, 2020</p>
                    <h5 class="font-weight-bold my-2">Living In Luxury With Diamond Properties: Diamond’s Edge</h5>
                    <h6>What’s more luxurious than diamonds? We’re starting off this series with one of our favourite listings, Diamond’s Edge: MLS# 409273. This spacious piece of land, located in the booming district of West Bay is both...</h6>
                    <a href="" class="blue-btn mt-3">Read More</a>
                </div>
                <div class="col-md-6">
                    <img src="img/news1.png" alt="">
                </div>
            </div>
        </div>
    </section>