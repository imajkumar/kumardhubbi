<?php
$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 2);
$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;

?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-white text-center mt-4">News and Blogs</h1>
        </div>
    </div>
</div>
</section>
<?php $data_arr = AyraHelp::getmyNewsByHOST_HOME($web_arr_->vid);
?>


<!-- BLOGS -->
<section class="pt-5">
    <div class="container pt-xl-5 px-xl-5">

        <?php
        foreach ($data_arr as $key => $rowData) {
            $created_at=date('F j ,Y', strtotime($rowData->start_date));
            $photo = asset('uploads/img/logo') . "/" . $rowData->photo;
        ?>
           
            <div class="row align-items-center mb-5">
                <div class="col-md-6 order-md-2 mb-4 mb-md-0">
                    <img src="{{$photo}}" alt="">
                </div>
                <div class="col-md-6">
                    <p class="text-blue">{{$created_at}}</p>
                    <h5 class="font-weight-bold my-2">{{optional($rowData)->title}}</h5>
                    <h6>{!!  Str::limit(optional($rowData)->info, 150,' (...)') !!}</h6>
                    <a href="{{route('blogDetails',$rowData->id)}}" class="blue-btn mt-3">Read More</a>
                </div>
            </div>
           
        <?php
        }
        ?>



    </div>
</section>