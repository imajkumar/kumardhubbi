<?php
$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 2);
$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;

$prop_ID = Request::segment(2);
$prop = AyraHelp::getProprtyBYPropID($prop_ID);
$prop_data = AyraHelp::getProprtyBYCatDetailBYPropID($prop_ID);
$proppic_data = AyraHelp::getProprtyBYCatPictureBYPropID($prop_ID);
$propAgent_data = AyraHelp::getProprtyAgentBYPropID($prop_ID);







?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-white text-center mt-4">Property Details</h1>
        </div>
    </div>
</div>
</section>



<section class="pt-5 pb-xl-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-4 mb-lg-0">
                <div id="detail-slider" class="carousel slide" data-ride="carousel" data-interval="false">

                    <ul class="carousel-indicators">
                        <li data-target="#detail-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#detail-slider" data-slide-to="1"></li>
                        <li data-target="#detail-slider" data-slide-to="2"></li>
                    </ul>

                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="img/property1.jpg" alt="">
                        </div>
                        <div class="carousel-item">
                            <img src="img/property1.jpg" alt="">
                        </div>
                        <div class="carousel-item">
                            <img src="img/property1.jpg" alt="">
                        </div>
                    </div>

                    <div class="controls">
                        <a class="prev" href="#detail-slider" data-slide="prev">
                            <i class="fa fa-caret-left"></i>
                        </a>
                        <a class="next" href="#detail-slider" data-slide="next">
                            <i class="fa fa-caret-right"></i>
                        </a>
                    </div>

                </div>

                <div class="d-md-flex justify-content-between align-items-center py-4 mt-5 mt-md-0">
                    <div class="mb-3 mb-md-0">
                        <h2 class="property-name mb-2">{{optional($prop_data)->property_title}}</h2>
                        <h2 class="property-name mb-2"></h2>
                        <p class="text-black">
                            <i class="fa fa-map-marker"></i>
                            {{optional($prop)->location_district}},
                            {{optional($prop)->location_address_1}}
                        </p>
                    </div>

                    <div class="text-left text-md-right">
                        <p class="text-black mb-2 font-weight-bold">MLS# {{$prop_ID}}</p>
                        <span class="label-blue">{{optional($prop_data)->listprice_currency}} {{optional($prop_data)->listprice}}</span>
                    </div>
                </div>

                <div class="row text-center">
                    <div class="col-6 col-md-3 mb-4 mb-md-0">
                        <div class="bg-light-blue p-3 h-100">
                            <p class="mb-1">Property width</p>
                            <h3 class="font-weight-bold">{{$prop_data->width}} <span class="small"></span></h3>
                        </div>
                    </div>

                    <div class="col-6 col-md-3 mb-4 mb-md-0">
                        <div class="bg-light-blue p-3 h-100">
                            <p class="mb-1">Property depth</p>
                            <h3 class="font-weight-bold">{{$prop_data->depth}} <span class="small"></span></h3>
                        </div>
                    </div>

                    <div class="col-6 col-md-3">
                        <div class="bg-light-blue p-3 h-100">
                            <p class="mb-1">Acres:</p>
                            <h3 class="font-weight-bold">5.67</h3>
                        </div>
                    </div>

                    <div class="col-6 col-md-3">
                        <div class="bg-light-blue p-3 px-md-4 h-100">
                            <p class="mb-1">Built in year:</p>
                            <h3 class="font-weight-bold">{{$prop_data->year_built}}</h3>
                        </div>
                    </div>
                </div>

                <div class="mt-5">
                    <h5 class="font-weight-bold mb-2">Description:</h5>
                    <p>
                        {!! $prop_data->description !!}
                    </p>

                    <h5 class="font-weight-bold mb-2 mt-5">Property Details:</h5>
                    <div class="d-md-flex justify-content-between">
                        <ul class="list-unstyled property-details flex-fill mb-3 mb-md-0">
                            <li><span>Property Type:</span> {{optional($prop)->property_type}}</li>
                            <li><span>Property Status:</span> Pending</li>
                            <li><span>Views:</span> {{optional($prop_data)->view}}</li>
                            <li><span>Parcel:</span> {{optional($prop_data)->parcel}}</li>
                            <!-- <li><span>Covenants:</span>  NoLand</li> -->
                            <!-- <li><span>Possession:</span>  At Completion</li> -->
                            <li><span>Title:</span> {{optional($prop_data)->property_title}}</li>
                            <li><span>How Shown:</span> By Appointment/List</li>
                            <li><span>Sea Frontage</span> {{optional($prop_data)->sea_frontage}}</li>
                            <li><span>City Water</span> {{optional($prop_data)->city_water}}</li>
                        </ul>
                        <ul class="list-unstyled property-details flex-fill">
                            <!-- <li><span>Listing Type:</span>  Commercial</li> -->
                            <li><span>Address:</span> {{optional($prop)->location_district}} ,{{optional($prop)->location_address_1}}</li>
                            <li><span>Block:</span> {{optional($prop_data)->block}}</li>
                            <!-- <li><span>Zoning:</span>  Commercial Neighbourhood</li> -->
                            <!-- <li><span>Certificate:</span>  Not Issued</li> -->
                            <li><span>Frontage Road:</span> {{optional($prop_data)->lot_size}}</li>
                            <li><span>Floor Level:</span> {{optional($prop_data)->floor_level}}</li>
                            <li><span>Garage:</span> {{optional($prop_data)->garage}}</li>
                            <li><span>TV:</span> {{optional($prop_data)->tv}}</li>
                            <li><span>AC:</span> {{optional($prop_data)->ac}}</li>
                            <li><span>Car Port:</span> {{optional($prop_data)->car_port}}</li>
                        </ul>
                    </div>
                </div>

                <div class="mt-5">
                    <div class="row justify-content-center justify-content-lg-start">
                        <div class="col-8 col-sm-6 col-xl-4 mb-4">
                            <a href="javascript:void(0)" class="prop-option blue-btn d-block" data-toggle="modal" data-target="#currency-modal"><i class="fa fa-retweet"></i> Currency Converter</a>
                        </div>
                        <div class="col-8 col-sm-6 col-xl-4 mb-4">
                            <a href="javascript:void(0)" class="prop-option blue-btn d-block" data-toggle="modal" data-target="#email-modal"><i class="fa fa-envelope"></i> Email to Friend</a>
                        </div>
                        <div class="col-8 col-sm-6 col-xl-4 mb-4">
                            <a href="{{route('propertyPrintDetail',Request::segment(2))}}" class="prop-option blue-btn d-block"><i class="fa fa-print"></i> Print Page</a>
                        </div>
                        <div class="col-8 col-sm-6 col-xl-4 mb-4">
                            <a href="#contact-agent" class="prop-option blue-btn d-block"><i class="fa fa-info-circle"></i> Request More info</a>
                        </div>
                        <div class="col-8 col-sm-6 col-xl-4 mb-4">
                            <a href="javascript:void(0)" class="prop-option blue-btn d-block" data-toggle="modal" data-target="#mortgage-modal"><i class="fa fa-calculator"></i> Mortgage Calculator</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <div class="row pl-lg-3 pl-xl-4">
                    <div class="col-md-6 col-lg-12 mb-4 mb-md-0 mb-lg-5">
                        <div class="agent-info text-center shadow p-4 h-100">
                            <h5 class="font-weight-bold mb-4">Agent Info</h5>
                            <figure>
                                <img src="{{ asset('ayra/front/img/team2.jpg')}}" alt="Agent">
                            </figure>
                            <h5 class="font-weight-bold mb-1 text-blue">{{optional($propAgent_data)->company_name}}</h5>
                            <p class="text-black mb-3">Broker / Owner</p>
                            <?php
                            if (isset($propAgent_data->agent_email)) {
                            ?>
                                <ul class="list-unstyled text-left agent-contact">
                                    <li>
                                        <span><i class="fa fa-envelope"></i></span>
                                        <a href="mailto:{{optional($propAgent_data)->agent_email}}">{{optional($propAgent_data)->agent_email}}</a>
                                    </li>
                                    <!-- <li>
                                        <span><i class="fa fa-phone"></i></span>
                                        <a href="tel:+13455163521"> +1 345 516 3521</a>
                                    </li> -->
                                </ul>
                            <?php
                            }
                            ?>

                        </div>
                    </div>
                    <div class="col-md-6 col-lg-12">
                        <div class="agent-info text-center shadow p-4 h-100">
                            <h5 class="font-weight-bold mb-4">Contact Agent</h5>
                            <h4 id="mess_info"></h4>

                            <form id="kt_form_1" method="post" action="{{route('savePropertyLeadData')}}" class="contact-form">
                                @csrf
                                <input type="hidden" id="frmType" value="3">
                                <div class="form-group pt-0">
                                    <input type="text" required name="first_name" id="first_name" class="form-control" placeholder="First Name*">
                                </div>
                                <div class="form-group pt-0">
                                    <input type="text" required name="last_name" id="last_name" class="form-control" placeholder="Last Name*">
                                </div>
                                <div class="form-group pt-0">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email Address*">
                                </div>
                                <div class="form-group pt-0">
                                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone No.*">
                                </div>
                                <div class="form-group pt-0">
                                    <textarea class="form-control" name="message" id="message" placeholder="Your Message*" rows="5"></textarea>
                                </div>
                                <div class="form-check">
                                    <input ype="checkbox" type="checkbox" class="form-check-input" id="chkLead1">
                                    <span class="form-check-label" for="exampleCheck1">
                                        I accept Privacy Policy & Terms & Conditions
                                    </span>
                                </div>
                                <div class="text-center mt-3">
                                    <button type="button" id="btnsavePropertyLeadData" class="btn send-btn">Send Message <i class="fa fa-caret-right ml-3"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- CONTACT FORM -->
<section class="py-5">
    <div class="container">
        <div class="row">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif
                
            <div class="col-lg-12 mx-auto">
                <h2 class="text-center mb-3">Let’s talk...</h2>
                <div class="col-lg-12 mx-auto">
                    <div class="z-index-0 position-relative bg-white agent-info p-4">
                        <h3 id="mess_info" style="color:#008080"></h3>
                        <form id="kt_form_1" id="contact_formAjaxSubmit" method="post" action="{{route('saveContactData')}}" class="contact-form">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" required id="first_name" name="first_name" class="form-control" placeholder="First Name*">
                                    <label>First Name*</label>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" required id="last_name" name="last_name" class="form-control" placeholder="Last Name*">
                                    <label>Last Name*</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="email" required id="email" name="email" class="form-control" placeholder="Email Address*">
                                    <label>Email Address*</label>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" required id="phone" name="phone" class="form-control" placeholder="Phone No.*">
                                    <label>Phone No.*</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <textarea class="form-control" id="message" required name="message" placeholder="Your Message*" rows="5"></textarea>
                                    <label>Your Message*</label>
                                </div>
                                <div class="col-12">
                                    <div class="form-check">
                                        <input required type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <span class="form-check-label" for="exampleCheck1">
                                            I accept Privacy Policy & Terms & Conditions
                                        </span>
                                    </div>
                                </div>
                                <div class="col-12 text-center text-md-right mt-3">

                                    <button type="button" class="btn send-btn" id="btnContactSave">
                                        Send Message <i class="fa fa-caret-right ml-3"></i>
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Currency Modal -->
<div class="modal property-modal" id="currency-modal">
    <div class="modal-dialog">
        <img src="{{asset('ayra/front/img/welcome-bg.png')}}" class="modal-bg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>

            <!-- Modal body -->
            <div class="modal-body text-center">
                <h2 class="mb-2 mb-md-3">Currency Converter</h2>
                <form class="contact-form mt-4">
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <select name="" class="form-control" id="currSelect">
                                <option value="1">US$ to CI$</option>
                                <option value="2">CI$ to US$</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6">
                            <input type="text" name="" class="form-control" id="currAmount" placeholder="Amount*">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">
                            <input type="text" style="display: none;" name="" class="form-control" id="txtCurrView" placeholder="Your conversion amount is" readonly="">
                        </div>
                    </div>

                    <div>
                        <button type="button" id="btnCurrview" class="btn blue-btn">Submit</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- Email Modal -->
<div class="modal property-modal" id="email-modal">
    <div class="modal-dialog">
        <img src="{{asset('ayra/front/img/welcome-bg.png')}}" class="modal-bg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>

            <!-- Modal body -->
            <div class="modal-body text-center">
                <h2 class="mb-2 mb-md-3">Email to Friend</h2>
                <form class="contact-form mt-4">
                    <p class="mb-2 text-left">*Denotes required field</p>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <input type="text" id="name" class="form-control" placeholder="Recipient Name*">
                        </div>
                        <div class="form-group col-lg-6">
                            <input type="email" id="RepEmail" class="form-control" placeholder="Recipient Email*">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-lg-6">
                            <input type="text" id="myname" class="form-control" placeholder="Your Name*">
                        </div>
                        <div class="form-group col-lg-6">
                            <input type="email" id="myemail" class="form-control" placeholder="Your Email*">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">
                            <textarea rows="2" id="msg" class="form-control" placeholder="Meassage*"></textarea>
                        </div>
                    </div>
                    <input type="hidden" id="propID" value="{{ Request::segment(2) }}">
                    <div class="row">
                        <div class="col-12 text-left">
                            <p>By clicking "Send Message", you agree to our <a href="" target="_blank">Privacy Policy.</a></p>
                            <div class="form-check mt-2 mb-4">
                                <input type="checkbox" class="form-check-input" va name="sendcopy" id="sendcopy">
                                <span class="form-check-label" for="send-copy">
                                    Send me a Copy
                                </span>
                            </div>
                        </div>
                    </div>


                    <div>
                        <button type="button" id="btnSendToFriend" class="btn blue-btn">Send Message</button>
                    </div>
                </form>
                <div id="mess_info_data">..</div>
            </div>

        </div>
    </div>
</div>

<!-- Mortgage Modal -->
<div class="modal property-modal" id="mortgage-modal">
    <div class="modal-dialog">
        <img src="{{asset('ayra/front/img/welcome-bg.png')}}" class="modal-bg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>

            <!-- Modal body -->
            <div class="modal-body text-center">
                <h2 class="mb-2 mb-md-3">Mortgage Calculator</h2>

                <div class="row ">
                    <div class="col-lg-7">
                        <form class="contact-form mt-4" id="mortcalcfrm" name="mortcalcfrm">
                            <div class="row">
                                <div class="form-group col-12">
                                    <p class="text-left mb-2">Mortgage Term(Years):</p>

                                    <input name="var_term" id="term" class="form-control" type="text" placeholder="Years" value="30" onkeypress="javascript: return chkprice(event);">

                                </div>
                                <div class="form-group col-12">
                                    <p class="text-left mb-2">Interest Rate(% Per year):</p>
                                    <input name="intrate" id="percent" class="form-control" type="text" placeholder="% Per year" value="2.96" onkeypress="javascript: return chkprice(event);">
                                </div>
                                <div class="form-group col-12">
                                    <p class="text-left mb-2">Principal Amount($):</p>
                                    <input name="main_amt" id="tprinc" class="form-control" placeholder="$" type="text" value="7895000.00" onkeypress="javascript: return chkprice(event);">

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <p class="text-left mb-2">Down Payment(%):</p>
                                    <input name="dp_per" id="downp" class="form-control" type="text" placeholder="%" value="20" onkeypress="javascript: return chkprice(event);">
                                </div>
                                <div class="form-group col-md-6">
                                    <p class="text-left mb-2">Amortization(Years):</p>
                                    <input name="var_amort" id="years" class="form-control" type="text" placeholder="Years" value="30" onkeypress="javascript: return chkprice(event);">

                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="bg-light-blue text-left mt-4 p-4">
                        <h5 class="mb-3 font-weight-bold">Your Estimated Payment</h5>
                        <div class="pb-3 border-bottom mb-3">
                            <p class="mb-1">Down Payment Required</p>
                            <p id="downr" class="text-black">$ 1,579,000.00</p>

                        </div>
                        <div class="pb-3 border-bottom mb-3">
                            <p class="mb-1">Mortgage Principal</p>
                            <p id="princ" class="text-black">$ 6,316,000.00</p>

                        </div>
                        <div class="pb-3 border-bottom mb-3">
                            <p class="mb-1">Monthly Payment</p>
                            <p id="paym" class="mb-1">$ 26,492.00</p>


                        </div>
                        <div class="">
                            <p class="mb-1">Still Owing at End of Term</p>
                            <p id="owed" class="text-black">$.0.00</p>




                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>