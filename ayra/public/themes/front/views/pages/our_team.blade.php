<?php

use App\Helpers\AyraHelp;

$web_arr_=AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data=AyraHelp::getBlockByHOST_HOME($web_arr_->vid,2);
$wecomeIMG=asset('uploads/img/logo')."/".optional($static_block_data)->video_img;


$photo=asset('uploads/img/logo')."/".$data_arr->photo;



?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-12">
					<h1 class="text-white text-center mt-4">Our Team</h1>
			    </div>
			</div>
		</div>
    </section>



    <section class="pt-5 pb-xl-5">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-5 col-lg-4 mb-4 mb-md-0">
                    <figure>
                        <img src="{{$photo}}" alt="Agent" class="img-thumbnail">
                    </figure>
                </div>
                <div class="col-md-7 col-lg-8">
                    <h3 class="font-weight-bold mb-2 text-blue">{{optional($data_arr)->name}}</h3>
                    <h5 class="font-weight-bold text-black mb-3">{{optional($data_arr)->designation}}</h5>
                    
                    <ul class="list-unstyled text-left agent-contact">
                        <li>
                            <span><i class="fa fa-envelope"></i></span>
                            <a href="mailto:{{optional($data_arr)->email}}">{{optional($data_arr)->email}}</a>
                        </li>
                        <li>
                            <span><i class="fa fa-phone"></i></span>
                            <a href="tel:+{{optional($data_arr)->phone}}"> {{optional($data_arr)->phone}}</a>
                        </li>
                    </ul>
                </div>

                <div class="col-12">
                    <div class="agent-text">
                       {!! $data_arr->info_data!!}
                    </div>
                </div>
            </div>

            <div class="row">                
                <div class="col-md-8 col-lg-6 mx-auto">
                @if(Session::has('success'))
   <div class="alert alert-success">
     {{ Session::get('success') }}
   </div>
@endif

                    <div class="agent-info text-center shadow p-4 h-100">
                        <h5 class="font-weight-bold mb-4">Send a Message</h5>
                        <form id="kt_form_1"   method="post" action="{{route('saveContactData')}}" class="contact-form">
						@csrf

                            <div class="form-group pt-0">
                                <input type="text" required name="first_name"  class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group pt-0">
                                <input type="text" required name="last_name"  class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group pt-0">
                                <input type="email" name="email" class="form-control" placeholder="Email Address*">
                            </div>                          
                            <div class="form-group pt-0">
                                <input type="text" name="phone" class="form-control" placeholder="Phone No.*">
                            </div>
                            <div class="form-group pt-0">
                                <textarea class="form-control"  name="message" placeholder="Your Message*" rows="5"></textarea>
                            </div>
                            <div class="form-check">
                                <input ype="checkbox" type="checkbox" class="form-check-input" id="exampleCheck1">
                                <span class="form-check-label" for="exampleCheck1">
                                    I accept Privacy Policy & Terms & Conditions
                                </span>
                            </div>
                            <div class="text-center mt-3">
                                <button type="submit" class="btn send-btn">Send Message <i class="fa fa-caret-right ml-3"></i></button>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>