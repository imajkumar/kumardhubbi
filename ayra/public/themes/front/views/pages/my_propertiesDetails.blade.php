<?php

use App\Helpers\AyraHelp;

$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 2);
$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;

?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-white text-center mt-4">Property ADetails</h1>
        </div>
    </div>
</div>
</section>



<section class="pt-5 pb-xl-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-4 mb-lg-0">
                <div id="detail-slider" class="carousel slide" data-ride="carousel" data-interval="false">

                    <ul class="carousel-indicators">
                        <li data-target="#detail-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#detail-slider" data-slide-to="1"></li>
                        <li data-target="#detail-slider" data-slide-to="2"></li>
                    </ul>

                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="img/property1.jpg" alt="">
                        </div>
                        <div class="carousel-item">
                            <img src="img/property1.jpg" alt="">
                        </div>
                        <div class="carousel-item">
                            <img src="img/property1.jpg" alt="">
                        </div>
                    </div>

                    <div class="controls">
                        <a class="prev" href="#detail-slider" data-slide="prev">
                            <i class="fa fa-caret-left"></i>
                        </a>
                        <a class="next" href="#detail-slider" data-slide="next">
                            <i class="fa fa-caret-right"></i>
                        </a>
                    </div>

                </div>

                <div class="d-md-flex justify-content-between align-items-center py-4 mt-5 mt-md-0">
                    <div class="mb-3 mb-md-0">
                        <h2 class="property-name mb-2">{{optional($data_arr)->property_name}}</h2>
                        <p class="text-black">
                            <i class="fa fa-map-marker"></i>
                            {{optional($data_arr)->address}}
                        </p>
                    </div>

                    <div class="text-left text-md-right">
                        <p class="text-black mb-2 font-weight-bold">MLS# {{optional($data_arr)->id}}</p>
                        <span class="label-blue">US ${{optional($data_arr)->monthly_rent}}</span>
                    </div>
                </div>

                <!-- <div class="row text-center">
                    	<div class="col-6 col-md-3 mb-4 mb-md-0">
                    		<div class="bg-light-blue p-3 px-md-4 h-100">
                    			<p class="mb-1">Property width</p>
                    			<h3 class="font-weight-bold">495 <span class="small">mtr</span></h3>
                    		</div>
                    	</div>

                    	<div class="col-6 col-md-3 mb-4 mb-md-0">
                    		<div class="bg-light-blue p-3 px-md-4 h-100">
                    			<p class="mb-1">Property depth</p>
                    			<h3 class="font-weight-bold">495 <span class="small">mtr</span></h3>
                    		</div>
                    	</div>

                    	<div class="col-6 col-md-3">
                    		<div class="bg-light-blue p-3 px-md-4 h-100">
                    			<p class="mb-1">Acres:</p>
                    			<h3 class="font-weight-bold">5.67</h3>
                    		</div>
                    	</div>

                    	<div class="col-6 col-md-3">
                    		<div class="bg-light-blue p-3 px-md-4 h-100">
                    			<p class="mb-1">Built in year:</p>
                    			<h3 class="font-weight-bold">1976</h3>
                    		</div>
                    	</div>
                    </div> -->

                <div class="mt-5">
                    <h5 class="font-weight-bold mb-2">Description:</h5>
                    <p>
                        {!! optional($data_arr)->data_info!!}
                    </p>
                    <?php
                    $pro_cat_arr = DB::table('propertycats')->where('id', $data_arr->id)->first();
                    switch ($data_arr->status) {
                        case 1:
                            $st = 'Pending';
                            break;

                        default:
                            $st = 'New';
                            break;
                    }

                    $agent_arr = AyraHelp::getTeamAgentByID($data_arr->p_agent);
                    $photo = asset('uploads/img/logo') . "/" . $agent_arr->photo;

                    ?>

                    <h5 class="font-weight-bold mb-2 mt-5">Property Details:</h5>
                    <div class="d-md-flex justify-content-between">
                        <ul class="list-unstyled property-details flex-fill mb-3 mb-md-0">
                            <li><span>Property Type:</span> {{optional($pro_cat_arr)->property_type}}</li>
                            <li><span>Property Status:</span> {{$st}}</li>

                        </ul>

                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="row pl-lg-3 pl-xl-4">
                    <div class="col-md-6 col-lg-12 mb-4 mb-md-0 mb-lg-5">
                        <div class="agent-info text-center shadow p-4 h-100">
                            <h5 class="font-weight-bold mb-4">Agent Info</h5>
                            <figure>
                                <img src="{{$photo}}" alt="Agent">
                            </figure>
                            <h5 class="font-weight-bold mb-1 text-blue">{{$agent_arr->name}}</h5>
                            <p class="text-black mb-3">{{$agent_arr->designation}}</p>
                            <ul class="list-unstyled text-left agent-contact">
                                <li>
                                    <span><i class="fa fa-envelope"></i></span>
                                    <a href="mailto:{{$agent_arr->email}}">{{$agent_arr->email}}</a>
                                </li>
                                <li>
                                    <span><i class="fa fa-phone"></i></span>
                                    <a href="tel:{{$agent_arr->phone}}"> {{$agent_arr->phone}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-12">
                        <div class="agent-info text-center shadow p-4 h-100">
                            <h5 class="font-weight-bold mb-4">Contact Agent</h5>
                            <h4 id="mess_info"></h4>

                            <form id="kt_form_1" method="post" action="{{route('savePropertyLeadData')}}" class="contact-form">
                                @csrf
                                 <input type="hidden" id="frmType" value="1">
                                <div class="form-group pt-0">
                                    <input type="text" required name="first_name"  id="first_name" class="form-control" placeholder="First Name*">
                                </div>
                                <div class="form-group pt-0">
                                    <input type="text" required name="last_name" id="last_name" class="form-control" placeholder="Last Name*">
                                </div>
                                <div class="form-group pt-0">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email Address*">
                                </div>
                                <div class="form-group pt-0">
                                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone No.*">
                                </div>
                                <div class="form-group pt-0">
                                    <textarea class="form-control" name="message" id="message" placeholder="Your Message*" rows="5"></textarea>
                                </div>
                                <div class="form-check">
                                    <input ype="checkbox"   type="checkbox" class="form-check-input" id="chkLead1">
                                    <span class="form-check-label" for="exampleCheck1">
                                        I accept Privacy Policy & Terms & Conditions
                                    </span>
                                </div>
                                <div class="text-center mt-3">
                                    <button type="button" id="btnsavePropertyLeadData" class="btn send-btn">Send Message <i class="fa fa-caret-right ml-3"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- CONTACT FORM -->
<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 mx-auto">
                <h2 class="text-center mb-3">Let’s talk...</h2>
                <div class="col-lg-6 mx-auto">
    				<div class="z-index-0 position-relative bg-white agent-info p-4">
						<h3 id="mess_info" style="color:#008080"></h3>
	    				<form id="kt_form_1"  id="contact_formAjaxSubmit"  method="post" action="{{route('saveContactData')}}" class="contact-form">
							@csrf
	    					<div class="row">
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="first_name" name="first_name" class="form-control" placeholder="First Name*">
		    						<label>First Name*</label>
		    					</div>	    					
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="last_name"  name="last_name" class="form-control" placeholder="Last Name*">
		    						<label>Last Name*</label>
		    					</div>
		    				</div>
		    				<div class="row">
		    					<div class="form-group col-md-6">
		    						<input type="email" required id="email" name="email" class="form-control" placeholder="Email Address*">
		    						<label>Email Address*</label>
		    					</div>	    					
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="phone" name="phone" class="form-control" placeholder="Phone No.*">
		    						<label>Phone No.*</label>
		    					</div>
		    				</div>
		    				<div class="row">
		    					<div class="form-group col-md-12">
		    						<textarea class="form-control" id="message" required name="message" placeholder="Your Message*" rows="5"></textarea>
		    						<label>Your Message*</label>
		    					</div>
	                            <div class="col-12">
	                                <div class="form-check">
	                                    <input required  type="checkbox" class="form-check-input" id="exampleCheck1">
	                                    <span class="form-check-label" for="exampleCheck1">
	                                        I accept Privacy Policy & Terms & Conditions
	                                    </span>
	                                </div>
	                            </div>
		    					<div class="col-12 text-center text-md-right mt-3">
									
									<button type="button" class="btn send-btn"  id="btnContactSave">
									Send Message <i class="fa fa-caret-right ml-3"></i>
										</button>

		    					</div>	
		    				</div>
	    				</form>
	    			</div>
    			</div>
            </div>
        </div>
    </div>
</section>