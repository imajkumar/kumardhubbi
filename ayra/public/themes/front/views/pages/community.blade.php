<?php

use App\Helpers\AyraHelp;

$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 2);
$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;

?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-white text-center mt-4">Cayman Communities</h1>
        </div>
    </div>
</div>
</section>
<?php
$data_arr = AyraHelp::getCommunitiesByHOSTHOME($web_arr_->vid);
//print_r($data_arr);

?>

<!-- Communities -->
<section class="pt-5">
    <div class="container">
        <div class="row">
            <?php
            foreach ($data_arr as $key => $rowData) {
                $photo = asset('uploads/img/logo') . "/" . $rowData->images;

                if($rowData->is_published==1){
                    ?>
                <div class="col-md-6 mb-4">
                    <a href="{{route('siteCommunityDetails',$rowData->slug)}}">
                        <div class="grid">
                            <figure class="effect-bubba">
                                <img src="{{$photo}}" alt="west-bay">
                                <figcaption>
                                    <h3>{{$rowData->title}}</h3>
                                </figcaption>
                            </figure>
                        </div>
                    </a>
                </div>
            <?php
                }
               
            
            }

            ?>







        </div>
    </div>
</section>