<?php

use App\Helpers\AyraHelp;

$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 2);
$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;

$data_arr = AyraHelp::getSellingData();

?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-white text-center mt-4">Search Results</h1>

            <form method="get" action="/search" class=" mt-4 search-form">
                {{ csrf_field() }}
                <div class="input-group">
                    <input type="text" name="search_text" class="form-control" placeholder="Search by property type, name, city">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</section>


<!-- Search Results -->
<section class="pt-5">
    <div class="container">
        <div class="row">
            @foreach($users as $rowData)

            <div class="col-md-6 col-lg-4 mb-4">
                <a href="{{route('prop_cat_details',$rowData->prop_id)}}" class="property-info text-center">
                    <figure class="overflow-hidden position-relative">
                        <img src="{{optional($rowData)->picture_url}}" alt="{{optional($rowData)->property_title}}">
                    </figure>
                    <h5 class="font-weight-bold text-blue">{{optional($rowData)->property_title}}</h5>

                    <p class="font-weight-bold d-flex justify-content-around my-3 text-black">
                        <span class="mx-lg-2">{{optional($rowData)->listprice_currency}} {{optional($rowData)->listprice}}</span>
                        <span class="mx-lg-2">MLS# : {{optional($rowData)->mlsId}}</span>
                    </p>

                    <ul class="text-light list-unstyled property-amenities mt-4">
                        <li title="Sq. Ft.">
                            <i class="fa fa-map"></i> 3850
                        </li>
                        <li title="Bedrooms">
                            <i class="fa fa-bed"></i> {{optional($rowData)->num_bedrooms}}
                        </li>
                        <li title="Bathrooms">
                            <i class="fa fa-bath"></i> {{optional($rowData)->num_full_bathrooms}}
                        </li>
                        <li title="Year Built">
                            <i class="fa fa-calendar"></i> {{optional($rowData)->year_built}}
                        </li>
                    </ul>
                </a>
            </div>

            @endforeach
            <!-- {{ $users->links() }} -->

            <div class="col-12 search-pagination">
                {{ $users->links() }}
            </div>
        </div>
    </div>
</section>