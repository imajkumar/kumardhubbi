<?php 
$web_arr_=AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data=AyraHelp::getBlockByHOST_HOME($web_arr_->vid,2);
$wecomeIMG=asset('uploads/img/logo')."/".optional($static_block_data)->video_img;

?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-12">
					<h1 class="text-white text-center mt-4">Buying Details</h1>
			    </div>
			</div>
		</div>
    </section>



    <section class="pt-5 pb-xl-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-11 col-xl-10 mx-auto">
                    <div id="detail-slider" class="carousel slide" data-ride="carousel" data-interval="false">

                        <ul class="carousel-indicators">
                            <li data-target="#detail-slider" data-slide-to="0" class="active"></li>
                            <li data-target="#detail-slider" data-slide-to="1"></li>
                            <li data-target="#detail-slider" data-slide-to="2"></li>
                        </ul>

                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="img/property1.jpg" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="img/property1.jpg" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="img/property1.jpg" alt="">
                            </div>
                        </div>

                        <div class="controls">
	                        <a class="prev" href="#detail-slider" data-slide="prev">
	                            <i class="fa fa-caret-left"></i>
	                        </a>
	                        <a class="next" href="#detail-slider" data-slide="next">
	                            <i class="fa fa-caret-right"></i>
	                        </a>
	                    </div>

                    </div>

                    <div class="d-md-flex justify-content-between align-items-center py-4 mt-5 mt-md-0">
                        <div class="mb-3 mb-md-0">
                            <h2 class="property-name mb-2">The Cayman Islander Hotel</h2>
                            <p class="text-black">
                                <i class="fa fa-map-marker"></i>
                                West Bay, Cayman Islands
                            </p>
                        </div>

                        <div class="text-left text-md-right">
                            <p class="text-black mb-2 font-weight-bold">MLS# 406210</p>
                            <span class="label-blue">US $20,100,000</span>
                        </div>
                    </div>

                    <div class="row text-center">
                    	<div class="col-6 col-md-3 mb-4 mb-md-0">
                    		<div class="bg-light-blue p-3 px-md-4 h-100">
                    			<p class="mb-1">Property width</p>
                    			<h3 class="font-weight-bold">495 <span class="small">mtr</span></h3>
                    		</div>
                    	</div>

                    	<div class="col-6 col-md-3 mb-4 mb-md-0">
                    		<div class="bg-light-blue p-3 px-md-4 h-100">
                    			<p class="mb-1">Property depth</p>
                    			<h3 class="font-weight-bold">495 <span class="small">mtr</span></h3>
                    		</div>
                    	</div>

                    	<div class="col-6 col-md-3">
                    		<div class="bg-light-blue p-3 px-md-4 h-100">
                    			<p class="mb-1">Acres:</p>
                    			<h3 class="font-weight-bold">5.67</h3>
                    		</div>
                    	</div>

                    	<div class="col-6 col-md-3">
                    		<div class="bg-light-blue p-3 px-md-4 h-100">
                    			<p class="mb-1">Built in year:</p>
                    			<h3 class="font-weight-bold">1976</h3>
                    		</div>
                    	</div>
                    </div>

                    <div class="mt-5">
                    	<h5 class="font-weight-bold mb-2">Description:</h5>
                    	<p>The Cayman Islander Hotel property was unarguably one of the most iconic resort locations in the Cayman Islands. With a rich heritage in Cayman’s past, this property is poised to prosper with the guidance of a visionary developer. The rarity of this lush, tropical oasis in the heart of Cayman’s Seven Mile Beach Resort area is a sanctuary that simply does not exists elsewhere. The hotel was damaged in Ivan and is being offered at simple land value, however there is a pool and four separate buildings throughout the property that, with varying degrees of repair, could be easily resurrected. This property offers an element of income that could be achieved rather quickly and expanded in phases or a complete and revolutionary redevelopment opportunity on acreage that is simply no longer available in the heart of Seven Mile Beach. Once redeveloped this property will rise again and stand as a pillar of The Cayman Islands tourism and resort offerings.</p>

                    	<h5 class="font-weight-bold mb-2 mt-5">Property Details:</h5>
                    	<div class="d-md-flex justify-content-between">
                    		
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<!-- CONTACT FORM -->
<section class="py-5">
    <div class="container">
        <div class="row">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif
            
            <div class="col-lg-8 mx-auto">
				<div class="z-index-0 position-relative bg-white agent-info p-4">
					<h3 id="mess_info" style="color:#008080"></h3>
    				<form id="kt_form_1"  id="contact_formAjaxSubmit"  method="post" action="{{route('saveContactData')}}" class="contact-form">
						@csrf
    					<div class="row">
	    					<div class="form-group col-md-6">
	    						<input type="text" required id="first_name" name="first_name" class="form-control" placeholder="First Name*">
	    						<label>First Name*</label>
	    					</div>	    					
	    					<div class="form-group col-md-6">
	    						<input type="text" required id="last_name"  name="last_name" class="form-control" placeholder="Last Name*">
	    						<label>Last Name*</label>
	    					</div>
	    				</div>
	    				<div class="row">
	    					<div class="form-group col-md-6">
	    						<input type="email" required id="email" name="email" class="form-control" placeholder="Email Address*">
	    						<label>Email Address*</label>
	    					</div>	    					
	    					<div class="form-group col-md-6">
	    						<input type="text" required id="phone" name="phone" class="form-control" placeholder="Phone No.*">
	    						<label>Phone No.*</label>
	    					</div>
	    				</div>
	    				<div class="row">
	    					<div class="form-group col-md-12">
	    						<textarea class="form-control" id="message" required name="message" placeholder="Your Message*" rows="5"></textarea>
	    						<label>Your Message*</label>
	    					</div>
                            <div class="col-12">
                                <div class="form-check">
                                    <input required  type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <span class="form-check-label" for="exampleCheck1">
                                        I accept Privacy Policy & Terms & Conditions
                                    </span>
                                </div>
                            </div>
	    					<div class="col-12 text-center text-md-right mt-3">
								
								<button type="button" class="btn send-btn"  id="btnContactSave">
								Send Message <i class="fa fa-caret-right ml-3"></i>
									</button>

	    					</div>	
	    				</div>
    				</form>
    			</div>
			</div>
        </div>
    </div>
</section>