<?php
$web_arr_ = AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data = AyraHelp::getBlockByHOST_HOME($web_arr_->vid, 5);
// print_r($static_block_data);

$wecomeIMG = asset('uploads/img/logo') . "/" . optional($static_block_data)->video_img;

?>



<!-- BANNER CONTENT -->
<div class="container text-center">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-white text-center mt-4">Valuation Form</h1>
        </div>
    </div>
</div>
</section>


<!-- CONTACT FORM -->
<section class="py-5">

    <div class="container">
    <div class="row">
        <div class="col-lg-12 my-5 text-center">
        	<h2 class="mb-3">Selling</h2>
            <p>
                {!! optional($static_block_data)->static_content !!}
            </p>
        </div>
    </div>

        <div class="row">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif

            <div class="col-lg-10 mx-auto">
            	<h3>Valuation Form</h3>
                <form id="kt_form_1" method="post" action="{{route('saveContactValuableData')}}" class="contact-form">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <input type="text" required name="name" class="form-control" placeholder="Your Name*">
                            <label>Your Name*</label>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" required name="email" class="form-control" placeholder="Email*">
                            <label>Email*</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <input type="text" required name="telephone" class="form-control" placeholder="Telephone*">
                            <label>Telephone*</label>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" required name="address" class="form-control" placeholder="Street Address.*">
                            <label>Street Address.*</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <input type="text" name="block" required class="form-control" placeholder="Block*">
                            <label>Block*</label>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" name="parcel" required class="form-control" placeholder="Parcel.*">
                            <label>Parcel.*</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <input type="text" name="square" required class="form-control" placeholder="*">
                            <label>Approximate Square Footage*</label>
                        </div>
                        <div class="form-group col-md-6">
                            <?php
                            $MyData = DB::table('all_mst')->where('table_name','frontView')->get();

                             ?>

                            <select name="view_data" id="" required class="form-control">
                                <option selected value="">-SELECT-</option>
                                <?php 
                                foreach ($MyData as $key => $rowData) {
                                   ?>
                                    <option value="{{$rowData->key_value}}">{{$rowData->key_value}}</option>
                                   <?php
                                }
                                ?>
                            </select>
                            <label>View*</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">

                            <select name="bedrooms" id="" required class="form-control">
                            <option selected value="">-SELECT-</option>
                                <?php 
                                $MyData = DB::table('all_mst')->where('table_name','BedRoom')->get();
                                foreach ($MyData as $key => $rowData) {
                                   ?>
                                    <option value="{{$rowData->key_value}}">{{$rowData->key_value}}</option>
                                   <?php
                                }
                                ?>
                            </select>

                            <label>Bedrooms*</label>
                        </div>
                        <div class="form-group col-md-6">

                            <select name="bathrooms" id="" required class="form-control">
                            <option selected value="">-SELECT-</option>
                                <?php 
                                $MyData = DB::table('all_mst')->where('table_name','BathRoom')->get();
                                foreach ($MyData as $key => $rowData) {
                                   ?>
                                    <option value="{{$rowData->key_value}}">{{$rowData->key_value}}</option>
                                   <?php
                                }
                                ?>
                            </select>
                            </select>

                            <label>Bathrooms*</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <textarea class="form-control" name="message" placeholder="Your Message*" rows="5"></textarea>
                            <label>Special Features - Additional Comments*</label>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input required type="checkbox" class="form-check-input" id="exampleCheck1">
                                <span class="form-check-label" for="exampleCheck1">
                                    By clicking "Send Message", you agree to our Privacy Policy.
                                </span>
                            </div>
                        </div>
                        <div class="col-12 text-center text-md-right mt-3">

                            <button class="btn send-btn" data-ktwizard-type="action-submit">
                                Send Message <i class="fa fa-caret-right ml-3"></i>
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>