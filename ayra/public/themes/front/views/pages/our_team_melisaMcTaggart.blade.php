<?php

use App\Helpers\AyraHelp;

$web_arr_=AyraHelp::getVendorbyDomain(HOST_DOMAIN);
$static_block_data=AyraHelp::getBlockByHOST_HOME($web_arr_->vid,2);
$wecomeIMG=asset('uploads/img/logo')."/".optional($static_block_data)->video_img;


$photo=asset('uploads/img/logo')."/".$data_arr->photo;



?>
<!-- BANNER CONTENT -->
<div class="container text-center">
    		<div class="row">
    			<div class="col-lg-12">
					<h1 class="text-white text-center mt-4">Melisa-McTaggart</h1>
			    </div>
			</div>
		</div>
    </section>



    <section class="pt-5 pb-xl-5">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-5 col-lg-4 mb-4 mb-md-0">
                    <figure class="agent-image">
                        <img src="{{$photo}}" alt="Agent" class="img-thumbnail">
                    </figure>
                    <figcaption>
                        <ul class="list-unstyled text-left agent-contact">
                            <li>
                                <span><i class="fa fa-envelope"></i></span>
                                <a href="mailto:{{optional($data_arr)->email}}">{{optional($data_arr)->email}}</a>
                            </li>
                            <li>
                                <span><i class="fa fa-phone"></i></span>
                                <a href="tel:+{{optional($data_arr)->phone}}"> {{optional($data_arr)->phone}}</a>
                            </li>   
                        </ul>
                    </figcaption>
                </div>
                <div class="col-md-7 col-lg-8">
                    <h3 class="font-weight-bold mb-2 text-blue">{{optional($data_arr)->name}}</h3>
                    <h5 class="font-weight-bold text-black mb-3">{{optional($data_arr)->designation}}</h5>
                    
                    <div class="agent-text">
                       {!! $data_arr->info_data!!}
                        <div id="more-text" style="display: none;">
                            <p>In an effort to expand her experiences, Paula became an Executive Legal Assistant and over the next 15 years earned much experience in corporate law, contract law, conveyance as well as litigation and estate law. In addition to her legal career, Paula opened an exclusive ladies boutique which she successfully ran for 3 years before selling.</p>
                        </div>
                    </div>

                    <p><a id="read-more" class="btn blue-btn mt-3">Read More</a></p>
                </div>
            </div>

            <div class="row">                
                <div class="col-md-8 col-lg-6 mx-auto">
                @if(Session::has('success'))
   <div class="alert alert-success">
     {{ Session::get('success') }}
   </div>
@endif

                    <div class="agent-info text-center shadow p-4 h-100">
                        <h5 class="font-weight-bold mb-4">Send a Message</h5>
                        <div class="col-lg-6 mx-auto">
    				<div class="z-index-0 position-relative bg-white agent-info p-4">
						<h3 id="mess_info" style="color:#008080"></h3>
	    				<form id="kt_form_1"  id="contact_formAjaxSubmit"  method="post" action="{{route('saveContactData')}}" class="contact-form">
							@csrf
	    					<div class="row">
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="first_name" name="first_name" class="form-control" placeholder="First Name*">
		    						<label>First Name*</label>
		    					</div>	    					
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="last_name"  name="last_name" class="form-control" placeholder="Last Name*">
		    						<label>Last Name*</label>
		    					</div>
		    				</div>
		    				<div class="row">
		    					<div class="form-group col-md-6">
		    						<input type="email" required id="email" name="email" class="form-control" placeholder="Email Address*">
		    						<label>Email Address*</label>
		    					</div>	    					
		    					<div class="form-group col-md-6">
		    						<input type="text" required id="phone" name="phone" class="form-control" placeholder="Phone No.*">
		    						<label>Phone No.*</label>
		    					</div>
		    				</div>
		    				<div class="row">
		    					<div class="form-group col-md-12">
		    						<textarea class="form-control" id="message" required name="message" placeholder="Your Message*" rows="5"></textarea>
		    						<label>Your Message*</label>
		    					</div>
	                            <div class="col-12">
	                                <div class="form-check">
	                                    <input required  type="checkbox" class="form-check-input" id="exampleCheck1">
	                                    <span class="form-check-label" for="exampleCheck1">
	                                        I accept Privacy Policy & Terms & Conditions
	                                    </span>
	                                </div>
	                            </div>
		    					<div class="col-12 text-center text-md-right mt-3">
									
									<button type="button" class="btn send-btn"  id="btnContactSave">
									Send Message <i class="fa fa-caret-right ml-3"></i>
										</button>

		    					</div>	
		    				</div>
	    				</form>
	    			</div>
    			</div>
                    </div>
                </div>
            </div>
        </div>
    </section>